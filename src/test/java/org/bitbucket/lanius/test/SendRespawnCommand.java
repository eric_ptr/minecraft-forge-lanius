package org.bitbucket.lanius.test;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;

import net.minecraft.network.play.client.CPacketClientStatus;

public class SendRespawnCommand extends ModCommand {

	public SendRespawnCommand() {
		super("sendrespawn", "respawn");
	}

	@Override
	protected String getParamUsage() {
		return null;
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		Lanius.mc.player.connection.sendPacket(new CPacketClientStatus(CPacketClientStatus.State.PERFORM_RESPAWN));
	}

}
