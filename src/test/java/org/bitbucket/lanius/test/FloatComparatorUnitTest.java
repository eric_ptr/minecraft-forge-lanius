package org.bitbucket.lanius.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FloatComparatorUnitTest {
	private static final Comparator<Float> floatCmp = new Comparator<Float>() {
		@Override
		public int compare(Float o1, Float o2) {
			return o1.compareTo(o2);
		}
	};
	
	public static void main(String[] args) {
		final List<Float> floats = new ArrayList<>();
		floats.add(0.0F);
		floats.add(-0.0F);
		floats.add(-0.000001F);
		floats.add(-0.000002F);
		floats.add(1.232435353535354F);
		floats.add(1.232435353535354F);
		floats.add(1.232535353535353F);
		floats.add(Float.POSITIVE_INFINITY);
		floats.add(Float.NEGATIVE_INFINITY);
		floats.add(Float.MIN_VALUE);
		floats.add(Float.MAX_VALUE);
		floats.add(Float.MIN_NORMAL);
		floats.add(Float.NaN);
		floats.add(Float.NaN);
		floats.add(-Float.valueOf(1000));
		floats.add(0.0000000000000000000000001F);
		floats.add(0.0000000000000000000000002F);
		floats.add(180.0F);
		floats.add(180.0F);
		floats.add(180.00000000002213F);
		Collections.sort(floats, floatCmp);
		System.out.println(Float.compare(Float.NaN, Float.NaN));
	}
}
