package org.bitbucket.lanius.test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.util.game.InventoryUtil;

import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionUtils;

public class HorriblePotCommand extends ModCommand {

	public HorriblePotCommand() {
		super("horriblepotiontest", "horriblepotion", "potion", "pot");
	}

	@Override
	protected String getParamUsage() {
		return null;
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		ItemStack crashStack = new ItemStack(Items.SPLASH_POTION);
		List<PotionEffect> effects = new ArrayList<>();
		// PotionEffect effect = new PotionEffect(MobEffects.LEVITATION,
		// Integer.MAX_VALUE, 127, false, true);
		// effects.add(effect);
		// effects.add(new PotionEffect(MobEffects., Integer.MAX_VALUE, 127, false,
		// true))
		Iterator<Potion> potIt = Potion.REGISTRY.iterator();
		while (potIt.hasNext()) {
			Potion pot = potIt.next();
			if (!pot.isBadEffect()) {
				continue;
			}
			PotionEffect effect = new PotionEffect(pot, Integer.MAX_VALUE, 127, false, true);
			effects.add(effect);
		}
		PotionUtils.appendEffects(crashStack, effects);
		/*
		 * for (final EntityEquipmentSlot equipSlot : EntityEquipmentSlot.values()) {
		 * for (IAttributeInstance attr :
		 * Lanius.mc.player.getAttributeMap().getAllAttributes()) { for (int operation =
		 * 0; operation <= 2; operation++) {
		 * crashStack.addAttributeModifier(attr.getAttribute().getName(), new
		 * AttributeModifier( UUID.randomUUID(), attr.getAttribute().getName(),
		 * Double.NaN, operation), equipSlot); } } }
		 */
		Lanius.mc.playerController.sendSlotPacket(crashStack,
				InventoryUtil.HOTBAR_BEGIN + Lanius.mc.player.inventory.currentItem);
	}

}
