package org.bitbucket.lanius.reflect;

public final class SrgMappings {
	// Eric: fields
	public static final String[] CommandHandler_commandSet = { "field_71561_b", "commandSet" };
	public static final String[] GuiIngame_itemRenderer = { "field_73841_b", "itemRenderer" };
	public static final String[] Minecraft_session = { "field_71449_j", "session" };
	public static final String[] Session_sessionType = { "field_152429_d", "sessionType" };
	public static final String[] EntityPlayerSP_positionUpdateTicks = { "field_175168_bP", "positionUpdateTicks" };
	public static final String[] CPacketPlayer_rotating = { "field_149481_i", "rotating" };
	public static final String[] CPacketPlayer_yaw = { "field_149476_e", "yaw" };
	public static final String[] CPacketPlayer_pitch = { "field_149473_f", "pitch" };
	public static final String[] GuiIngame_overlayPlayerList = { "field_175196_v", "overlayPlayerList" };
	public static final String[] BlockStateContainer_StateImplementation_block = { "field_177239_a", "block" };
	public static final String[] NetHandlerPlayClient_doneLoadingTerrain = { "field_147309_h", "doneLoadingTerrain" };
	public static final String[] CPacketPlayer_moving = { "field_149480_h", "moving" };
	public static final String[] Minecraft_timer = { "field_71428_T", "timer" };
	public static final String[] Entity_isInWeb = { "field_70134_J", "isInWeb" };
	public static final String[] EntityLivingBase_jumpTicks = { "field_70773_bE", "jumpTicks" };
	public static final String[] CPacketPlayer_y = { "field_149477_b", "y" };
	public static final String[] EntityRenderer_renderHand = { "field_175074_C", "renderHand" };
	public static final String[] CPacketChatMessage_message = { "field_149440_a", "message" };
	public static final String[] ClickEvent_value = { "field_150670_b", "value" };
	public static final String[] EntityPlayer_sleeping = { "field_71083_bS", "sleeping" };
	public static final String[] EntityPlayer_sleepTimer = { "field_71076_b", "sleepTimer" };
	public static final String[] CPacketPlayer_onGround = { "field_149474_g", "onGround" };
	public static final String[] PlayerControllerMP_blockHitDelay = { "field_78781_i", "blockHitDelay" };
	public static final String[] Entity_fire = { "field_190534_ay", "fire" };
	public static final String[] PlayerControllerMP_isHittingBlock = { "field_78778_j", "isHittingBlock" };
	public static final String[] PlayerControllerMP_currentBlock = { "field_178895_c", "currentBlock" };
	public static final String[] PlayerControllerMP_curBlockDamageMP = { "field_78770_f", "curBlockDamageMP" };
	public static final String[] Minecraft_rightClickDelayTimer = { "field_71467_ac", "rightClickDelayTimer" };
	public static final String[] ItemTool_attackDamage = { "field_77865_bY", "attackDamage" };
	public static final String[] EntityPlayerSP_horseJumpPowerCounter = { "field_110320_a", "horseJumpPowerCounter" };
	public static final String[] EntityPlayerSP_horseJumpPower = { "field_110321_bQ", "horseJumpPower" };
	public static final String[] GuiEditSign_tileSign = { "field_146848_f", "tileSign" };
	public static final String[] GuiChest_upperChestInventory = { "field_147016_v", "upperChestInventory" };
	public static final String[] GuiChest_lowerChestInventory = { "field_147015_w", "lowerChestInventory" };
	public static final String[] EntityFireball_ticksInAir = { "field_70234_an", "ticksInAir" };

	// Eric: methods
	public static final String[] Block_setSoundType = { "setSoundType", "func_149672_a" };
	public static final String[] EntityBoat_updateMotion = { "updateMotion", "func_184450_w" };
	public static final String[] EntityPlayerSP_onUpdateWalkingPlayer = { "onUpdateWalkingPlayer", "func_175161_p" };
	public static final String[] Block_disableStats = { "disableStats", "func_149649_H" };
	public static final String[] EntityRenderer_getFOVModifier = { "getFOVModifier", "func_78481_a" };
	public static final String[] ItemBow_findAmmo = { "findAmmo", "func_185060_a" };
	public static final String[] EntityLivingBase_getWaterSlowDown = { "getWaterSlowDown", "func_189749_co" };
	public static final String[] EntityRenderer_setupCameraTransform = { "setupCameraTransform", "func_78479_a" };
	public static final String[] EntityLivingBase_getJumpUpwardsMotion = { "getJumpUpwardsMotion", "func_175134_bD" };
}
