package org.bitbucket.lanius.reflect;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.annotation.Nonnull;

import net.minecraftforge.fml.relauncher.ReflectionHelper;

@SuppressWarnings("deprecation")
public final class ReflectHelper {
	private static class MemberMapping implements Serializable {
		private static final long serialVersionUID = 1L;
		private final String name1, name2;

		private MemberMapping(String name1, String name2) {
			this.name1 = name1;
			this.name2 = name2;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof MemberMapping)) {
				return false;
			}
			MemberMapping other = (MemberMapping) obj;
			return other.name1.equals(name1) && other.name2.equals(name2);
		}

		@Override
		public int hashCode() {
			return Objects.hash(name1, name2);
		}

		@Override
		public String toString() {
			return name1 + ":" + name2;
		}
	}

	private static final ConcurrentMap<MemberMapping, Field> cachedFields = new ConcurrentHashMap<>();
	private static final ConcurrentMap<MemberMapping, Method> cachedMethods = new ConcurrentHashMap<>();
	private static Field modifiersField = null;

	static {
		try {
			modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Retrieves the method with the specified source code name and SRG name and
	 * parameter types. This method was created for compatibility with the
	 * SrgMappings class.
	 * 
	 * @param clazz          the class containing the method
	 * @param methodNames    the source code name (at index 0) and the SRG name (at
	 *                       index 1) of the method
	 * @param parameterTypes the types in order that the method takes
	 * @return a Method object representing the specified method
	 */
	@Nonnull
	public static Method findMethod(@Nonnull Class<?> clazz, @Nonnull String[] methodNames,
			Class<?>... parameterTypes) {
		MemberMapping mapping = new MemberMapping(methodNames[0], methodNames[1]);
		if (!cachedMethods.containsKey(mapping)) {
			Method method = ReflectionHelper.findMethod(clazz, methodNames[0], methodNames[1], parameterTypes);
			cachedMethods.put(mapping, method);
			return method;
		}
		return cachedMethods.get(mapping);
	}

	@SuppressWarnings("unchecked")
	public static <T, E> T getValue(Class<? super E> classToAccess, E instance, String... fieldNames) {
		MemberMapping mapping = new MemberMapping(fieldNames[0], fieldNames[1]);
		if (!cachedFields.containsKey(mapping)) {
			try {
				return (T) putCachedField(classToAccess, mapping).get(instance); // Eric: just so that it avoids walking
																					// the cache.
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		try {
			return (T) cachedFields.get(mapping).get(instance);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void makeAccessible(Field field)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		field.setAccessible(true);
		if (Modifier.isFinal(field.getModifiers())) {
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		}
	}

	private static <E> Field putCachedField(Class<? super E> classToAccess, MemberMapping mapping)
			throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
		Field valueField = classToAccess
				.getDeclaredField(System.getProperty("lanius.debugEnv") == null ? mapping.name1 : mapping.name2);
		makeAccessible(valueField);
		cachedFields.put(mapping, valueField);
		return valueField;
	}

	public static <T, E> void setValue(Class<? super T> classToAccess, T instance, E value, String... fieldNames) {
		MemberMapping mapping = new MemberMapping(fieldNames[0], fieldNames[1]);
		if (!cachedFields.containsKey(mapping)) {
			try {
				putCachedField(classToAccess, mapping).set(instance, value);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (NoSuchFieldException e) {
				e.printStackTrace();
			} catch (SecurityException e) {
				e.printStackTrace();
			}
		} else {
			try {
				cachedFields.get(mapping).set(instance, value);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}
}
