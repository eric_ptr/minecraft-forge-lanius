package org.bitbucket.lanius.core;

import static org.objectweb.asm.Opcodes.ASM5;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

public class ContainerPlayerTransformer extends BasicTransformer {
	private class Visitor extends ClassVisitor {

		public Visitor(ClassVisitor cv) {
			super(ASM5, cv);
		}

		@Override
		public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
			MethodVisitor delegateVisitor = super.visitMethod(access, name, desc, signature, exceptions);
			if (name.equals(LaniusLoadingPlugin.nameRegistry.get("isItemValid"))
					&& desc.equals("(Lnet/minecraft/item/ItemStack;)Z")) {
				return new ObfuscationAdapter(delegateVisitor) {
					@Override
					public void visitCode() {
						super.visitCode();
						visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/hook/HookManager", "itemValidManager",
								"Lorg/bitbucket/lanius/hook/HookManager;");
						visitTypeInsn(NEW, "org/bitbucket/lanius/hook/impl/ItemValidData");
						visitInsn(DUP);
						visitVarInsn(ALOAD, 0);
						visitVarInsn(ALOAD, 1);
						visitInsn(DUP);
						visitVarInsn(ALOAD, 0);
						visitFieldInsn(GETFIELD, "net/minecraft/inventory/ContainerPlayer$1", "val$entityequipmentslot",
								"Lnet/minecraft/inventory/EntityEquipmentSlot;");
						visitVarInsn(ALOAD, 1);
						visitMethodInsn(INVOKESTATIC, "net/minecraft/entity/EntityLiving", "getSlotForItemStack",
								"(Lnet/minecraft/item/ItemStack;)Lnet/minecraft/inventory/EntityEquipmentSlot;", false);
						Label cmpneLabel = new Label();
						visitJumpInsn(IF_ACMPNE, cmpneLabel);
						visitInsn(POP);
						visitInsn(ICONST_1);
						Label gotoLabel = new Label();
						visitJumpInsn(GOTO, gotoLabel);
						visitLabel(cmpneLabel);
						visitInsn(POP);
						visitInsn(ICONST_0);
						visitLabel(gotoLabel);
						visitMethodInsn(INVOKESPECIAL, "org/bitbucket/lanius/hook/impl/ItemValidData", "<init>",
								"(Lnet/minecraft/inventory/Slot;Lnet/minecraft/item/ItemStack;Z)V", false);
						visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/util/Phase", "START",
								"Lorg/bitbucket/lanius/util/Phase;");
						visitMethodInsn(INVOKEVIRTUAL, "org/bitbucket/lanius/hook/HookManager", "execute",
								"(Lorg/bitbucket/lanius/hook/HookData;Lorg/bitbucket/lanius/util/Phase;)Ljava/lang/Object;",
								false);
						visitTypeInsn(CHECKCAST, "java/lang/Boolean");
						visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);
						visitInsn(IRETURN);
					}
				};
			}
			return delegateVisitor;
		}

	}

	public ContainerPlayerTransformer() {
		super("net.minecraft.inventory.ContainerPlayer$1");
	}

	@Override
	protected ClassVisitor classVisitor(ClassWriter classWriter) {
		return new Visitor(classWriter);
	}

}
