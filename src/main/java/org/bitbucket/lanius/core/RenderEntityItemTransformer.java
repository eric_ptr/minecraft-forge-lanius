package org.bitbucket.lanius.core;

import static org.objectweb.asm.Opcodes.ASM5;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;

public final class RenderEntityItemTransformer extends BasicTransformer {

	private final class RenderVisitor extends ClassVisitor {

		public RenderVisitor(ClassVisitor cv) {
			super(ASM5, cv);
		}

		@Override
		public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
			final MethodVisitor delegateVisitor = super.visitMethod(access, name, desc, signature, exceptions);
			return name.equals(LaniusLoadingPlugin.nameRegistry.get("doRender"))
					&& desc.equals("(Lnet/minecraft/entity/item/EntityItem;DDDFF)V")
							? new ObfuscationAdapter(delegateVisitor) {
								@Override
								public void visitCode() {
									super.visitCode();
									mv.visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/hook/HookManager",
											"doRenderManager", "Lorg/bitbucket/lanius/hook/HookManager;");
									mv.visitTypeInsn(NEW, "org/bitbucket/lanius/hook/impl/DoRenderData");
									mv.visitInsn(DUP);
									mv.visitVarInsn(ALOAD, 0);
									mv.visitVarInsn(ALOAD, 1);
									mv.visitMethodInsn(INVOKESPECIAL, "org/bitbucket/lanius/hook/impl/DoRenderData",
											"<init>",
											"(Lnet/minecraft/client/renderer/entity/Render;Lnet/minecraft/entity/Entity;)V",
											false);
									mv.visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/util/Phase", "START",
											"Lorg/bitbucket/lanius/util/Phase;");
									mv.visitMethodInsn(INVOKEVIRTUAL, "org/bitbucket/lanius/hook/HookManager",
											"execute",
											"(Lorg/bitbucket/lanius/hook/HookData;Lorg/bitbucket/lanius/util/Phase;)Ljava/lang/Object;",
											false);
									mv.visitTypeInsn(CHECKCAST, "java/lang/Boolean");
									mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z",
											false);
									final Label retVal = new Label();
									mv.visitJumpInsn(IFEQ, retVal);
									mv.visitInsn(RETURN);
									mv.visitLabel(retVal);
								}

								@Override
								public void visitInsn(int opcode) {
									if (opcode == RETURN) {
										mv.visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/hook/HookManager",
												"doRenderManager", "Lorg/bitbucket/lanius/hook/HookManager;");
										mv.visitTypeInsn(NEW, "org/bitbucket/lanius/hook/impl/DoRenderData");
										mv.visitInsn(DUP);
										mv.visitVarInsn(ALOAD, 0);
										mv.visitVarInsn(ALOAD, 1);
										mv.visitMethodInsn(INVOKESPECIAL, "org/bitbucket/lanius/hook/impl/DoRenderData",
												"<init>",
												"(Lnet/minecraft/client/renderer/entity/Render;Lnet/minecraft/entity/Entity;)V",
												false);
										mv.visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/util/Phase", "END",
												"Lorg/bitbucket/lanius/util/Phase;");
										mv.visitMethodInsn(INVOKEVIRTUAL, "org/bitbucket/lanius/hook/HookManager",
												"execute",
												"(Lorg/bitbucket/lanius/hook/HookData;Lorg/bitbucket/lanius/util/Phase;)Ljava/lang/Object;",
												false);
									}
									super.visitInsn(opcode);
								}
							}
							: delegateVisitor;
		}
	}

	public RenderEntityItemTransformer() {
		super("net/minecraft/client/renderer/entity/RenderEntityItem");
	}

	@Override
	protected ClassVisitor classVisitor(final ClassWriter classWriter) {
		return new RenderVisitor(classWriter);
	}

}
