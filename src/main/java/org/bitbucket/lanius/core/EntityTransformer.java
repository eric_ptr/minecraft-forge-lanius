package org.bitbucket.lanius.core;

import static org.objectweb.asm.Opcodes.ALOAD;
import static org.objectweb.asm.Opcodes.ASM5;
import static org.objectweb.asm.Opcodes.ASTORE;
import static org.objectweb.asm.Opcodes.ATHROW;
import static org.objectweb.asm.Opcodes.BIPUSH;
import static org.objectweb.asm.Opcodes.CHECKCAST;
import static org.objectweb.asm.Opcodes.D2F;
import static org.objectweb.asm.Opcodes.DADD;
import static org.objectweb.asm.Opcodes.DALOAD;
import static org.objectweb.asm.Opcodes.DASTORE;
import static org.objectweb.asm.Opcodes.DCMPG;
import static org.objectweb.asm.Opcodes.DCMPL;
import static org.objectweb.asm.Opcodes.DCONST_0;
import static org.objectweb.asm.Opcodes.DLOAD;
import static org.objectweb.asm.Opcodes.DMUL;
import static org.objectweb.asm.Opcodes.DNEG;
import static org.objectweb.asm.Opcodes.DSTORE;
import static org.objectweb.asm.Opcodes.DSUB;
import static org.objectweb.asm.Opcodes.DUP;
import static org.objectweb.asm.Opcodes.F2D;
import static org.objectweb.asm.Opcodes.F2I;
import static org.objectweb.asm.Opcodes.FADD;
import static org.objectweb.asm.Opcodes.FCMPL;
import static org.objectweb.asm.Opcodes.FCONST_0;
import static org.objectweb.asm.Opcodes.FCONST_1;
import static org.objectweb.asm.Opcodes.FLOAD;
import static org.objectweb.asm.Opcodes.FMUL;
import static org.objectweb.asm.Opcodes.FNEG;
import static org.objectweb.asm.Opcodes.FSTORE;
import static org.objectweb.asm.Opcodes.FSUB;
import static org.objectweb.asm.Opcodes.GETFIELD;
import static org.objectweb.asm.Opcodes.GETSTATIC;
import static org.objectweb.asm.Opcodes.GOTO;
import static org.objectweb.asm.Opcodes.I2F;
import static org.objectweb.asm.Opcodes.IADD;
import static org.objectweb.asm.Opcodes.ICONST_0;
import static org.objectweb.asm.Opcodes.ICONST_1;
import static org.objectweb.asm.Opcodes.IFEQ;
import static org.objectweb.asm.Opcodes.IFGE;
import static org.objectweb.asm.Opcodes.IFGT;
import static org.objectweb.asm.Opcodes.IFLE;
import static org.objectweb.asm.Opcodes.IFLT;
import static org.objectweb.asm.Opcodes.IFNE;
import static org.objectweb.asm.Opcodes.IFNULL;
import static org.objectweb.asm.Opcodes.IF_ACMPEQ;
import static org.objectweb.asm.Opcodes.IF_ACMPNE;
import static org.objectweb.asm.Opcodes.IF_ICMPGE;
import static org.objectweb.asm.Opcodes.ILOAD;
import static org.objectweb.asm.Opcodes.INEG;
import static org.objectweb.asm.Opcodes.INSTANCEOF;
import static org.objectweb.asm.Opcodes.INVOKEINTERFACE;
import static org.objectweb.asm.Opcodes.INVOKESPECIAL;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;
import static org.objectweb.asm.Opcodes.INVOKEVIRTUAL;
import static org.objectweb.asm.Opcodes.ISTORE;
import static org.objectweb.asm.Opcodes.LCMP;
import static org.objectweb.asm.Opcodes.LLOAD;
import static org.objectweb.asm.Opcodes.LSTORE;
import static org.objectweb.asm.Opcodes.NEW;
import static org.objectweb.asm.Opcodes.POP;
import static org.objectweb.asm.Opcodes.PUTFIELD;
import static org.objectweb.asm.Opcodes.RETURN;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public final class EntityTransformer extends BasicTransformer {
	private final class EntityVisitor extends ClassVisitor {
		public EntityVisitor(ClassVisitor cv) {
			super(ASM5, cv);
		}

		@Override
		public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
			final MethodVisitor mv = new ObfuscationAdapter(
					super.visitMethod(access, name, desc, signature, exceptions));
			if (name.equals(LaniusLoadingPlugin.nameRegistry.get("move"))
					&& desc.equals("(Lnet/minecraft/entity/MoverType;DDD)V")) {
				mv.visitCode();
				Label l0 = new Label();
				Label l1 = new Label();
				Label l2 = new Label();
				mv.visitTryCatchBlock(l0, l1, l2, "java/lang/Throwable");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "noClip", "Z");
				Label l3 = new Label();
				mv.visitJumpInsn(IFEQ, l3);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "resetPositionToBB", "()V", false);
				Label l4 = new Label();
				mv.visitJumpInsn(GOTO, l4);
				mv.visitLabel(l3);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ALOAD, 1);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/entity/MoverType", "PISTON",
						"Lnet/minecraft/entity/MoverType;");
				Label l5 = new Label();
				mv.visitJumpInsn(IF_ACMPNE, l5);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getTotalWorldTime", "()J", false);
				mv.visitVarInsn(LSTORE, 8);
				mv.visitVarInsn(LLOAD, 8);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltasGameTime", "J");
				mv.visitInsn(LCMP);
				Label l6 = new Label();
				mv.visitJumpInsn(IFEQ, l6);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKESTATIC, "java/util/Arrays", "fill", "([DD)V", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(LLOAD, 8);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "pistonDeltasGameTime", "J");
				mv.visitLabel(l6);
				mv.visitFrame(Opcodes.F_APPEND, 1, new Object[] { Opcodes.LONG }, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l7 = new Label();
				mv.visitJumpInsn(IFEQ, l7);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/util/EnumFacing$Axis", "X",
						"Lnet/minecraft/util/EnumFacing$Axis;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/EnumFacing$Axis", "ordinal", "()I", false);
				mv.visitVarInsn(ISTORE, 10);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitInsn(DALOAD);
				mv.visitInsn(DADD);
				mv.visitLdcInsn(new Double("-0.51"));
				mv.visitLdcInsn(new Double("0.51"));
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "clamp", "(DDD)D", false);
				mv.visitVarInsn(DSTORE, 11);
				mv.visitVarInsn(DLOAD, 11);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitInsn(DALOAD);
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitVarInsn(DLOAD, 11);
				mv.visitInsn(DASTORE);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "abs", "(D)D", false);
				mv.visitLdcInsn(new Double("9.999999747378752E-6"));
				mv.visitInsn(DCMPG);
				Label l8 = new Label();
				mv.visitJumpInsn(IFGT, l8);
				mv.visitInsn(RETURN);
				mv.visitLabel(l8);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitJumpInsn(GOTO, l5);
				mv.visitLabel(l7);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l9 = new Label();
				mv.visitJumpInsn(IFEQ, l9);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/util/EnumFacing$Axis", "Y",
						"Lnet/minecraft/util/EnumFacing$Axis;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/EnumFacing$Axis", "ordinal", "()I", false);
				mv.visitVarInsn(ISTORE, 10);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitInsn(DALOAD);
				mv.visitInsn(DADD);
				mv.visitLdcInsn(new Double("-0.51"));
				mv.visitLdcInsn(new Double("0.51"));
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "clamp", "(DDD)D", false);
				mv.visitVarInsn(DSTORE, 11);
				mv.visitVarInsn(DLOAD, 11);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitInsn(DALOAD);
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 4);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitVarInsn(DLOAD, 11);
				mv.visitInsn(DASTORE);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "abs", "(D)D", false);
				mv.visitLdcInsn(new Double("9.999999747378752E-6"));
				mv.visitInsn(DCMPG);
				Label l10 = new Label();
				mv.visitJumpInsn(IFGT, l10);
				mv.visitInsn(RETURN);
				mv.visitLabel(l10);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitJumpInsn(GOTO, l5);
				mv.visitLabel(l9);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l11 = new Label();
				mv.visitJumpInsn(IFNE, l11);
				mv.visitInsn(RETURN);
				mv.visitLabel(l11);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/util/EnumFacing$Axis", "Z",
						"Lnet/minecraft/util/EnumFacing$Axis;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/EnumFacing$Axis", "ordinal", "()I", false);
				mv.visitVarInsn(ISTORE, 10);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitInsn(DALOAD);
				mv.visitInsn(DADD);
				mv.visitLdcInsn(new Double("-0.51"));
				mv.visitLdcInsn(new Double("0.51"));
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "clamp", "(DDD)D", false);
				mv.visitVarInsn(DSTORE, 11);
				mv.visitVarInsn(DLOAD, 11);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitInsn(DALOAD);
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "pistonDeltas", "[D");
				mv.visitVarInsn(ILOAD, 10);
				mv.visitVarInsn(DLOAD, 11);
				mv.visitInsn(DASTORE);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "abs", "(D)D", false);
				mv.visitLdcInsn(new Double("9.999999747378752E-6"));
				mv.visitInsn(DCMPG);
				mv.visitJumpInsn(IFGT, l5);
				mv.visitInsn(RETURN);
				mv.visitLabel(l5);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitFieldInsn(GETFIELD, "net/minecraft/world/World", "profiler",
						"Lnet/minecraft/profiler/Profiler;");
				mv.visitLdcInsn("move");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection",
						"(Ljava/lang/String;)V", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posX", "D");
				mv.visitVarInsn(DSTORE, 8);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posY", "D");
				mv.visitVarInsn(DSTORE, 10);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posZ", "D");
				mv.visitVarInsn(DSTORE, 12);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "isInWeb", "Z");
				Label l12 = new Label();
				mv.visitJumpInsn(IFEQ, l12);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitInsn(ICONST_0);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "isInWeb", "Z");
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("0.25"));
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitLdcInsn(new Double("0.05000000074505806"));
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DSTORE, 4);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("0.25"));
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitInsn(DCONST_0);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "motionX", "D");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitInsn(DCONST_0);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "motionY", "D");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitInsn(DCONST_0);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "motionZ", "D");
				mv.visitLabel(l12);
				mv.visitFrame(Opcodes.F_APPEND, 3, new Object[] { Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE }, 0,
						null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DSTORE, 14);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DSTORE, 16);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitVarInsn(DSTORE, 18);

				mv.visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/hook/HookManager", "edgeWalkManager",
						"Lorg/bitbucket/lanius/hook/HookManager;");
				mv.visitTypeInsn(NEW, "org/bitbucket/lanius/hook/impl/EdgeWalkData");
				mv.visitInsn(DUP);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 1);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKESPECIAL, "org/bitbucket/lanius/hook/impl/EdgeWalkData", "<init>",
						"(Lnet/minecraft/entity/Entity;Lnet/minecraft/entity/MoverType;DDD)V", false);
				mv.visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/util/Phase", "START",
						"Lorg/bitbucket/lanius/util/Phase;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "org/bitbucket/lanius/hook/HookManager", "execute",
						"(Lorg/bitbucket/lanius/hook/HookData;Lorg/bitbucket/lanius/util/Phase;)Ljava/lang/Object;",
						false);
				mv.visitTypeInsn(CHECKCAST, "java/lang/Boolean");
				mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/Boolean", "booleanValue", "()Z", false);
				Label edgeWalkLabel = new Label();
				mv.visitJumpInsn(IFNE, edgeWalkLabel);

				mv.visitVarInsn(ALOAD, 1);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/entity/MoverType", "SELF",
						"Lnet/minecraft/entity/MoverType;");
				Label l13 = new Label();
				mv.visitJumpInsn(IF_ACMPEQ, l13);
				mv.visitVarInsn(ALOAD, 1);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/entity/MoverType", "PLAYER",
						"Lnet/minecraft/entity/MoverType;");
				Label l14 = new Label();
				mv.visitJumpInsn(IF_ACMPNE, l14);
				mv.visitLabel(l13);
				mv.visitFrame(Opcodes.F_APPEND, 3, new Object[] { Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE }, 0,
						null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "onGround", "Z");
				mv.visitJumpInsn(IFEQ, l14);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "isSneaking", "()Z", false);
				mv.visitJumpInsn(IFEQ, l14);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitTypeInsn(INSTANCEOF, "net/minecraft/entity/player/EntityPlayer");
				mv.visitJumpInsn(IFEQ, l14);

				mv.visitLabel(edgeWalkLabel);

				mv.visitLdcInsn(new Double("0.05"));
				mv.visitVarInsn(DSTORE, 20);
				Label l15 = new Label();
				mv.visitLabel(l15);
				mv.visitFrame(Opcodes.F_APPEND, 1, new Object[] { Opcodes.DOUBLE }, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l16 = new Label();
				mv.visitJumpInsn(IFEQ, l16);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "stepHeight", "F");
				mv.visitInsn(FNEG);
				mv.visitInsn(F2D);
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getCollisionBoxes",
						"(Lnet/minecraft/entity/Entity;Lnet/minecraft/util/math/AxisAlignedBB;)Ljava/util/List;",
						false);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "isEmpty", "()Z", true);
				mv.visitJumpInsn(IFEQ, l16);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DCMPG);
				Label l17 = new Label();
				mv.visitJumpInsn(IFGE, l17);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("-0.05"));
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFLT, l17);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DSTORE, 2);
				Label l18 = new Label();
				mv.visitJumpInsn(GOTO, l18);
				mv.visitLabel(l17);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l19 = new Label();
				mv.visitJumpInsn(IFLE, l19);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitJumpInsn(GOTO, l18);
				mv.visitLabel(l19);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DADD);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitLabel(l18);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DSTORE, 14);
				mv.visitJumpInsn(GOTO, l15);
				mv.visitLabel(l16);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l20 = new Label();
				mv.visitJumpInsn(IFEQ, l20);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "stepHeight", "F");
				mv.visitInsn(FNEG);
				mv.visitInsn(F2D);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getCollisionBoxes",
						"(Lnet/minecraft/entity/Entity;Lnet/minecraft/util/math/AxisAlignedBB;)Ljava/util/List;",
						false);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "isEmpty", "()Z", true);
				mv.visitJumpInsn(IFEQ, l20);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DCMPG);
				Label l21 = new Label();
				mv.visitJumpInsn(IFGE, l21);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("-0.05"));
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFLT, l21);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DSTORE, 6);
				Label l22 = new Label();
				mv.visitJumpInsn(GOTO, l22);
				mv.visitLabel(l21);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l23 = new Label();
				mv.visitJumpInsn(IFLE, l23);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitJumpInsn(GOTO, l22);
				mv.visitLabel(l23);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DADD);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitLabel(l22);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitVarInsn(DSTORE, 18);
				mv.visitJumpInsn(GOTO, l16);
				mv.visitLabel(l20);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFEQ, l14);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFEQ, l14);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "stepHeight", "F");
				mv.visitInsn(FNEG);
				mv.visitInsn(F2D);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getCollisionBoxes",
						"(Lnet/minecraft/entity/Entity;Lnet/minecraft/util/math/AxisAlignedBB;)Ljava/util/List;",
						false);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "isEmpty", "()Z", true);
				mv.visitJumpInsn(IFEQ, l14);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DCMPG);
				Label l24 = new Label();
				mv.visitJumpInsn(IFGE, l24);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("-0.05"));
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFLT, l24);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DSTORE, 2);
				Label l25 = new Label();
				mv.visitJumpInsn(GOTO, l25);
				mv.visitLabel(l24);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l26 = new Label();
				mv.visitJumpInsn(IFLE, l26);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitJumpInsn(GOTO, l25);
				mv.visitLabel(l26);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DADD);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitLabel(l25);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DSTORE, 14);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DCMPG);
				Label l27 = new Label();
				mv.visitJumpInsn(IFGE, l27);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("-0.05"));
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFLT, l27);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DSTORE, 6);
				Label l28 = new Label();
				mv.visitJumpInsn(GOTO, l28);
				mv.visitLabel(l27);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l29 = new Label();
				mv.visitJumpInsn(IFLE, l29);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitJumpInsn(GOTO, l28);
				mv.visitLabel(l29);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitLdcInsn(new Double("0.05"));
				mv.visitInsn(DADD);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitLabel(l28);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitVarInsn(DSTORE, 18);
				mv.visitJumpInsn(GOTO, l20);
				mv.visitLabel(l14);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);

				mv.visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/hook/HookManager", "edgeWalkManager",
						"Lorg/bitbucket/lanius/hook/HookManager;");
				mv.visitTypeInsn(NEW, "org/bitbucket/lanius/hook/impl/EdgeWalkData");
				mv.visitInsn(DUP);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 1);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKESPECIAL, "org/bitbucket/lanius/hook/impl/EdgeWalkData", "<init>",
						"(Lnet/minecraft/entity/Entity;Lnet/minecraft/entity/MoverType;DDD)V", false);
				mv.visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/util/Phase", "END",
						"Lorg/bitbucket/lanius/util/Phase;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "org/bitbucket/lanius/hook/HookManager", "execute",
						"(Lorg/bitbucket/lanius/hook/HookData;Lorg/bitbucket/lanius/util/Phase;)Ljava/lang/Object;",
						false);
				mv.visitInsn(POP);

				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "expand",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getCollisionBoxes",
						"(Lnet/minecraft/entity/Entity;Lnet/minecraft/util/math/AxisAlignedBB;)Ljava/util/List;",
						false);
				mv.visitVarInsn(ASTORE, 20);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 21);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l30 = new Label();
				mv.visitJumpInsn(IFEQ, l30);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 22);
				mv.visitVarInsn(ALOAD, 20);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 23);
				Label l31 = new Label();
				mv.visitLabel(l31);
				mv.visitFrame(Opcodes.F_FULL, 15,
						new Object[] { "net/minecraft/entity/Entity", "net/minecraft/entity/MoverType", Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, "java/util/List",
								"net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER, Opcodes.INTEGER },
						0, new Object[] {});
				mv.visitVarInsn(ILOAD, 22);
				mv.visitVarInsn(ILOAD, 23);
				Label l32 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l32);
				mv.visitVarInsn(ALOAD, 20);
				mv.visitVarInsn(ILOAD, 22);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateYOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 4);
				mv.visitIincInsn(22, 1);
				mv.visitJumpInsn(GOTO, l31);
				mv.visitLabel(l32);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				mv.visitLabel(l30);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l33 = new Label();
				mv.visitJumpInsn(IFEQ, l33);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 22);
				mv.visitVarInsn(ALOAD, 20);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 23);
				Label l34 = new Label();
				mv.visitLabel(l34);
				mv.visitFrame(Opcodes.F_APPEND, 2, new Object[] { Opcodes.INTEGER, Opcodes.INTEGER }, 0, null);
				mv.visitVarInsn(ILOAD, 22);
				mv.visitVarInsn(ILOAD, 23);
				Label l35 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l35);
				mv.visitVarInsn(ALOAD, 20);
				mv.visitVarInsn(ILOAD, 22);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateXOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitIincInsn(22, 1);
				mv.visitJumpInsn(GOTO, l34);
				mv.visitLabel(l35);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFEQ, l33);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				mv.visitLabel(l33);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				Label l36 = new Label();
				mv.visitJumpInsn(IFEQ, l36);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 22);
				mv.visitVarInsn(ALOAD, 20);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 23);
				Label l37 = new Label();
				mv.visitLabel(l37);
				mv.visitFrame(Opcodes.F_APPEND, 2, new Object[] { Opcodes.INTEGER, Opcodes.INTEGER }, 0, null);
				mv.visitVarInsn(ILOAD, 22);
				mv.visitVarInsn(ILOAD, 23);
				Label l38 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l38);
				mv.visitVarInsn(ALOAD, 20);
				mv.visitVarInsn(ILOAD, 22);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateZOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitIincInsn(22, 1);
				mv.visitJumpInsn(GOTO, l37);
				mv.visitLabel(l38);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFEQ, l36);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				mv.visitLabel(l36);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "onGround", "Z");
				Label l39 = new Label();
				mv.visitJumpInsn(IFNE, l39);
				mv.visitVarInsn(DLOAD, 16);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitInsn(DCMPL);
				Label l40 = new Label();
				mv.visitJumpInsn(IFEQ, l40);
				mv.visitVarInsn(DLOAD, 16);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPG);
				mv.visitJumpInsn(IFGE, l40);
				mv.visitLabel(l39);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitInsn(ICONST_1);
				Label l41 = new Label();
				mv.visitJumpInsn(GOTO, l41);
				mv.visitLabel(l40);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitInsn(ICONST_0);
				mv.visitLabel(l41);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { Opcodes.INTEGER });
				mv.visitVarInsn(ISTORE, 22);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "stepHeight", "F");
				mv.visitInsn(FCONST_0);
				mv.visitInsn(FCMPL);
				Label l42 = new Label();
				mv.visitJumpInsn(IFLE, l42);
				mv.visitVarInsn(ILOAD, 22);
				mv.visitJumpInsn(IFEQ, l42);
				mv.visitVarInsn(DLOAD, 14);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCMPL);
				Label l43 = new Label();
				mv.visitJumpInsn(IFNE, l43);
				mv.visitVarInsn(DLOAD, 18);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFEQ, l42);
				mv.visitLabel(l43);
				mv.visitFrame(Opcodes.F_APPEND, 1, new Object[] { Opcodes.INTEGER }, 0, null);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DSTORE, 23);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DSTORE, 25);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitVarInsn(DSTORE, 27);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 29);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 21);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "stepHeight", "F");
				mv.visitInsn(F2D);
				mv.visitVarInsn(DSTORE, 4);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 14);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DLOAD, 18);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "expand",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getCollisionBoxes",
						"(Lnet/minecraft/entity/Entity;Lnet/minecraft/util/math/AxisAlignedBB;)Ljava/util/List;",
						false);
				mv.visitVarInsn(ASTORE, 30);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 31);
				mv.visitVarInsn(ALOAD, 31);
				mv.visitVarInsn(DLOAD, 14);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DLOAD, 18);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "expand",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 32);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DSTORE, 33);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 35);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 36);
				Label l44 = new Label();
				mv.visitLabel(l44);
				mv.visitFrame(Opcodes.F_FULL, 24, new Object[] { "net/minecraft/entity/Entity",
						"net/minecraft/entity/MoverType", Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
						Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
						"java/util/List", "net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER, Opcodes.DOUBLE,
						Opcodes.DOUBLE, Opcodes.DOUBLE, "net/minecraft/util/math/AxisAlignedBB", "java/util/List",
						"net/minecraft/util/math/AxisAlignedBB", "net/minecraft/util/math/AxisAlignedBB",
						Opcodes.DOUBLE, Opcodes.INTEGER, Opcodes.INTEGER }, 0, new Object[] {});
				mv.visitVarInsn(ILOAD, 35);
				mv.visitVarInsn(ILOAD, 36);
				Label l45 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l45);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitVarInsn(ILOAD, 35);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 32);
				mv.visitVarInsn(DLOAD, 33);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateYOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 33);
				mv.visitIincInsn(35, 1);
				mv.visitJumpInsn(GOTO, l44);
				mv.visitLabel(l45);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 31);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DLOAD, 33);
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 31);
				mv.visitVarInsn(DLOAD, 14);
				mv.visitVarInsn(DSTORE, 36);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 38);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 39);
				Label l46 = new Label();
				mv.visitLabel(l46);
				mv.visitFrame(Opcodes.F_APPEND, 3, new Object[] { Opcodes.DOUBLE, Opcodes.INTEGER, Opcodes.INTEGER }, 0,
						null);
				mv.visitVarInsn(ILOAD, 38);
				mv.visitVarInsn(ILOAD, 39);
				Label l47 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l47);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitVarInsn(ILOAD, 38);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 31);
				mv.visitVarInsn(DLOAD, 36);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateXOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 36);
				mv.visitIincInsn(38, 1);
				mv.visitJumpInsn(GOTO, l46);
				mv.visitLabel(l47);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 31);
				mv.visitVarInsn(DLOAD, 36);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 31);
				mv.visitVarInsn(DLOAD, 18);
				mv.visitVarInsn(DSTORE, 39);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 41);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 42);
				Label l48 = new Label();
				mv.visitLabel(l48);
				mv.visitFrame(Opcodes.F_APPEND, 3, new Object[] { Opcodes.DOUBLE, Opcodes.INTEGER, Opcodes.INTEGER }, 0,
						null);
				mv.visitVarInsn(ILOAD, 41);
				mv.visitVarInsn(ILOAD, 42);
				Label l49 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l49);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitVarInsn(ILOAD, 41);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 31);
				mv.visitVarInsn(DLOAD, 39);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateZOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 39);
				mv.visitIincInsn(41, 1);
				mv.visitJumpInsn(GOTO, l48);
				mv.visitLabel(l49);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 31);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DLOAD, 39);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 31);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 42);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(DSTORE, 43);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 45);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 46);
				Label l50 = new Label();
				mv.visitLabel(l50);
				mv.visitFrame(Opcodes.F_FULL, 31, new Object[] { "net/minecraft/entity/Entity",
						"net/minecraft/entity/MoverType", Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
						Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
						"java/util/List", "net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER, Opcodes.DOUBLE,
						Opcodes.DOUBLE, Opcodes.DOUBLE, "net/minecraft/util/math/AxisAlignedBB", "java/util/List",
						"net/minecraft/util/math/AxisAlignedBB", "net/minecraft/util/math/AxisAlignedBB",
						Opcodes.DOUBLE, Opcodes.INTEGER, Opcodes.DOUBLE, Opcodes.INTEGER, Opcodes.DOUBLE,
						Opcodes.INTEGER, "net/minecraft/util/math/AxisAlignedBB", Opcodes.DOUBLE, Opcodes.INTEGER,
						Opcodes.INTEGER }, 0, new Object[] {});
				mv.visitVarInsn(ILOAD, 45);
				mv.visitVarInsn(ILOAD, 46);
				Label l51 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l51);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitVarInsn(ILOAD, 45);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 42);
				mv.visitVarInsn(DLOAD, 43);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateYOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 43);
				mv.visitIincInsn(45, 1);
				mv.visitJumpInsn(GOTO, l50);
				mv.visitLabel(l51);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 42);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DLOAD, 43);
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 42);
				mv.visitVarInsn(DLOAD, 14);
				mv.visitVarInsn(DSTORE, 46);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 48);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 49);
				Label l52 = new Label();
				mv.visitLabel(l52);
				mv.visitFrame(Opcodes.F_APPEND, 3, new Object[] { Opcodes.DOUBLE, Opcodes.INTEGER, Opcodes.INTEGER }, 0,
						null);
				mv.visitVarInsn(ILOAD, 48);
				mv.visitVarInsn(ILOAD, 49);
				Label l53 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l53);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitVarInsn(ILOAD, 48);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 42);
				mv.visitVarInsn(DLOAD, 46);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateXOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 46);
				mv.visitIincInsn(48, 1);
				mv.visitJumpInsn(GOTO, l52);
				mv.visitLabel(l53);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 42);
				mv.visitVarInsn(DLOAD, 46);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 42);
				mv.visitVarInsn(DLOAD, 18);
				mv.visitVarInsn(DSTORE, 49);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 51);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 52);
				Label l54 = new Label();
				mv.visitLabel(l54);
				mv.visitFrame(Opcodes.F_APPEND, 3, new Object[] { Opcodes.DOUBLE, Opcodes.INTEGER, Opcodes.INTEGER }, 0,
						null);
				mv.visitVarInsn(ILOAD, 51);
				mv.visitVarInsn(ILOAD, 52);
				Label l55 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l55);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitVarInsn(ILOAD, 51);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 42);
				mv.visitVarInsn(DLOAD, 49);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateZOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 49);
				mv.visitIincInsn(51, 1);
				mv.visitJumpInsn(GOTO, l54);
				mv.visitLabel(l55);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 42);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DLOAD, 49);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(ASTORE, 42);
				mv.visitVarInsn(DLOAD, 36);
				mv.visitVarInsn(DLOAD, 36);
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DLOAD, 39);
				mv.visitVarInsn(DLOAD, 39);
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitVarInsn(DSTORE, 52);
				mv.visitVarInsn(DLOAD, 46);
				mv.visitVarInsn(DLOAD, 46);
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DLOAD, 49);
				mv.visitVarInsn(DLOAD, 49);
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitVarInsn(DSTORE, 54);
				mv.visitVarInsn(DLOAD, 52);
				mv.visitVarInsn(DLOAD, 54);
				mv.visitInsn(DCMPL);
				Label l56 = new Label();
				mv.visitJumpInsn(IFLE, l56);
				mv.visitVarInsn(DLOAD, 36);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitVarInsn(DLOAD, 39);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitVarInsn(DLOAD, 33);
				mv.visitInsn(DNEG);
				mv.visitVarInsn(DSTORE, 4);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 31);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				Label l57 = new Label();
				mv.visitJumpInsn(GOTO, l57);
				mv.visitLabel(l56);
				mv.visitFrame(Opcodes.F_APPEND, 2, new Object[] { Opcodes.DOUBLE, Opcodes.DOUBLE }, 0, null);
				mv.visitVarInsn(DLOAD, 46);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitVarInsn(DLOAD, 49);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitVarInsn(DLOAD, 43);
				mv.visitInsn(DNEG);
				mv.visitVarInsn(DSTORE, 4);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 42);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				mv.visitLabel(l57);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitInsn(ICONST_0);
				mv.visitVarInsn(ISTORE, 56);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "size", "()I", true);
				mv.visitVarInsn(ISTORE, 57);
				Label l58 = new Label();
				mv.visitLabel(l58);
				mv.visitFrame(Opcodes.F_APPEND, 2, new Object[] { Opcodes.INTEGER, Opcodes.INTEGER }, 0, null);
				mv.visitVarInsn(ILOAD, 56);
				mv.visitVarInsn(ILOAD, 57);
				Label l59 = new Label();
				mv.visitJumpInsn(IF_ICMPGE, l59);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitVarInsn(ILOAD, 56);
				mv.visitMethodInsn(INVOKEINTERFACE, "java/util/List", "get", "(I)Ljava/lang/Object;", true);
				mv.visitTypeInsn(CHECKCAST, "net/minecraft/util/math/AxisAlignedBB");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "calculateYOffset",
						"(Lnet/minecraft/util/math/AxisAlignedBB;D)D", false);
				mv.visitVarInsn(DSTORE, 4);
				mv.visitIincInsn(56, 1);
				mv.visitJumpInsn(GOTO, l58);
				mv.visitLabel(l59);
				mv.visitFrame(Opcodes.F_CHOP, 1, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitInsn(DCONST_0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "offset",
						"(DDD)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				mv.visitVarInsn(DLOAD, 23);
				mv.visitVarInsn(DLOAD, 23);
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DLOAD, 27);
				mv.visitVarInsn(DLOAD, 27);
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitInsn(DCMPL);
				mv.visitJumpInsn(IFLT, l42);
				mv.visitVarInsn(DLOAD, 23);
				mv.visitVarInsn(DSTORE, 2);
				mv.visitVarInsn(DLOAD, 25);
				mv.visitVarInsn(DSTORE, 4);
				mv.visitVarInsn(DLOAD, 27);
				mv.visitVarInsn(DSTORE, 6);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 29);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setEntityBoundingBox",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)V", false);
				mv.visitLabel(l42);
				mv.visitFrame(Opcodes.F_FULL, 14,
						new Object[] { "net/minecraft/entity/Entity", "net/minecraft/entity/MoverType", Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, "java/util/List",
								"net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER },
						0, new Object[] {});
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitFieldInsn(GETFIELD, "net/minecraft/world/World", "profiler",
						"Lnet/minecraft/profiler/Profiler;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitFieldInsn(GETFIELD, "net/minecraft/world/World", "profiler",
						"Lnet/minecraft/profiler/Profiler;");
				mv.visitLdcInsn("rest");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "startSection",
						"(Ljava/lang/String;)V", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "resetPositionToBB", "()V", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(DLOAD, 14);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCMPL);
				Label l60 = new Label();
				mv.visitJumpInsn(IFNE, l60);
				mv.visitVarInsn(DLOAD, 18);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCMPL);
				Label l61 = new Label();
				mv.visitJumpInsn(IFEQ, l61);
				mv.visitLabel(l60);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { "net/minecraft/entity/Entity" });
				mv.visitInsn(ICONST_1);
				Label l62 = new Label();
				mv.visitJumpInsn(GOTO, l62);
				mv.visitLabel(l61);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { "net/minecraft/entity/Entity" });
				mv.visitInsn(ICONST_0);
				mv.visitLabel(l62);
				mv.visitFrame(Opcodes.F_FULL, 14,
						new Object[] { "net/minecraft/entity/Entity", "net/minecraft/entity/MoverType", Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, "java/util/List",
								"net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER },
						2, new Object[] { "net/minecraft/entity/Entity", Opcodes.INTEGER });
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "collidedHorizontally", "Z");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(DLOAD, 16);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitInsn(DCMPL);
				Label l63 = new Label();
				mv.visitJumpInsn(IFEQ, l63);
				mv.visitInsn(ICONST_1);
				Label l64 = new Label();
				mv.visitJumpInsn(GOTO, l64);
				mv.visitLabel(l63);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { "net/minecraft/entity/Entity" });
				mv.visitInsn(ICONST_0);
				mv.visitLabel(l64);
				mv.visitFrame(Opcodes.F_FULL, 14,
						new Object[] { "net/minecraft/entity/Entity", "net/minecraft/entity/MoverType", Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, "java/util/List",
								"net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER },
						2, new Object[] { "net/minecraft/entity/Entity", Opcodes.INTEGER });
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "collidedVertically", "Z");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "collidedVertically", "Z");
				Label l65 = new Label();
				mv.visitJumpInsn(IFEQ, l65);
				mv.visitVarInsn(DLOAD, 16);
				mv.visitInsn(DCONST_0);
				mv.visitInsn(DCMPG);
				mv.visitJumpInsn(IFGE, l65);
				mv.visitInsn(ICONST_1);
				Label l66 = new Label();
				mv.visitJumpInsn(GOTO, l66);
				mv.visitLabel(l65);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { "net/minecraft/entity/Entity" });
				mv.visitInsn(ICONST_0);
				mv.visitLabel(l66);
				mv.visitFrame(Opcodes.F_FULL, 14,
						new Object[] { "net/minecraft/entity/Entity", "net/minecraft/entity/MoverType", Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, "java/util/List",
								"net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER },
						2, new Object[] { "net/minecraft/entity/Entity", Opcodes.INTEGER });
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "onGround", "Z");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "collidedHorizontally", "Z");
				Label l67 = new Label();
				mv.visitJumpInsn(IFNE, l67);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "collidedVertically", "Z");
				Label l68 = new Label();
				mv.visitJumpInsn(IFEQ, l68);
				mv.visitLabel(l67);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { "net/minecraft/entity/Entity" });
				mv.visitInsn(ICONST_1);
				Label l69 = new Label();
				mv.visitJumpInsn(GOTO, l69);
				mv.visitLabel(l68);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { "net/minecraft/entity/Entity" });
				mv.visitInsn(ICONST_0);
				mv.visitLabel(l69);
				mv.visitFrame(Opcodes.F_FULL, 14,
						new Object[] { "net/minecraft/entity/Entity", "net/minecraft/entity/MoverType", Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, "java/util/List",
								"net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER },
						2, new Object[] { "net/minecraft/entity/Entity", Opcodes.INTEGER });
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "collided", "Z");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posX", "D");
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "floor", "(D)I", false);
				mv.visitVarInsn(ISTORE, 23);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posY", "D");
				mv.visitLdcInsn(new Double("0.20000000298023224"));
				mv.visitInsn(DSUB);
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "floor", "(D)I", false);
				mv.visitVarInsn(ISTORE, 24);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posZ", "D");
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "floor", "(D)I", false);
				mv.visitVarInsn(ISTORE, 25);
				mv.visitTypeInsn(NEW, "net/minecraft/util/math/BlockPos");
				mv.visitInsn(DUP);
				mv.visitVarInsn(ILOAD, 23);
				mv.visitVarInsn(ILOAD, 24);
				mv.visitVarInsn(ILOAD, 25);
				mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/math/BlockPos", "<init>", "(III)V", false);
				mv.visitVarInsn(ASTORE, 26);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 26);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getBlockState",
						"(Lnet/minecraft/util/math/BlockPos;)Lnet/minecraft/block/state/IBlockState;", false);
				mv.visitVarInsn(ASTORE, 27);
				mv.visitVarInsn(ALOAD, 27);
				mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/block/state/IBlockState", "getMaterial",
						"()Lnet/minecraft/block/material/Material;", true);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/block/material/Material", "AIR",
						"Lnet/minecraft/block/material/Material;");
				Label l70 = new Label();
				mv.visitJumpInsn(IF_ACMPNE, l70);
				mv.visitVarInsn(ALOAD, 26);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/BlockPos", "down",
						"()Lnet/minecraft/util/math/BlockPos;", false);
				mv.visitVarInsn(ASTORE, 28);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 28);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "getBlockState",
						"(Lnet/minecraft/util/math/BlockPos;)Lnet/minecraft/block/state/IBlockState;", false);
				mv.visitVarInsn(ASTORE, 29);
				mv.visitVarInsn(ALOAD, 29);
				mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/block/state/IBlockState", "getBlock",
						"()Lnet/minecraft/block/Block;", true);
				mv.visitVarInsn(ASTORE, 30);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitTypeInsn(INSTANCEOF, "net/minecraft/block/BlockFence");
				Label l71 = new Label();
				mv.visitJumpInsn(IFNE, l71);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitTypeInsn(INSTANCEOF, "net/minecraft/block/BlockWall");
				mv.visitJumpInsn(IFNE, l71);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitTypeInsn(INSTANCEOF, "net/minecraft/block/BlockFenceGate");
				mv.visitJumpInsn(IFEQ, l70);
				mv.visitLabel(l71);
				mv.visitFrame(Opcodes.F_FULL, 22,
						new Object[] { "net/minecraft/entity/Entity", "net/minecraft/entity/MoverType", Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE,
								Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE, "java/util/List",
								"net/minecraft/util/math/AxisAlignedBB", Opcodes.INTEGER, Opcodes.INTEGER,
								Opcodes.INTEGER, Opcodes.INTEGER, "net/minecraft/util/math/BlockPos",
								"net/minecraft/block/state/IBlockState", "net/minecraft/util/math/BlockPos",
								"net/minecraft/block/state/IBlockState", "net/minecraft/block/Block" },
						0, new Object[] {});
				mv.visitVarInsn(ALOAD, 29);
				mv.visitVarInsn(ASTORE, 27);
				mv.visitVarInsn(ALOAD, 28);
				mv.visitVarInsn(ASTORE, 26);
				mv.visitLabel(l70);
				mv.visitFrame(Opcodes.F_CHOP, 3, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "onGround", "Z");
				mv.visitVarInsn(ALOAD, 27);
				mv.visitVarInsn(ALOAD, 26);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "updateFallState",
						"(DZLnet/minecraft/block/state/IBlockState;Lnet/minecraft/util/math/BlockPos;)V", false);
				mv.visitVarInsn(DLOAD, 14);
				mv.visitVarInsn(DLOAD, 2);
				mv.visitInsn(DCMPL);
				Label l72 = new Label();
				mv.visitJumpInsn(IFEQ, l72);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitInsn(DCONST_0);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "motionX", "D");
				mv.visitLabel(l72);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(DLOAD, 18);
				mv.visitVarInsn(DLOAD, 6);
				mv.visitInsn(DCMPL);
				Label l73 = new Label();
				mv.visitJumpInsn(IFEQ, l73);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitInsn(DCONST_0);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "motionZ", "D");
				mv.visitLabel(l73);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ALOAD, 27);
				mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/block/state/IBlockState", "getBlock",
						"()Lnet/minecraft/block/Block;", true);
				mv.visitVarInsn(ASTORE, 28);
				mv.visitVarInsn(DLOAD, 16);
				mv.visitVarInsn(DLOAD, 4);
				mv.visitInsn(DCMPL);
				Label l74 = new Label();
				mv.visitJumpInsn(IFEQ, l74);
				mv.visitVarInsn(ALOAD, 28);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/block/Block", "onLanded",
						"(Lnet/minecraft/world/World;Lnet/minecraft/entity/Entity;)V", false);
				mv.visitLabel(l74);
				mv.visitFrame(Opcodes.F_APPEND, 1, new Object[] { "net/minecraft/block/Block" }, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "canTriggerWalking", "()Z", false);
				mv.visitJumpInsn(IFEQ, l0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "onGround", "Z");
				Label l75 = new Label();
				mv.visitJumpInsn(IFEQ, l75);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "isSneaking", "()Z", false);
				mv.visitJumpInsn(IFEQ, l75);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitTypeInsn(INSTANCEOF, "net/minecraft/entity/player/EntityPlayer");
				mv.visitJumpInsn(IFNE, l0);
				mv.visitLabel(l75);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "isRiding", "()Z", false);
				mv.visitJumpInsn(IFNE, l0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posX", "D");
				mv.visitVarInsn(DLOAD, 8);
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 29);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posY", "D");
				mv.visitVarInsn(DLOAD, 10);
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 31);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "posZ", "D");
				mv.visitVarInsn(DLOAD, 12);
				mv.visitInsn(DSUB);
				mv.visitVarInsn(DSTORE, 33);
				mv.visitVarInsn(ALOAD, 28);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/init/Blocks", "LADDER", "Lnet/minecraft/block/Block;");
				Label l76 = new Label();
				mv.visitJumpInsn(IF_ACMPEQ, l76);
				mv.visitInsn(DCONST_0);
				mv.visitVarInsn(DSTORE, 31);
				mv.visitLabel(l76);
				mv.visitFrame(Opcodes.F_APPEND, 3, new Object[] { Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE }, 0,
						null);
				mv.visitVarInsn(ALOAD, 28);
				Label l77 = new Label();
				mv.visitJumpInsn(IFNULL, l77);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "onGround", "Z");
				mv.visitJumpInsn(IFEQ, l77);
				mv.visitVarInsn(ALOAD, 28);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 26);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/block/Block", "onEntityWalk",
						"(Lnet/minecraft/world/World;Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/entity/Entity;)V",
						false);
				mv.visitLabel(l77);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "distanceWalkedModified", "F");
				mv.visitInsn(F2D);
				mv.visitVarInsn(DLOAD, 29);
				mv.visitVarInsn(DLOAD, 29);
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DLOAD, 33);
				mv.visitVarInsn(DLOAD, 33);
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "sqrt", "(D)F", false);
				mv.visitInsn(F2D);
				mv.visitLdcInsn(new Double("0.6"));
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitInsn(D2F);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "distanceWalkedModified", "F");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "distanceWalkedOnStepModified", "F");
				mv.visitInsn(F2D);
				mv.visitVarInsn(DLOAD, 29);
				mv.visitVarInsn(DLOAD, 29);
				mv.visitInsn(DMUL);
				mv.visitVarInsn(DLOAD, 31);
				mv.visitVarInsn(DLOAD, 31);
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitVarInsn(DLOAD, 33);
				mv.visitVarInsn(DLOAD, 33);
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "sqrt", "(D)F", false);
				mv.visitInsn(F2D);
				mv.visitLdcInsn(new Double("0.6"));
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitInsn(D2F);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "distanceWalkedOnStepModified", "F");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "distanceWalkedOnStepModified", "F");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "nextStepDistance", "I");
				mv.visitInsn(I2F);
				mv.visitInsn(FCMPL);
				Label l78 = new Label();
				mv.visitJumpInsn(IFLE, l78);
				mv.visitVarInsn(ALOAD, 27);
				mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/block/state/IBlockState", "getMaterial",
						"()Lnet/minecraft/block/material/Material;", true);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/block/material/Material", "AIR",
						"Lnet/minecraft/block/material/Material;");
				mv.visitJumpInsn(IF_ACMPEQ, l78);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "distanceWalkedOnStepModified", "F");
				mv.visitInsn(F2I);
				mv.visitInsn(ICONST_1);
				mv.visitInsn(IADD);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "nextStepDistance", "I");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "isInWater", "()Z", false);
				Label l79 = new Label();
				mv.visitJumpInsn(IFEQ, l79);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "isBeingRidden", "()Z", false);
				Label l80 = new Label();
				mv.visitJumpInsn(IFEQ, l80);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getControllingPassenger",
						"()Lnet/minecraft/entity/Entity;", false);
				mv.visitJumpInsn(IFNULL, l80);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getControllingPassenger",
						"()Lnet/minecraft/entity/Entity;", false);
				Label l81 = new Label();
				mv.visitJumpInsn(GOTO, l81);
				mv.visitLabel(l80);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitLabel(l81);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { "net/minecraft/entity/Entity" });
				mv.visitVarInsn(ASTORE, 35);
				mv.visitVarInsn(ALOAD, 35);
				mv.visitVarInsn(ALOAD, 0);
				Label l82 = new Label();
				mv.visitJumpInsn(IF_ACMPNE, l82);
				mv.visitLdcInsn(new Float("0.35"));
				Label l83 = new Label();
				mv.visitJumpInsn(GOTO, l83);
				mv.visitLabel(l82);
				mv.visitFrame(Opcodes.F_APPEND, 1, new Object[] { "net/minecraft/entity/Entity" }, 0, null);
				mv.visitLdcInsn(new Float("0.4"));
				mv.visitLabel(l83);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { Opcodes.FLOAT });
				mv.visitVarInsn(FSTORE, 36);
				mv.visitVarInsn(ALOAD, 35);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "motionX", "D");
				mv.visitVarInsn(ALOAD, 35);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "motionX", "D");
				mv.visitInsn(DMUL);
				mv.visitLdcInsn(new Double("0.20000000298023224"));
				mv.visitInsn(DMUL);
				mv.visitVarInsn(ALOAD, 35);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "motionY", "D");
				mv.visitVarInsn(ALOAD, 35);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "motionY", "D");
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitVarInsn(ALOAD, 35);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "motionZ", "D");
				mv.visitVarInsn(ALOAD, 35);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "motionZ", "D");
				mv.visitInsn(DMUL);
				mv.visitLdcInsn(new Double("0.20000000298023224"));
				mv.visitInsn(DMUL);
				mv.visitInsn(DADD);
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/util/math/MathHelper", "sqrt", "(D)F", false);
				mv.visitVarInsn(FLOAD, 36);
				mv.visitInsn(FMUL);
				mv.visitVarInsn(FSTORE, 37);
				mv.visitVarInsn(FLOAD, 37);
				mv.visitInsn(FCONST_1);
				mv.visitInsn(FCMPL);
				Label l84 = new Label();
				mv.visitJumpInsn(IFLE, l84);
				mv.visitInsn(FCONST_1);
				mv.visitVarInsn(FSTORE, 37);
				mv.visitLabel(l84);
				mv.visitFrame(Opcodes.F_APPEND, 2, new Object[] { Opcodes.FLOAT, Opcodes.FLOAT }, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getSwimSound",
						"()Lnet/minecraft/util/SoundEvent;", false);
				mv.visitVarInsn(FLOAD, 37);
				mv.visitInsn(FCONST_1);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "rand", "Ljava/util/Random;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Random", "nextFloat", "()F", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "rand", "Ljava/util/Random;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Random", "nextFloat", "()F", false);
				mv.visitInsn(FSUB);
				mv.visitLdcInsn(new Float("0.4"));
				mv.visitInsn(FMUL);
				mv.visitInsn(FADD);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "playSound",
						"(Lnet/minecraft/util/SoundEvent;FF)V", false);
				mv.visitJumpInsn(GOTO, l0);
				mv.visitLabel(l79);
				mv.visitFrame(Opcodes.F_CHOP, 3, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 26);
				mv.visitVarInsn(ALOAD, 28);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "playStepSound",
						"(Lnet/minecraft/util/math/BlockPos;Lnet/minecraft/block/Block;)V", false);
				mv.visitJumpInsn(GOTO, l0);
				mv.visitLabel(l78);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "distanceWalkedOnStepModified", "F");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "nextFlap", "F");
				mv.visitInsn(FCMPL);
				mv.visitJumpInsn(IFLE, l0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "makeFlySound", "()Z", false);
				mv.visitJumpInsn(IFEQ, l0);
				mv.visitVarInsn(ALOAD, 27);
				mv.visitMethodInsn(INVOKEINTERFACE, "net/minecraft/block/state/IBlockState", "getMaterial",
						"()Lnet/minecraft/block/material/Material;", true);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/block/material/Material", "AIR",
						"Lnet/minecraft/block/material/Material;");
				mv.visitJumpInsn(IF_ACMPNE, l0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "distanceWalkedOnStepModified", "F");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "playFlySound", "(F)F", false);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "nextFlap", "F");
				mv.visitLabel(l0);
				mv.visitFrame(Opcodes.F_CHOP, 3, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "doBlockCollisions", "()V", false);
				mv.visitLabel(l1);
				Label l85 = new Label();
				mv.visitJumpInsn(GOTO, l85);
				mv.visitLabel(l2);
				mv.visitFrame(Opcodes.F_SAME1, 0, null, 1, new Object[] { "java/lang/Throwable" });
				mv.visitVarInsn(ASTORE, 29);
				mv.visitVarInsn(ALOAD, 29);
				mv.visitLdcInsn("Checking entity block collision");
				mv.visitMethodInsn(INVOKESTATIC, "net/minecraft/crash/CrashReport", "makeCrashReport",
						"(Ljava/lang/Throwable;Ljava/lang/String;)Lnet/minecraft/crash/CrashReport;", false);
				mv.visitVarInsn(ASTORE, 30);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitLdcInsn("Entity being checked for collision");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/crash/CrashReport", "makeCategory",
						"(Ljava/lang/String;)Lnet/minecraft/crash/CrashReportCategory;", false);
				mv.visitVarInsn(ASTORE, 31);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 31);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "addEntityCrashInfo",
						"(Lnet/minecraft/crash/CrashReportCategory;)V", false);
				mv.visitTypeInsn(NEW, "net/minecraft/util/ReportedException");
				mv.visitInsn(DUP);
				mv.visitVarInsn(ALOAD, 30);
				mv.visitMethodInsn(INVOKESPECIAL, "net/minecraft/util/ReportedException", "<init>",
						"(Lnet/minecraft/crash/CrashReport;)V", false);
				mv.visitInsn(ATHROW);
				mv.visitLabel(l85);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "isWet", "()Z", false);
				mv.visitVarInsn(ISTORE, 29);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getEntityBoundingBox",
						"()Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitLdcInsn(new Double("0.001"));
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/util/math/AxisAlignedBB", "shrink",
						"(D)Lnet/minecraft/util/math/AxisAlignedBB;", false);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/world/World", "isFlammableWithin",
						"(Lnet/minecraft/util/math/AxisAlignedBB;)Z", false);
				Label l86 = new Label();
				mv.visitJumpInsn(IFEQ, l86);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitInsn(ICONST_1);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "dealFireDamage", "(I)V", false);
				mv.visitVarInsn(ILOAD, 29);
				Label l87 = new Label();
				mv.visitJumpInsn(IFNE, l87);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitInsn(DUP);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "fire", "I");
				mv.visitInsn(ICONST_1);
				mv.visitInsn(IADD);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "fire", "I");
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "fire", "I");
				mv.visitJumpInsn(IFNE, l87);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitIntInsn(BIPUSH, 8);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "setFire", "(I)V", false);
				mv.visitJumpInsn(GOTO, l87);
				mv.visitLabel(l86);
				mv.visitFrame(Opcodes.F_APPEND, 1, new Object[] { Opcodes.INTEGER }, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "fire", "I");
				mv.visitJumpInsn(IFGT, l87);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getFireImmuneTicks", "()I", false);
				mv.visitInsn(INEG);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "fire", "I");
				mv.visitLabel(l87);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ILOAD, 29);
				Label l88 = new Label();
				mv.visitJumpInsn(IFEQ, l88);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "isBurning", "()Z", false);
				mv.visitJumpInsn(IFEQ, l88);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETSTATIC, "net/minecraft/init/SoundEvents", "ENTITY_GENERIC_EXTINGUISH_FIRE",
						"Lnet/minecraft/util/SoundEvent;");
				mv.visitLdcInsn(new Float("0.7"));
				mv.visitLdcInsn(new Float("1.6"));
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "rand", "Ljava/util/Random;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Random", "nextFloat", "()F", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "rand", "Ljava/util/Random;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Random", "nextFloat", "()F", false);
				mv.visitInsn(FSUB);
				mv.visitLdcInsn(new Float("0.4"));
				mv.visitInsn(FMUL);
				mv.visitInsn(FADD);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "playSound",
						"(Lnet/minecraft/util/SoundEvent;FF)V", false);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/entity/Entity", "getFireImmuneTicks", "()I", false);
				mv.visitInsn(INEG);
				mv.visitFieldInsn(PUTFIELD, "net/minecraft/entity/Entity", "fire", "I");
				mv.visitLabel(l88);
				mv.visitFrame(Opcodes.F_SAME, 0, null, 0, null);
				mv.visitVarInsn(ALOAD, 0);
				mv.visitFieldInsn(GETFIELD, "net/minecraft/entity/Entity", "world", "Lnet/minecraft/world/World;");
				mv.visitFieldInsn(GETFIELD, "net/minecraft/world/World", "profiler",
						"Lnet/minecraft/profiler/Profiler;");
				mv.visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/profiler/Profiler", "endSection", "()V", false);
				mv.visitLabel(l4);
				mv.visitFrame(
						Opcodes.F_FULL, 5, new Object[] { "net/minecraft/entity/Entity",
								"net/minecraft/entity/MoverType", Opcodes.DOUBLE, Opcodes.DOUBLE, Opcodes.DOUBLE },
						0, new Object[] {});
				mv.visitInsn(RETURN);
				mv.visitMaxs(9, 58);
				mv.visitEnd();
				return null;
			}
			return mv;
		}
	}

	public EntityTransformer() {
		super("net.minecraft.entity.Entity");
	}

	@Override
	protected ClassVisitor classVisitor(ClassWriter classWriter) {
		return new EntityVisitor(classWriter);
	}
}
