package org.bitbucket.lanius.core;

import static org.objectweb.asm.Opcodes.ASM5;

import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;

public final class RenderLivingTransformer extends BasicTransformer {

	private final class RenderLivingVisitor extends ClassVisitor {

		public RenderLivingVisitor(ClassVisitor cv) {
			super(ASM5, cv);
		}

		@Override
		public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
			final MethodVisitor delegateVisitor = super.visitMethod(access, name, desc, signature, exceptions);
			return name.equals(LaniusLoadingPlugin.nameRegistry.get("doRender"))
					&& desc.equals("(Lnet/minecraft/entity/EntityLivingBase;DDDFF)V") && signature.equals("(TT;DDDFF)V")
							? new ObfuscationAdapter(delegateVisitor) {
								@Override
								public void visitMethodInsn(int opcode, String owner, String name, String desc,
										boolean itf) {
									super.visitMethodInsn(opcode, owner, name, desc, itf);
									if (name.equals(LaniusLoadingPlugin.nameRegistry.get("enableOutlineMode"))
											&& desc.equals("(I)V")) {
										visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/hook/HookManager",
												"outlineManager", "Lorg/bitbucket/lanius/hook/HookManager;");
										visitTypeInsn(NEW, "org/bitbucket/lanius/hook/impl/OutlineModeData");
										visitInsn(DUP);
										visitVarInsn(ALOAD, 0);
										visitVarInsn(ALOAD, 0);
										visitVarInsn(ALOAD, 1);
										visitMethodInsn(INVOKEVIRTUAL, "net/minecraft/client/renderer/entity/Render",
												"getTeamColor", "(Lnet/minecraft/entity/Entity;)I", false);
										visitVarInsn(ALOAD, 1);
										visitMethodInsn(INVOKESPECIAL, "org/bitbucket/lanius/hook/impl/OutlineModeData",
												"<init>",
												"(Lnet/minecraft/client/renderer/entity/RenderLivingBase;ILnet/minecraft/entity/Entity;)V",
												false);
										visitFieldInsn(GETSTATIC, "org/bitbucket/lanius/util/Phase", "START",
												"Lorg/bitbucket/lanius/util/Phase;");
										visitMethodInsn(INVOKEVIRTUAL, "org/bitbucket/lanius/hook/HookManager",
												"execute",
												"(Lorg/bitbucket/lanius/hook/HookData;Lorg/bitbucket/lanius/util/Phase;)Ljava/lang/Object;",
												false);
										visitTypeInsn(CHECKCAST, "java/lang/Integer");
										visitMethodInsn(INVOKEVIRTUAL, "java/lang/Integer", "intValue", "()I", false);
										super.visitMethodInsn(opcode, owner, name, desc, itf);
									}
								}
							}
							: delegateVisitor;
		}
	}

	public RenderLivingTransformer() {
		super("net.minecraft.client.renderer.entity.RenderLivingBase");
	}

	@Override
	protected ClassVisitor classVisitor(final ClassWriter classWriter) {
		return new RenderLivingVisitor(classWriter);
	}

}
