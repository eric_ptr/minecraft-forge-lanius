package org.bitbucket.lanius.core;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

public class ObfuscationAdapter extends MethodVisitor implements Opcodes {
	public ObfuscationAdapter(int api, MethodVisitor mv) {
		super(api, mv);
	}

	public ObfuscationAdapter(MethodVisitor mv) {
		this(ASM5, mv);
	}

	private boolean isMemberMapped(String owner, String name) {
		return owner.replace(".", "/").startsWith("net/minecraft/")
				&& LaniusLoadingPlugin.nameRegistry.get(name) != null;
	}

	@Override
	public void visitFieldInsn(int opcode, String owner, String name, String descriptor) {
		if (isMemberMapped(owner, name)) {
			name = LaniusLoadingPlugin.nameRegistry.get(name);
		}
		super.visitFieldInsn(opcode, owner, name, descriptor);
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface) {
		if (isMemberMapped(owner, name)) {
			name = LaniusLoadingPlugin.nameRegistry.get(name);
		}
		super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
	}
}
