package org.bitbucket.lanius.routine.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.HookManager;
import org.bitbucket.lanius.hook.impl.NetHandlerData;
import org.bitbucket.lanius.hook.impl.NetHandlerHook;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Phase;
import org.bitbucket.lanius.util.RoutineUtil;
import org.bitbucket.lanius.util.game.InventoryUtil;
import org.bitbucket.lanius.util.game.NetworkUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Enchantments;
import net.minecraft.init.Items;
import net.minecraft.inventory.ClickType;
import net.minecraft.item.ItemBow;
import net.minecraft.item.ItemFood;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.CPacketHeldItemChange;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.network.play.client.CPacketPlayerTryUseItem;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public final class SpeedyRoutine extends TabbedRoutine implements Hook<NetHandlerData> {

	private int bowMoveCount;
	private boolean executedEat;

	public SpeedyRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.WORLD);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Makes the player mine blocks, place blocks, use food, potions, and bow faster.";
	}

	@Override
	public void init() {
		setMoveCount();
		executedEat = false;
	}

	@Override
	public String name() {
		return "Speedy Gonzales";
	}

	@SubscribeEvent
	public void onClientTick(final TickEvent.ClientTickEvent clientTickEv) {
		if (clientTickEv.phase.equals(TickEvent.Phase.START)) {
			if (Lanius.mc.player != null && !Lanius.mc.isGamePaused()) {
				if (!Lanius.mc.player.isHandActive()) {
					executedEat = false;
				}
				final ItemStack heldStack = Lanius.mc.player.getHeldItem(EnumHand.MAIN_HAND);
				try {
					if (!(InventoryUtil.isStackValid(heldStack) && heldStack.getItem() == Items.BOW
							&& (Lanius.mc.player.capabilities.isCreativeMode
									|| EnchantmentHelper.getEnchantmentLevel(Enchantments.INFINITY, heldStack) > 0
									|| ReflectHelper
											.findMethod(ItemBow.class, SrgMappings.ItemBow_findAmmo, EntityPlayer.class)
											.invoke(Items.BOW, Lanius.mc.player) != null))
							&& !(InventoryUtil.isStackValid(Lanius.mc.player.inventory.getCurrentItem())
									&& (Lanius.mc.player.inventory.getCurrentItem().getItem() instanceof ItemFood
											|| Lanius.mc.player.inventory.getCurrentItem()
													.getItem() instanceof ItemPotion))) {
						return;
					}
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				}
			}
			setRightClickDelayTimer();
		} else if (clientTickEv.phase.equals(TickEvent.Phase.END) && Lanius.mc.player != null) {
			if (!Lanius.mc.isGamePaused()) {
				if (Lanius.mc.player.isHandActive()) {
					final boolean ncpEnabled = RoutineUtil.ncpEnabled(),
							viaVersionEnabled = RoutineUtil.viaVersionEnabled();
					final float EXPAND_VEC = 0.0625F;
					final AxisAlignedBB playerBox = Lanius.mc.player.getEntityBoundingBox();
					if (bowMoveCount >= 0) {
						++bowMoveCount;
						final int shootDelay = getInt("Shoot Delay").intValue();
						final boolean multiPacket = shootDelay == 0 && !ncpEnabled;
						final boolean extraMovement = viaVersionEnabled && getBoolean("Extra Movement");
						if (extraMovement && (multiPacket || bowMoveCount % 2 == 0)
								&& (!Lanius.mc.player.capabilities.isCreativeMode
										&& Lanius.mc.player.world.checkBlockCollision(playerBox
												.grow(EXPAND_VEC, EXPAND_VEC, EXPAND_VEC).expand(0.0D, -0.55D, 0.0D))
										|| Lanius.mc.player.capabilities.isCreativeMode)) {
							for (int count = 0; count < (multiPacket ? 20 : 1); count++) {
								NetHandlerHook.sendPlayerPacket(new CPacketPlayer(Lanius.mc.player.onGround));
							}
						}
						final int NORMAL_MIN = 3;
						if (multiPacket && extraMovement || bowMoveCount >= shootDelay
								+ (shootDelay < NORMAL_MIN && !(multiPacket && extraMovement) ? NORMAL_MIN - shootDelay
										: 0)
								+ (!viaVersionEnabled ? 1 : 0) + (ncpEnabled ? NetworkUtil.lagTicks() : 0)) {
							Lanius.mc.playerController.onStoppedUsingItem(Lanius.mc.player);
						}
					} else if (!executedEat && viaVersionEnabled
							&& InventoryUtil.isStackValid(Lanius.mc.player.inventory.getCurrentItem())
							&& (Lanius.mc.player.inventory.getCurrentItem().getItem() instanceof ItemFood
									|| Lanius.mc.player.inventory.getCurrentItem().getItem() instanceof ItemPotion)
							&& (!Lanius.mc.player.capabilities.isCreativeMode
									&& Lanius.mc.player.world.checkBlockCollision(playerBox
											.grow(EXPAND_VEC, EXPAND_VEC, EXPAND_VEC).expand(0.0D, -0.55D, 0.0D))
									|| Lanius.mc.player.capabilities.isCreativeMode)) {
						for (int count = 0; count < (ncpEnabled
								? Lanius.mc.player.inventory.getCurrentItem().getMaxItemUseDuration() * (700.0F
										/ (Lanius.mc.player.inventory.getCurrentItem().getMaxItemUseDuration() * 50.0F))
										- (ncpEnabled ? NetworkUtil.lagTicks() + 2 : 0)
								: Lanius.mc.player.inventory.getCurrentItem().getMaxItemUseDuration()); count++) {
							NetHandlerHook.sendPlayerPacket(new CPacketPlayer(Lanius.mc.player.onGround));
						}
						executedEat = true;
					}
				} else {
					executedEat = false;
				}
			}
			final boolean isHittingBlock = !Lanius.mc.player.capabilities.isCreativeMode
					&& Lanius.mc.playerController != null && (Boolean) ReflectHelper.getValue(PlayerControllerMP.class,
							Lanius.mc.playerController, SrgMappings.PlayerControllerMP_isHittingBlock),
					autoTool = getBoolean("Auto-tool");
			if (autoTool && isHittingBlock) {
				int bestSlotId = InventoryUtil.INIT_SLOT;
				float bestStrength = 1.0F;
				final IBlockState currentBlock = Lanius.mc.world
						.getBlockState((BlockPos) ReflectHelper.getValue(PlayerControllerMP.class,
								Lanius.mc.playerController, SrgMappings.PlayerControllerMP_currentBlock));
				for (int slotId = InventoryUtil.HOTBAR_BEGIN; slotId < InventoryUtil.MAX_SLOT; slotId++) {
					final ItemStack slotStack = Lanius.mc.player.inventoryContainer.getSlot(slotId).getStack();
					if (InventoryUtil.isStackValid(slotStack)) {
						float strength = slotStack.getItem().getDestroySpeed(slotStack, currentBlock);
						if (Lanius.mc.player.inventoryContainer.getSlot(slotId).getHasStack()
								&& strength > bestStrength) {
							bestSlotId = slotId;
							bestStrength = strength;
						}
					}
				}
				boolean hotbarFull = true;
				for (int slotId = InventoryUtil.HOTBAR_BEGIN; slotId < InventoryUtil.MAX_SLOT; slotId++) {
					if (!Lanius.mc.player.inventoryContainer.getSlot(slotId).getHasStack()) {
						hotbarFull = false;
						break;
					}
				}
				if (!hotbarFull) {
					for (int slotId = InventoryUtil.MIN_SLOT; slotId < InventoryUtil.HOTBAR_BEGIN; slotId++) {
						final ItemStack slotStack = Lanius.mc.player.inventoryContainer.getSlot(slotId).getStack();
						if (InventoryUtil.isStackValid(slotStack)) {
							float strength = slotStack.getItem().getDestroySpeed(slotStack, currentBlock);
							if (Lanius.mc.player.inventoryContainer.getSlot(slotId).getHasStack()
									&& strength > bestStrength) {
								bestSlotId = slotId;
								bestStrength = strength;
							}
						}
					}
				}
				if (bestSlotId != InventoryUtil.INIT_SLOT) {
					InventoryUtil.ensureInventory();
					if (bestSlotId < InventoryUtil.HOTBAR_BEGIN) {
						int bestItem = InventoryUtil.INIT_SLOT;
						for (int newSlotId = InventoryUtil.HOTBAR_BEGIN; newSlotId < InventoryUtil.MAX_SLOT; newSlotId++) {
							if (!Lanius.mc.player.inventoryContainer.getSlot(newSlotId).getHasStack()) {
								bestItem = newSlotId - InventoryUtil.HOTBAR_BEGIN;
								break;
							}
						}
						InventoryUtil.clickWindow(bestSlotId, 0, ClickType.QUICK_MOVE);
						if (bestItem != InventoryUtil.INIT_SLOT) {
							InventoryUtil.switchItem(bestItem);
						}
					} else {
						InventoryUtil.switchItem(bestSlotId - InventoryUtil.HOTBAR_BEGIN);
					}
				}
			}
			int hitDelay = getInt("Hit Delay").intValue();
			if ((Integer) ReflectHelper.getValue(PlayerControllerMP.class, Lanius.mc.playerController,
					SrgMappings.PlayerControllerMP_blockHitDelay) == 5 && hitDelay != 5) {
				ReflectHelper.setValue(PlayerControllerMP.class, Lanius.mc.playerController, hitDelay,
						SrgMappings.PlayerControllerMP_blockHitDelay);
			}
			if (isHittingBlock && (Float) ReflectHelper.getValue(PlayerControllerMP.class, Lanius.mc.playerController,
					SrgMappings.PlayerControllerMP_curBlockDamageMP) >= getFloat("Threshold").floatValue()) {
				ReflectHelper.setValue(PlayerControllerMP.class, Lanius.mc.playerController, 1.0F,
						SrgMappings.PlayerControllerMP_curBlockDamageMP);
			}
			setRightClickDelayTimer();
		}
	}

	@Override
	public void onExecute(final NetHandlerData data, final Phase phase) {
		if (!phase.equals(Phase.START)) {
			return;
		}
		if (isEnabled() && data.retVal instanceof CPacketPlayerTryUseItem
				&& InventoryUtil.isStackValid(Lanius.mc.player.inventory.getCurrentItem())
				&& Lanius.mc.player.inventory.getCurrentItem().getItem() == Items.BOW) {
			bowMoveCount = 0;
		} else if (data.retVal instanceof CPacketPlayerDigging || data.retVal instanceof CPacketHeldItemChange) {
			setMoveCount();
			executedEat = false;
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onLivingUpdateLowest(final LivingEvent.LivingUpdateEvent livingUpdateEv) {
		final EntityLivingBase livingEntity = livingUpdateEv.getEntityLiving();
		if (!livingEntity.equals(Lanius.mc.player) || livingEntity instanceof EntityPlayerMP
				|| !RoutineUtil.viaVersionEnabled()) {
			return;
		}
		HookManager.netHook.forcePlayerPacket();
	}

	@Override
	public void registerValues() {
		registerValue("Click Delay", 0, 0, 4,
				"Specifies how long to wait the player must wait before right-clicking again.");
		registerValue("Auto-tool", true, "Determines whether or not to switch to the player's best tool.");
		registerValue("Hit Delay", 0, 0, 5,
				"Specifies the delay that " + name() + " will wait until before hitting another block.");
		registerValue("Threshold", 0.7F, 0.7F, 1.0F, "Specifies the threshold of damage the block will break at.");
		registerValue("Extra Movement", true,
				"Determines whether or not to send extra packets to make " + name() + " faster.");
		registerValue("Shoot Delay", 20, 0, 20, "Specifies how long to wait before shooting a bow.");
	}

	private void setMoveCount() {
		bowMoveCount = -1;
	}

	private void setRightClickDelayTimer() {
		final int rightClickDelay = getInt("Click Delay").intValue();
		if (((Integer) ReflectHelper.getValue(Minecraft.class, Lanius.mc,
				SrgMappings.Minecraft_rightClickDelayTimer)) > rightClickDelay) {
			ReflectHelper.setValue(Minecraft.class, Lanius.mc, rightClickDelay,
					SrgMappings.Minecraft_rightClickDelayTimer);
		}
	}

}
