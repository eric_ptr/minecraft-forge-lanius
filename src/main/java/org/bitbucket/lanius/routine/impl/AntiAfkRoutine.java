package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.HookManager;
import org.bitbucket.lanius.hook.impl.NetHandlerData;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Phase;
import org.bitbucket.lanius.util.RoutineUtil;
import org.bitbucket.lanius.util.game.NetworkUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.block.material.Material;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public final class AntiAfkRoutine extends TabbedRoutine implements Hook<NetHandlerData> {
	private long afkStartTime;
	private double prevPosX, prevPosY, prevPosZ;
	private float prevServerYaw, prevServerPitch;

	public AntiAfkRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.PLAYER);
		init();
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NO_VIAVERSION, Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION);
	}

	@Override
	public String description() {
		return "Prevents your player from being considered AFK.";
	}

	@Override
	public void init() {
		afkStartTime = -1L;
		prevPosX = prevPosY = prevPosZ = -999.0D;
		prevServerYaw = prevServerPitch = -999.0F;
	}

	@Override
	public String name() {
		return "Anti-AFK";
	}

	@Override
	public void onExecute(NetHandlerData data, Phase phase) {
		if (!(data.retVal instanceof CPacketPlayer) || !phase.equals(Phase.START) || !isEnabled()
				|| Lanius.mc.isGamePaused() || Lanius.mc.isSingleplayer()) {
			return;
		}
		if (afkStartTime != -1L && prevPosX == Lanius.mc.player.posX
				&& prevPosY == Lanius.mc.player.getEntityBoundingBox().minY && prevPosZ == Lanius.mc.player.posZ
				&& prevServerYaw == HookManager.netHook.getServerYaw()
				&& prevServerPitch == HookManager.netHook.getServerPitch()) {
			if (getBoolean("Ignore Key Binds") || !Lanius.mc.gameSettings.keyBindAttack.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindUseItem.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindJump.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindForward.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindBack.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindLeft.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindRight.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindDrop.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindSprint.isKeyDown()
					&& !Lanius.mc.gameSettings.keyBindSwapHands.isKeyDown()
					&& !GameSettings.isKeyDown(Lanius.mc.gameSettings.keyBindSneak)) {
				int delay = getInt("Delay").intValue() - NetworkUtil.lagTime();
				if (System.currentTimeMillis() - afkStartTime > delay) {
					Material posMat = Lanius.mc.player.world
							.getBlockState(new BlockPos(Lanius.mc.player.posX,
									Lanius.mc.player.posY - JesusRoutine.BLOCK_OFF, Lanius.mc.player.posZ))
							.getMaterial();
					if (!Lanius.mc.player.capabilities.isFlying && !RoutineUtil.flyEnabled()
							&& (Lanius.mc.player.onGround || !Lanius.mc.player.world.getCollisionBoxes(Lanius.mc.player,
									Lanius.mc.player.getEntityBoundingBox().offset(SpeedRoutine.NO_MOVE,
											SpeedRoutine.GROUND_OFF, SpeedRoutine.NO_MOVE))
									.isEmpty())
							&& Lanius.mc.player.motionY < 0.0D && !Lanius.mc.player.isInWater()
							&& !Lanius.mc.player.isInLava()
							&& !((Boolean) ReflectHelper.getValue(Entity.class, Lanius.mc.player,
									SrgMappings.Entity_isInWeb))
							&& (!RoutineUtil.ncpEnabled() || Lanius.mc.player.capabilities.allowFlying
									|| !(RoutineUtil.enabled("Jesus")
											&& (posMat == Material.WATER || posMat == Material.LAVA)))
							&& !Lanius.mc.gameSettings.keyBindJump.isKeyDown()) {
						Lanius.mc.player.jump();
					}
					if (getBoolean("Rotate")) {
						HookManager.netHook
								.setServerYaw(MathHelper.wrapDegrees(HookManager.netHook.getServerYaw() + 5.0F));
						HookManager.netHook
								.setServerPitch(MathHelper.wrapDegrees(HookManager.netHook.getServerPitch() + 6.0F));
					}
				}
			}
		} else {
			afkStartTime = System.currentTimeMillis();
			prevPosX = Lanius.mc.player.posX;
			prevPosY = Lanius.mc.player.getEntityBoundingBox().minY;
			prevPosZ = Lanius.mc.player.posZ;
			prevServerYaw = HookManager.netHook.getServerYaw();
			prevServerPitch = HookManager.netHook.getServerPitch();
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onLivingUpdateLowest(final LivingEvent.LivingUpdateEvent livingUpdateEv) {
		final EntityLivingBase livingEntity = livingUpdateEv.getEntityLiving();
		if (!livingEntity.equals(Lanius.mc.player) || livingEntity instanceof EntityPlayerMP) {
			return;
		}
		HookManager.netHook.forcePlayerPacket();
	}

	@Override
	public void registerValues() {
		registerValue("Delay", 1000 * 60, 1000 * 10, 1000 * 60 * 10,
				"Specifies the time to wait before moving the player.");
		registerValue("Rotate", true,
				"Determines whether or not to rotate the player as well as move them when you go AFK.");
		registerValue("Ignore Key Binds", false,
				"Determines whether or not to ignore when the player has a key bind down and execute anyway.");
	}
}
