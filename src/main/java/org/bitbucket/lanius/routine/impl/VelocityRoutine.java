package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.impl.NetHandlerData;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Phase;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketEntityVelocity;
import net.minecraft.network.play.server.SPacketExplosion;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public final class VelocityRoutine extends TabbedRoutine implements Hook<NetHandlerData> {

	private float prevReduction;

	public VelocityRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.COMBAT);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Changes the player's velocity to the specified.";
	}

	@Override
	public void init() {
		prevReduction = -1.0F;
	}

	@Override
	public String name() {
		return "Velocity";
	}

	@SubscribeEvent
	public void onClientTick(final TickEvent.ClientTickEvent clientTickEv) {
		if (!getBoolean("No Collision") || !clientTickEv.phase.equals(TickEvent.Phase.END) || Lanius.mc.player == null
				|| prevReduction == -1.0F) {
			return;
		}
		Lanius.mc.player.entityCollisionReduction = prevReduction;
		prevReduction = -1.0F;
	}

	@Override
	public void onExecute(final NetHandlerData data, final Phase phase) {
		if (!phase.equals(Phase.START) || !isEnabled() || Lanius.mc.world == null || Lanius.mc.player == null) {
			return;
		}
		final float velocityH = getFloat("H-multiplier").floatValue(),
				velocityY = getFloat("Y-multiplier").floatValue();
		if (data.retVal instanceof SPacketEntityVelocity) {
			SPacketEntityVelocity velocityPacket = (SPacketEntityVelocity) data.retVal;
			Entity entity = Lanius.mc.world.getEntityByID(velocityPacket.getEntityID());
			if (entity == null || !entity.equals(Lanius.mc.player)) {
				return;
			}
			data.retVal = velocityH == 0.0F && velocityY == 0.0F ? null
					: new SPacketEntityVelocity(velocityPacket.getEntityID(),
							velocityPacket.getMotionX() / 8000.0D * velocityH,
							velocityPacket.getMotionY() / 8000.0D * velocityY,
							velocityPacket.getMotionZ() / 8000.0D * velocityH);
		} else if (data.retVal instanceof SPacketExplosion && getBoolean("Explosions")) {
			SPacketExplosion explosionPacket = (SPacketExplosion) data.retVal;
			data.retVal = velocityH == 0.0F && velocityY == 0.0F ? null
					: new SPacketExplosion(explosionPacket.getX(), explosionPacket.getY(), explosionPacket.getZ(),
							explosionPacket.getStrength(), explosionPacket.getAffectedBlockPositions(),
							new Vec3d(explosionPacket.getMotionX() * velocityH,
									explosionPacket.getMotionY() * velocityY,
									explosionPacket.getMotionZ() * velocityH));
		}
	}

	@SubscribeEvent
	public void onLivingUpdate(final LivingEvent.LivingUpdateEvent livingUpdateEv) {
		final EntityLivingBase entityLiving = livingUpdateEv.getEntityLiving();
		if (!getBoolean("No Collision") || !entityLiving.equals(Lanius.mc.player)
				|| entityLiving instanceof EntityPlayerMP) {
			return;
		}
		prevReduction = entityLiving.entityCollisionReduction;
		entityLiving.entityCollisionReduction = 1.0F;
	}

	@Override
	public void registerValues() {
		registerValue("H-multiplier", 0.0F, -9.0F, 9.0F, "Specifies how much to multiply received h-velocity by.");
		registerValue("Y-multiplier", 0.0F, -9.0F, 9.0F, "Specifies how much to multiply received y-velocity by.");
		registerValue("No Collision", true, "Determines whether or not to prevent entity collision.");
		registerValue("Explosions", true, "Determines whether or not to handle explosion velocity.");
	}

}
