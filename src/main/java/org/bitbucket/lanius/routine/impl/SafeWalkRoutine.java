package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.impl.EdgeWalkData;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Phase;
import org.bitbucket.lanius.util.RoutineUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public final class SafeWalkRoutine extends TabbedRoutine implements Hook<EdgeWalkData> {
	private static class GroundData {
		public final double groundDist;
		public final int groundY;

		public GroundData(double groundDist, int groundY) {
			this.groundDist = groundDist;
			this.groundY = groundY;
		}
	}

	private boolean jumping, inWeb;
	private double jumpGroundY = -999.0D;
	private float prevStepHeight = 0.5F;

	public SafeWalkRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.MOVEMENT);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Prevents the player from being able to fall off edges.";
	}

	private GroundData getGroundInfo(Entity entity, double motionX, double motionZ) {
		int groundY = MathHelper.floor(entity.posY - 0.20000000298023224D);
		for (; Lanius.mc.world
				.getBlockState(new BlockPos(MathHelper.floor(entity.posX + motionX), groundY,
						MathHelper.floor(entity.posZ + motionZ)))
				.getCollisionBoundingBox(Lanius.mc.world,
						new BlockPos(MathHelper.floor(entity.posX + motionX), groundY,
								MathHelper.floor(entity.posZ + motionZ))) == null
				|| Lanius.mc.world
						.getBlockState(new BlockPos(MathHelper.floor(entity.posX + motionX), groundY,
								MathHelper.floor(entity.posZ + motionZ)))
						.getCollisionBoundingBox(Lanius.mc.world, new BlockPos(MathHelper.floor(entity.posX + motionX),
								groundY, MathHelper.floor(entity.posZ + motionZ)))
						.equals(Block.NULL_AABB); groundY--) {
			if (groundY < 0) {
				break;
			}
		}
		AxisAlignedBB groundBox = Lanius.mc.world
				.getBlockState(new BlockPos(MathHelper.floor(entity.posX + motionX), groundY,
						MathHelper.floor(entity.posZ + motionZ)))
				.getCollisionBoundingBox(Lanius.mc.world, new BlockPos(MathHelper.floor(entity.posX + motionX), groundY,
						MathHelper.floor(entity.posZ + motionZ)));
		return new GroundData(Math.abs(groundY
				+ (groundBox == null || groundBox.equals(Block.NULL_AABB) ? 1.0D : groundBox.maxY - groundBox.minY)
				- entity.posY), groundY + 1);
	}

	@Override
	public void init() {
		jumping = inWeb = false;
		jumpGroundY = -999.0D;
		prevStepHeight = 0.5F;
	}

	@Override
	public String name() {
		return "Safe Walk";
	}

	@Override
	public void onExecute(EdgeWalkData data, Phase phase) {
		if (!isEnabled() || Lanius.mc.player == null || Lanius.mc.player.movementInput == null
				|| RoutineUtil.enabled("Freecam") || !data.source.equals(Lanius.mc.player)
				|| data.source instanceof EntityPlayerMP || data.type != MoverType.SELF && data.type != MoverType.PLAYER
				|| data.source.onGround && data.source.isSneaking()) {
			return;
		}
		if (phase.equals(Phase.START)) {
			prevStepHeight = data.source.stepHeight;
			double distThreshold = getFloat("Distance Threshold").floatValue();
			// Eric: distance check:
			if (jumping) {
				GroundData groundData = getGroundInfo(data.source, 0.0D, 0.0D);
				if (groundData.groundY >= jumpGroundY) {
					distThreshold = Math.max(distThreshold, groundData.groundDist);
				}
			}
			if (((SpeedRoutine) Lanius.getInstance().getRoutineRegistry().get("Speed"))
					.executingNcpSpeed((EntityLivingBase) data.source)) {
				distThreshold = Math.max(distThreshold, 1.0F); // Eric: 1.0 seems to be the maximum bhop height.
			}
			if (getGroundInfo(data.source, data.x, data.z).groundDist <= distThreshold) {
				return;
			}

			// Eric: vanilla checks:
			EntityPlayerSP player = (EntityPlayerSP) data.source;
			if (player.capabilities.isFlying || player.isElytraFlying() || player.isRiding() || player.isInLava()
					|| player.isInWater() || inWeb || player.isSpectator()
					|| !getBoolean("Creative") && player.capabilities.isCreativeMode) {
				return;
			}

			// Eric: routine checks:
			if (RoutineUtil.flyEnabled() || RoutineUtil.noclipEnabled()
					|| ((LongJumpRoutine) Lanius.getInstance().getRoutineRegistry().get("Long Jump")).isJumped()) {
				return;
			}

			// Eric: routine configuration checks:
			data.retVal = getBoolean("Forward") && player.movementInput.moveForward > 0.0F
					|| getBoolean("Backward")
							&& player.movementInput.moveForward < 0.0F
					|| getBoolean("Left") && player.movementInput.moveStrafe > 0.0F
					|| getBoolean("Right") && player.movementInput.moveStrafe < 0.0F
					|| getBoolean("On Ground") && (player.onGround || !player.world
							.getCollisionBoxes(player,
									player.getEntityBoundingBox().offset(0.0D, SpeedRoutine.GROUND_OFF, 0.0D))
							.isEmpty())
					|| getBoolean("Sprint") && Lanius.mc.gameSettings.keyBindSprint.isKeyDown()
					|| getBoolean("Jump") && jumping || getBoolean("Creative") && player.capabilities.isCreativeMode
					|| player.movementInput.moveForward == 0.0F && player.movementInput.moveStrafe == 0.0F;
			if (data.retVal && shouldFixStepHeight()) {
				player.stepHeight = Math.min(0.0625F, player.stepHeight); // Sets stepHeight to a value close to zero to
																			// handle 0.5 block-safe-walk correctly.
																			// It can't be zero as it glitches out when
																			// you step up blocks otherwise.
			}
		} else if (shouldFixStepHeight()) {
			data.source.stepHeight = prevStepHeight;
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onLivingJumpLowest(LivingJumpEvent event) {
		EntityLivingBase entityLiving = event.getEntityLiving();
		if (!entityLiving.equals(Lanius.mc.player) || entityLiving instanceof EntityPlayerMP) {
			return;
		}
		if (entityLiving.motionY > 0.0D) {
			jumping = true;
			jumpGroundY = entityLiving.posY;
		}
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onLivingUpdateLowest(LivingEvent.LivingUpdateEvent event) {
		if (Lanius.mc.player == null || Lanius.mc.player.movementInput == null || RoutineUtil.enabled("Freecam")) {
			return;
		}
		EntityLivingBase entityLiving = event.getEntityLiving();
		if (!entityLiving.equals(Lanius.mc.player) || entityLiving instanceof EntityPlayerMP) {
			return;
		}
		if ((entityLiving.onGround
				|| !entityLiving.world
						.getCollisionBoxes(entityLiving,
								entityLiving.getEntityBoundingBox().offset(0.0D, SpeedRoutine.GROUND_OFF, 0.0D))
						.isEmpty())
				&& entityLiving.motionY <= 0.0D || entityLiving.isInWater() || entityLiving.isInLava()
				|| RoutineUtil.flyEnabled() || RoutineUtil.noclipEnabled() || Lanius.mc.player.capabilities.isFlying
				|| entityLiving.isElytraFlying() || entityLiving.isRiding()) {
			jumping = false;
		}
		// Need to get isInWeb here because it gets unset in the move method of Entity:
		inWeb = ReflectHelper.getValue(Entity.class, entityLiving, SrgMappings.Entity_isInWeb);
	}

	@Override
	public void registerValues() {
		registerValue("Forward", true,
				"Determines whether or not to safe walk when you are inputting forward movement.");
		registerValue("Backward", true,
				"Determines whether or not to safe walk when you are inputting backward movement.");
		registerValue("Right", true, "Determines whether or not to safe walk when you are inputting right movement.");
		registerValue("Left", true, "Determines whether or not to safe walk when you are inputting left movement.");
		registerValue("Jump", true, "Determines whether or not to safe walk when the player is jumping.");
		registerValue("On Ground", true,
				"Determines whether or not to safe walk when the player is touching the ground.");
		registerValue("Sprint", true, "Determines whether or not to safe walk when you are trying to sprint.");
		registerValue("Creative", true, "Determines whether or not to safe walk when the player is in Creative mode.");
		registerValue("Distance Threshold", 3.0F, 0.0F, 30.0F,
				"Specifies the minimum threshold of the distance from the ground the player may fall off of.");
	}

	private boolean shouldFixStepHeight() {
		return getFloat("Distance Threshold").floatValue() <= 0.5F;
	}
}
