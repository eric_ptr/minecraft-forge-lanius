package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.impl.ItemValidData;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Phase;
import org.bitbucket.lanius.util.game.InventoryUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

public final class CreativeArmorRoutine extends TabbedRoutine implements Hook<ItemValidData> {

	public CreativeArmorRoutine() {
		super(Keyboard.KEY_NONE, true, Tab.PLAYER);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Allows you to equip any item as armor.";
	}

	@Override
	public String name() {
		return "Creative Armor";
	}

	@Override
	public void onExecute(ItemValidData data, Phase phase) {
		if (phase.equals(Phase.START) && isEnabled() && Lanius.mc.player.capabilities.isCreativeMode
				&& InventoryUtil.isStackValid(data.getStack())) {
			data.retVal = true;
		}
	}

}
