package org.bitbucket.lanius.routine.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cfg.Configurable;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.HookManager;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Priority;
import org.bitbucket.lanius.util.RoutineUtil;
import org.bitbucket.lanius.util.game.InventoryUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.client.gui.inventory.GuiChest;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public final class ChestStealerRoutine extends TabbedRoutine implements Configurable {
	private class StealableItem {
		private final Item item;
		private Priority priority;

		private StealableItem(Item item, Priority priority) {
			this.item = item;
			this.priority = priority;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof StealableItem)) {
				return false;
			}
			StealableItem stealable = (StealableItem) obj;
			return stealable.item.equals(item);
		}

		@Override
		public int hashCode() {
			return item.hashCode();
		}

		@Override
		public String toString() {
			return "'" + item.getRegistryName() + "':" + priority;
		}
	}

	private static final File prioritiesFile = new File(Lanius.dataDir, "chest_stealer.cfg");
	private static final Pattern prioritiesRegex = Pattern.compile("'(.+)':(.+)");
	private BlockPos lastClickedChest;
	private final List<Slot> clickedSlots = new ArrayList<>();
	private int stolenItemCount;
	private long lastMovedSlotTime = -1L;
	private boolean closeContainerNextTick;
	private final List<StealableItem> itemPriorities = new ArrayList<>();

	public ChestStealerRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.WORLD);
	}

	public boolean addItemPriority(Item item, Priority priority) {
		if (getStealableForItem(item) != null) {
			return false;
		}
		return itemPriorities.add(new StealableItem(item, priority));
	}

	public void clearItemPriorities() {
		itemPriorities.clear();
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NO_VIAVERSION, Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION);
	}

	@Override
	public String description() {
		return "Automatically steals items from chests.";
	}

	private void execAutoClose(Slot slot, List<Slot> slots) {
		Slot lastSlot = InventoryUtil.calcLastSlotAgainstInventory(slots, Lanius.mc.player.inventory, true);
		if (getBoolean("Auto-close") && (lastSlot == null || slot.equals(lastSlot) || !shouldExecute())) {
			closeContainerNextTick = true;
		}
	}

	private void execAutoOpen() {
		final BlockPos mouseOverPos;
		if (lastClickedChest == null && shouldExecute() && getBoolean("Auto-open") && Lanius.mc.objectMouseOver != null
				&& !Lanius.mc.player.isRowingBoat()
				&& Lanius.mc.objectMouseOver.typeOfHit.equals(RayTraceResult.Type.BLOCK)
				&& !Lanius.mc.world.isAirBlock(mouseOverPos = Lanius.mc.objectMouseOver.getBlockPos())
				&& Lanius.mc.world.getWorldBorder().contains(mouseOverPos) && isValidChest(mouseOverPos)
				&& !RoutineUtil.enabled("Nuker") && !RoutineUtil.enabled("Auto-break")) {
			for (final EnumHand hand : ViaVersionRoutine.getValidHands()) {
				if (Lanius.mc.playerController.processRightClickBlock(Lanius.mc.player, Lanius.mc.world, mouseOverPos,
						Lanius.mc.objectMouseOver.sideHit, Lanius.mc.objectMouseOver.hitVec,
						hand) == EnumActionResult.SUCCESS) {
					if (getBoolean("Swing")) {
						Lanius.mc.player.swingArm(EnumHand.MAIN_HAND);
					}
					if (HookManager.netHook.isUseSent()) {
						lastClickedChest = mouseOverPos;
					}
					break;
				}
			}
		} else if (Lanius.mc.objectMouseOver == null
				|| (!Lanius.mc.objectMouseOver.typeOfHit.equals(RayTraceResult.Type.BLOCK)
						|| !isValidChest(Lanius.mc.objectMouseOver.getBlockPos())
						|| lastClickedChest != null
								&& !Lanius.mc.objectMouseOver.getBlockPos().equals(lastClickedChest))
				|| Lanius.mc.world.isAirBlock(Lanius.mc.objectMouseOver.getBlockPos())
				|| !Lanius.mc.world.getWorldBorder().contains(Lanius.mc.objectMouseOver.getBlockPos())) {
			lastClickedChest = null;
		}
	}

	private void execStealing(GuiChest chestUi) {
		IInventory upperInv = ReflectHelper.getValue(GuiChest.class, chestUi, SrgMappings.GuiChest_upperChestInventory);
		boolean verify = getBoolean("Verify");
		if (!shouldExecute() || verify && (lastClickedChest == null || upperInv != null && upperInv.hasCustomName())) {
			return;
		}
		List<Slot> sortedSlots = InventoryUtil.getSortedSlots(Lanius.mc.player.openContainer),
				invSlots = InventoryUtil.getSortedSlots(Lanius.mc.player.inventoryContainer);
		if (getBoolean("Prioritize Valuables")) {
			Collections.sort(sortedSlots, (o1, o2) -> {
				Priority p1 = null, p2 = null;
				if (!o1.getHasStack()) {
					p1 = Priority.LOWEST;
				}
				if (!o2.getHasStack()) {
					p2 = Priority.LOWEST;
				}
				if (p1 == null) {
					StealableItem stealable1 = getStealableForItem(o1.getStack().getItem());
					if (stealable1 == null) {
						p1 = Priority.LOWEST;
					} else {
						p1 = stealable1.priority;
					}
				}
				if (p2 == null) {
					StealableItem stealable2 = getStealableForItem(o2.getStack().getItem());
					if (stealable2 == null) {
						p2 = Priority.LOWEST;
					} else {
						p2 = stealable2.priority;
					}
				}
				return p1.compareTo(p2);
			});
		}
		for (Slot slot : sortedSlots) {
			if (slot.isHere(Lanius.mc.player.inventory, slot.getSlotIndex())) {
				continue;
			}
			if (RoutineUtil.ncpEnabled() && clickedSlots.contains(slot) || !slot.getHasStack()
					|| InventoryUtil.isInventoryFull(false)
							&& !InventoryUtil.isStackInSlotsAndNotFull(slot.getStack(), invSlots)) {
				execAutoClose(slot, sortedSlots);
				continue;
			}
			if (stolenItemCount < getInt("Item Stack Limit").intValue()) {
				if (lastMovedSlotTime == -1L) {
					lastMovedSlotTime = System.currentTimeMillis();
				}
				if (System.currentTimeMillis() - lastMovedSlotTime >= getInt("Delay").intValue()) {
					if (getBoolean("Drop")) {
						InventoryUtil.clickWindowOpenContainer(slot.slotNumber, 1, ClickType.THROW);
					} else {
						InventoryUtil.clickWindowOpenContainer(slot.slotNumber, 0, ClickType.QUICK_MOVE);
					}
					lastMovedSlotTime = System.currentTimeMillis();
					clickedSlots.add(slot);
					++stolenItemCount;
					execAutoClose(slot, sortedSlots);
					if (RoutineUtil.ncpEnabled()) {
						break;
					}
				}
			} else if (getBoolean("Auto-close")) {
				closeContainerNextTick = true;
			}
		}
	}

	public StealableItem getStealableForItem(Item item) {
		for (StealableItem stealable : itemPriorities) {
			if (stealable.item.equals(item)) {
				return stealable;
			}
		}
		return null;
	}

	@Override
	public void init() {
		lastClickedChest = null;
		clickedSlots.clear();
		lastMovedSlotTime = -1L;
		stolenItemCount = 0;
		closeContainerNextTick = false;
	}

	private boolean isValidChest(BlockPos pos) {
		return Lanius.mc.world.getBlockState(pos).getBlock().equals(Blocks.CHEST)
				|| getBoolean("Trapped Chests")
						&& Lanius.mc.world.getBlockState(pos).getBlock().equals(Blocks.TRAPPED_CHEST)
				|| getBoolean("Ender Chests")
						&& Lanius.mc.world.getBlockState(pos).getBlock().equals(Blocks.ENDER_CHEST);
	}

	@Override
	public void load() {
		clearItemPriorities();
		if (!prioritiesFile.exists()) {
			List<CreativeTabs> priorityTabs = new ArrayList<>();
			priorityTabs.add(CreativeTabs.COMBAT);
			priorityTabs.add(CreativeTabs.TOOLS);
			priorityTabs.add(CreativeTabs.BREWING);
			priorityTabs.add(CreativeTabs.FOOD);
			priorityTabs.add(CreativeTabs.MISC);
			Iterator<Item> itemIt = Item.REGISTRY.iterator();
			while (itemIt.hasNext()) {
				Item item = itemIt.next();
				if (priorityTabs.contains(item.getCreativeTab())) {
					int priorityIdx;
					for (priorityIdx = 0; priorityIdx < priorityTabs.size(); priorityIdx++) {
						if (priorityTabs.get(priorityIdx).equals(item.getCreativeTab())) {
							break;
						}
					}
					addItemPriority(item, Priority.values()[priorityIdx]);
				}
			}
			return;
		}
		try (BufferedReader in = new BufferedReader(new FileReader(prioritiesFile))) {
			String line;
			while ((line = in.readLine()) != null) {
				// TODO(Eric) make regex usage like this centralized in some class file (the
				// Pattern and File object usages in Configurable instances).
				Matcher fieldMatcher = prioritiesRegex.matcher(line);
				if (!fieldMatcher.matches()) {
					System.err.println("Skipping bad entry in file " + prioritiesFile + ": \"" + line + "\"");
					continue;
				}
				Item item = Item.getByNameOrId(fieldMatcher.group(1));
				if (item == null) {
					System.err.println(
							"Skipping bad item in file " + prioritiesFile + ": \"" + fieldMatcher.group(1) + "\"");
					continue;
				}
				addItemPriority(item, Priority.getById(fieldMatcher.group(2)));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String name() {
		return "Chest Stealer";
	}

	@SubscribeEvent
	public void onClientTick(final TickEvent.ClientTickEvent event) {
		if (!event.phase.equals(TickEvent.Phase.END) || Lanius.mc.player == null || Lanius.mc.isGamePaused()) {
			return;
		}

		// Eric: closes the chest on the next tick to avoid inventory desync:
		if (closeContainerNextTick && Lanius.mc.currentScreen instanceof GuiChest) {
			closeContainerNextTick = false;
			Lanius.mc.player.closeScreen();
		}

		if (Lanius.mc.currentScreen == null) {
			clickedSlots.clear();
			stolenItemCount = 0;
			execAutoOpen();
		} else if (Lanius.mc.currentScreen instanceof GuiChest) {
			execStealing((GuiChest) Lanius.mc.currentScreen);
		}
	}

	@SubscribeEvent
	public void onRightClickBlock(final PlayerInteractEvent.RightClickBlock event) {
		final EntityPlayer entityPlayer = event.getEntityPlayer();
		if (!entityPlayer.equals(Lanius.mc.player) || entityPlayer instanceof EntityPlayerMP) {
			return;
		}
		if (isValidChest(event.getPos())) {
			lastClickedChest = event.getPos();
		}
	}

	@Override
	public void registerValues() {
		registerValue("Delay", 0, 0, 10000, "Specifies the time to wait before stealing another item.");
		registerValue("Drop", false, "Determines whether or not to drop the items.");
		registerValue("Prioritize Valuables", true, "Determines whether or not to prioritize valuable items.");
		registerValue("Auto-open", false,
				"Determines whether or not to automatically open the chest your player is looking at.");
		registerValue("Verify", true, "Determines whether or not to verify that an opened chest is indeed a chest.");
		registerValue("Auto-close", false,
				"Determines whether or not to automatically close an opened chest after stealing from it.");
		registerValue("Item Stack Limit", 100, 1, 100, "Specifies the limit on items to steal from a chest.");
		registerValue("Swing", true, "Determines whether or not to swing the player's item.");
		registerValue("Ender Chests", false, "Determines whether or not to steal from ender chests.");
		registerValue("Trapped Chests", false, "Determines whether or not to steal from trapped chests.");
	}

	public boolean removeItemPriority(Item item) {
		StealableItem stealable = getStealableForItem(item);
		if (stealable == null) {
			return false;
		}
		return itemPriorities.remove(stealable);
	}

	@Override
	public void save() {
		prioritiesFile.delete();
		try (PrintWriter out = new PrintWriter(new BufferedWriter((new FileWriter(prioritiesFile))))) {
			for (StealableItem entry : itemPriorities) {
				out.println(entry);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private boolean shouldExecute() {
		return !InventoryUtil.isInventoryFull(true) || getBoolean("Drop");
	}
}
