package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.impl.DoRenderData;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Phase;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.client.renderer.entity.RenderEntityItem;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.entity.passive.IAnimals;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public final class NoRenderRoutine extends TabbedRoutine implements Hook<DoRenderData> {
	public NoRenderRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.RENDER);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Prevents animals, items, and monsters from rendering.";
	}

	@Override
	public String name() {
		return "No Render";
	}

	@Override
	public void onExecute(final DoRenderData data, final Phase phase) {
		if (!isEnabled() || !phase.equals(Phase.START) || !getBoolean("Items")
				|| !(data.source instanceof RenderEntityItem)) {
			return;
		}
		data.retVal = true;
	}

	@SubscribeEvent
	public void onRenderLivingPre(final RenderLivingEvent.Pre<EntityLivingBase> renderLivingPreEv) {
		final EntityLivingBase entity = renderLivingPreEv.getEntity();
		if (getBoolean("Animals") && entity instanceof IAnimals || getBoolean("Mobs") && entity instanceof IMob) {
			renderLivingPreEv.setCanceled(true);
		}
	}

	@Override
	public void registerValues() {
		registerValue("Animals", false, "Determines whether or not to prevent animals from rendering.");
		registerValue("Items", true, "Determines whether or not to prevent items from rendering.");
		registerValue("Mobs", false, "Determines whether or not to prevent mobs from rendering.");
	}

}
