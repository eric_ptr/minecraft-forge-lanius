package org.bitbucket.lanius.routine.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.RoutineUtil;
import org.bitbucket.lanius.util.game.InventoryUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemShield;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class AutoArmorRoutine extends TabbedRoutine {
	private static class StackInSlot {
		private final ItemStack stack;
		private final Slot slot;

		private StackInSlot(ItemStack stack, Slot slot) {
			this.stack = stack;
			this.slot = slot;
		}

		@Override
		public boolean equals(Object obj) {
			if (!(obj instanceof StackInSlot)) {
				return false;
			}
			StackInSlot other = (StackInSlot) obj;
			return stack == other.stack && slot.slotNumber == other.slot.slotNumber;
		}

		@Override
		public int hashCode() {
			return Objects.hash(stack, slot.slotNumber);
		}
	}

	private final Map<EntityEquipmentSlot, StackInSlot> bestDefenseItems = new HashMap<>();

	private final List<EntityEquipmentSlot> clickedSlots = new ArrayList<>();

	private long clickStartTime = -1L;

	public AutoArmorRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.COMBAT);
	}

	private int compareArmor(ItemStack armor1, ItemStack armor2) {
		if (armor1 == null || armor2 == null) {
			return armor1 == null && armor2 == null ? 0 : armor1 != null ? -1 : 1;
		}
		if (!(armor1.getItem() instanceof ItemArmor) || !(armor2.getItem() instanceof ItemArmor)) {
			return !(armor1.getItem() instanceof ItemArmor) && !(armor2.getItem() instanceof ItemArmor) ? 0
					: armor1.getItem() instanceof ItemArmor ? -1 : 1;
		}
		ItemArmor armorItem1 = (ItemArmor) armor1.getItem(), armorItem2 = (ItemArmor) armor2.getItem();
		if (!armorItem1.getEquipmentSlot().equals(armorItem2.getEquipmentSlot())) {
			return 0;
		}
		int materialCmp = InventoryUtil.compareMaterials(armorItem1.getArmorMaterial(), armorItem2.getArmorMaterial());
		if (materialCmp == 0 && armor1.isItemEnchanted() != armor2.isItemEnchanted()) {
			return armor1.isItemEnchanted() ? -1 : 1;
		}
		if (materialCmp == 0 && armor1.getItemDamage() != armor2.getItemDamage()) {
			return Integer.compare(armor1.getItemDamage(), armor2.getItemDamage());
		}
		return materialCmp;
	}

	private int compareShields(ItemStack shield1, ItemStack shield2) {
		if (shield1 == null || shield2 == null) {
			return shield1 == null && shield2 == null ? 0 : shield1 != null ? -1 : 1;
		}
		if (!(shield1.getItem() instanceof ItemShield) || !(shield2.getItem() instanceof ItemShield)) {
			return !(shield1.getItem() instanceof ItemShield) && !(shield2.getItem() instanceof ItemShield) ? 0
					: shield1.getItem() instanceof ItemShield ? -1 : 1;
		}
		if (shield1.isItemEnchanted() != shield2.isItemEnchanted()) {
			return shield1.isItemEnchanted() ? -1 : 1;
		}
		return Integer.compare(shield1.getItemDamage(), shield2.getItemDamage());
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NO_VIAVERSION, Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION);
	}

	@Override
	public String description() {
		return "Automatically equips the player's best armor.";
	}

	private boolean handleEquip(Slot srcSlot, Slot dstSlot) {
		boolean moveSrcItem = !dstSlot.getHasStack();
		if (!moveSrcItem) {
			if (getBoolean("Drop")) {
				InventoryUtil.clickWindowOpenContainer(dstSlot.slotNumber, 1, ClickType.THROW);
				if (RoutineUtil.ncpEnabled()) {
					return false;
				}
				moveSrcItem = true;
			} else if (!InventoryUtil.isInventoryFull(false)) {
				InventoryUtil.clickWindowOpenContainer(dstSlot.slotNumber, 0, ClickType.QUICK_MOVE);
				if (RoutineUtil.ncpEnabled()) {
					return false;
				}
				moveSrcItem = true;
			}
		}
		if (moveSrcItem) {
			InventoryUtil.clickWindowOpenContainer(srcSlot.slotNumber, 0, ClickType.QUICK_MOVE);
		}
		return true;
	}

	@Override
	public void init() {
		bestDefenseItems.clear();
		clickedSlots.clear();
		clickStartTime = -1L;
	}

	@Override
	public String name() {
		return "Auto-armor";
	}

	@SubscribeEvent
	public void onClientTick(final TickEvent.ClientTickEvent event) {
		if (!event.phase.equals(TickEvent.Phase.END) || Lanius.mc.player == null || Lanius.mc.isGamePaused()
				|| Lanius.mc.player.inventoryContainer != Lanius.mc.player.openContainer) {
			return;
		}
		bestDefenseItems.clear();
		List<Slot> sortedSlots = InventoryUtil.getSortedSlots(Lanius.mc.player.openContainer);
		for (Slot slot : sortedSlots) {
			if (!slot.getHasStack() || InventoryUtil.isSlotDefenseEquipment(slot.slotNumber)) {
				continue;
			}
			ItemStack stack = slot.getStack();
			if (stack.getItem() instanceof ItemArmor) {
				ItemArmor armor = (ItemArmor) stack.getItem();
				Slot targetSlot = Lanius.mc.player.openContainer
						.getSlot(InventoryUtil.convertEquipSlotToSlotNum(armor.getEquipmentSlot()));
				if (targetSlot.getSlotIndex() != slot.getSlotIndex()
						&& compareArmor(stack, targetSlot.getStack()) < 0) {
					putBestDefenseItem(slot);
				}
			} else if (stack.getItem() instanceof ItemShield && getBoolean("Equip Shield")) {
				Slot targetSlot = Lanius.mc.player.openContainer
						.getSlot(InventoryUtil.convertEquipSlotToSlotNum(EntityEquipmentSlot.OFFHAND));
				if (targetSlot.getSlotIndex() != slot.getSlotIndex()
						&& compareShields(stack, targetSlot.getStack()) < 0) {
					putBestDefenseItem(slot);
				}
			}
		}
		if (!RoutineUtil.ncpEnabled()) {
			clickedSlots.clear();
			for (Map.Entry<EntityEquipmentSlot, StackInSlot> bestDefenseItem : bestDefenseItems.entrySet()) {
				handleEquip(bestDefenseItem.getValue().slot, Lanius.mc.player.openContainer
						.getSlot(InventoryUtil.convertEquipSlotToSlotNum(bestDefenseItem.getKey())));
			}
		} else if (clickStartTime == -1L || System.currentTimeMillis() - clickStartTime >= getInt("Delay").intValue()) {
			boolean clickedSlot = false;
			for (Map.Entry<EntityEquipmentSlot, StackInSlot> bestDefenseItem : bestDefenseItems.entrySet()) {
				if (clickedSlots.contains(bestDefenseItem.getKey())) {
					continue;
				}
				if (handleEquip(bestDefenseItem.getValue().slot, Lanius.mc.player.openContainer
						.getSlot(InventoryUtil.convertEquipSlotToSlotNum(bestDefenseItem.getKey())))) {
					clickedSlots.add(bestDefenseItem.getKey());
				}
				clickStartTime = System.currentTimeMillis();
				clickedSlot = true;
				break;
			}
			if (!clickedSlot) {
				clickedSlots.clear();
			}
		}
	}

	private void putBestDefenseItem(Slot srcSlot) {
		ItemStack srcStack = srcSlot.getStack();
		if (srcStack.getItem() instanceof ItemArmor) {
			ItemArmor armor = (ItemArmor) srcStack.getItem();
			StackInSlot prevStack = bestDefenseItems.get(armor.getEquipmentSlot());
			if (prevStack == null || compareArmor(srcStack, prevStack.stack) < 0) {
				bestDefenseItems.put(armor.getEquipmentSlot(), new StackInSlot(srcStack, srcSlot));
			}
		} else if (srcStack.getItem() instanceof ItemShield) {
			StackInSlot prevStack = bestDefenseItems.get(EntityEquipmentSlot.OFFHAND);
			if (prevStack == null || compareShields(srcStack, prevStack.stack) < 0) {
				bestDefenseItems.put(EntityEquipmentSlot.OFFHAND, new StackInSlot(srcStack, srcSlot));
			}
		}
	}

	@Override
	public void registerValues() {
		registerValue("Delay", 0, 0, 10000, "Specifies the time to wait before equipping another armor piece.");
		registerValue("Drop", true, "Determines whether or not to drop lesser armor from the equipment slots.");
		registerValue("Equip Shield", true, "Determines whether or not to equip a better shield.");
	}
}
