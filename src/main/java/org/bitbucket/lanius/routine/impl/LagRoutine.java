package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.game.NetworkUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

public final class LagRoutine extends TabbedRoutine {

	public LagRoutine() {
		super(Keyboard.KEY_NONE, true, Tab.MISCELLANEOUS);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Puts " + Lanius.NAME + " into its lag compensation mode.";
	}

	@Override
	public String displayData() {
		return String.valueOf(NetworkUtil.lagTime());
	}

	@Override
	public String name() {
		return "Lag Compensation";
	}

}
