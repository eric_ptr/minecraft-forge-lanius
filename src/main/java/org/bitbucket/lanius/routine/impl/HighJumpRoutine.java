package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderGameOverlayEvent.ElementType;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingJumpEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public final class HighJumpRoutine extends TabbedRoutine {
	public HighJumpRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.MOVEMENT);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Makes the player and ridden horses jump higher.";
	}

	@Override
	public String name() {
		return "High Jump";
	}

	@SubscribeEvent
	public void onLivingJump(final LivingJumpEvent livingJumpEv) {
		final EntityLivingBase entityLiving = livingJumpEv.getEntityLiving();
		if (!entityLiving.equals(Lanius.mc.player) || entityLiving instanceof EntityPlayerMP) {
			return;
		}
		entityLiving.motionY *= getFloat("Multiplier").floatValue();
	}

	@SubscribeEvent
	public void onLivingUpdate(final LivingEvent.LivingUpdateEvent livingUpdateEv) {
		final EntityLivingBase entityLiving = livingUpdateEv.getEntityLiving();
		if (!entityLiving.equals(Lanius.mc.player) || entityLiving instanceof EntityPlayerMP) {
			return;
		}
		setupHorseJumping();
	}

	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onRenderGameOverlayPreLowest(final RenderGameOverlayEvent.Pre event) {
		if (event.getType().equals(ElementType.JUMPBAR) && !event.isCanceled()) {
			setupHorseJumping(); // Eric: ensures that the horse jump bar is displaying the correct amount as
									// onLivingUpdate in MC code also sets the horse jump amount during execution.
		}
	}

	@Override
	public void registerValues() {
		registerValue("Multiplier", 1.5F, 1.1F, 9.0F, "Specifies how much to multiply the player's jump height by.");
		registerValue("Horse Power", 1.0F, 0.0F, 1.0F,
				"Specifies the percentage of a horse's maximum jump height to insta-jump at.");
	}

	private void setupHorseJumping() {
		float horsePower = getFloat("Horse Power").floatValue();
		if (horsePower > 0.0F && Lanius.mc.player.isRidingHorse()) {
			ReflectHelper.setValue(EntityPlayerSP.class, Lanius.mc.player, 10,
					SrgMappings.EntityPlayerSP_horseJumpPowerCounter);
			ReflectHelper.setValue(EntityPlayerSP.class, Lanius.mc.player, horsePower,
					SrgMappings.EntityPlayerSP_horseJumpPower);
		}
	}
}
