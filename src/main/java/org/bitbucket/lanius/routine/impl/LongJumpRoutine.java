package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.impl.NetHandlerData;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Phase;
import org.bitbucket.lanius.util.RoutineUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.block.material.Material;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.server.SPacketEntityVelocity;
import net.minecraft.network.play.server.SPacketExplosion;
import net.minecraft.network.play.server.SPacketRespawn;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public final class LongJumpRoutine extends TabbedRoutine implements Hook<NetHandlerData> {

	private boolean jumped;

	private float receivedVel;

	public LongJumpRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.MOVEMENT);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Makes the player jump far.";
	}

	@Override
	public String displayData() {
		final int flooredVel = MathHelper.floor(receivedVel);
		return flooredVel == receivedVel ? String.valueOf(flooredVel) : String.format("%,.1f", receivedVel);
	}

	@Override
	public void init() {
		receivedVel = 0.0F;
		jumped = false;
	}

	boolean isJumped() {
		return jumped;
	}

	@Override
	public String name() {
		return "Long Jump";
	}

	@Override
	public void onExecute(final NetHandlerData data, final Phase phase) {
		if (data.retVal instanceof SPacketRespawn) {
			init();
		}
		if (!isEnabled() || !RoutineUtil.ncpEnabled() || !phase.equals(Phase.START)
				|| !(data.retVal instanceof SPacketEntityVelocity || data.retVal instanceof SPacketExplosion)) {
			return;
		}
		double velocityX = 0.0F, velocityZ = 0.0F;
		if (data.retVal instanceof SPacketEntityVelocity) {
			final SPacketEntityVelocity entityVelPacket = (SPacketEntityVelocity) data.retVal;
			final Entity entity = Lanius.mc.world.getEntityByID(entityVelPacket.getEntityID());
			if (entity == null || !entity.equals(Lanius.mc.player)) {
				return;
			}
			velocityX = entityVelPacket.getMotionX() / 8000.0F;
			velocityZ = entityVelPacket.getMotionZ() / 8000.0F;

		} else if (data.retVal instanceof SPacketExplosion) {
			final SPacketExplosion explosionPacket = (SPacketExplosion) data.retVal;
			velocityX = explosionPacket.getMotionX();
			velocityZ = explosionPacket.getMotionZ();
		}
		receivedVel += MathHelper.sqrt(velocityX * velocityX + velocityZ * velocityZ);
		data.retVal = null;
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onLivingUpdateHighest(final LivingEvent.LivingUpdateEvent livingUpdateEv) {
		final EntityLivingBase entityLiving = livingUpdateEv.getEntityLiving();
		if (!entityLiving.equals(Lanius.mc.player) || entityLiving instanceof EntityPlayerMP
				|| Lanius.mc.player.movementInput == null) {
			return;
		}
		if (entityLiving.onGround) {
			jumped = false;
		}
		final Material predictMat = entityLiving.world
				.getBlockState(new BlockPos(entityLiving.posX + entityLiving.motionX,
						entityLiving.posY - JesusRoutine.BLOCK_OFF + entityLiving.motionY,
						entityLiving.posZ + entityLiving.motionZ))
				.getMaterial(),
				posMat = entityLiving.world.getBlockState(
						new BlockPos(entityLiving.posX, entityLiving.posY - JesusRoutine.BLOCK_OFF, entityLiving.posZ))
						.getMaterial();
		final float multiplier = getFloat("Multiplier").floatValue();
		if (Lanius.mc.player.movementInput.moveForward > 0.0F && entityLiving.onGround && !entityLiving.isInWater()
				&& !entityLiving.isInLava()
				&& !(Boolean) ReflectHelper.getValue(Entity.class, entityLiving, SrgMappings.Entity_isInWeb)
				&& (!RoutineUtil.ncpEnabled() || !(RoutineUtil.enabled("Jesus") && (posMat == Material.WATER
						|| posMat == Material.LAVA || predictMat == Material.WATER || predictMat == Material.LAVA))
						&& receivedVel >= multiplier - 1.0F)) {
			Vec3d newMotion = org.bitbucket.lanius.util.math.MathHelper.calcMoveRelative(entityLiving.moveStrafing, 0.0F, 
					entityLiving.moveForward, 
					(float) ((SpeedRoutine) Lanius.getInstance().getRoutineRegistry().get("Speed")).baseSpeed(entityLiving), 
					new Vec3d(0.0D, entityLiving.motionY, 0.0D), entityLiving);
			Lanius.getInstance().getGameResources().getResource("local_sprint").setNewValue(true);
			newMotion = org.bitbucket.lanius.util.math.MathHelper.calcJump(newMotion, entityLiving);
			newMotion = new Vec3d(newMotion.x * multiplier, newMotion.y, newMotion.z * multiplier);
			Lanius.getInstance().getGameResources().getResource("local_motion_x").setNewValue(newMotion.x);
			Lanius.getInstance().getGameResources().getResource("local_motion_y").setNewValue(newMotion.y);
			Lanius.getInstance().getGameResources().getResource("local_motion_z").setNewValue(newMotion.z);
			receivedVel = 0.0F;
			jumped = true;
		}
	}

	@Override
	public void registerValues() {
		registerValue("Multiplier", 9.0F, 1.1F, 9.0F, "Specifies the multiplier for the player's velocity.");
	}

}
