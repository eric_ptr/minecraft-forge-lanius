package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.game.GameResource;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.impl.NetHandlerData;
import org.bitbucket.lanius.hook.impl.NetHandlerHook;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.Phase;
import org.bitbucket.lanius.util.RoutineUtil;
import org.bitbucket.lanius.util.math.MathHelper;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.MoverType;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.MobEffects;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.server.SPacketPlayerPosLook;
import net.minecraft.network.play.server.SPacketRespawn;
import net.minecraft.util.MovementInput;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public final class FlightRoutine extends TabbedRoutine implements Hook<NetHandlerData> {

	private static final double INIT_POS = -999.0D;

	private boolean executing, movePlayer, keysReset = true;

	// Eric: the flight bypass was patched in 1.9...
	private double oldX, oldY, oldZ;

	public FlightRoutine() {
		super(Keyboard.KEY_G, false, Tab.MOVEMENT);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Makes the player to walk through the air.";
	}

	private Vec3d fly(final EntityLivingBase entityLiving, final float speed) {
		Lanius.mc.player.capabilities.isFlying = false; // Eric: override
		// vanilla flight
		final MovementInput movementIn = Lanius.mc.player.movementInput;
		movementIn.updatePlayerMoveState();
		if (movementIn.sneak) {
			movementIn.moveStrafe /= 0.3D;
			movementIn.moveForward /= 0.3D;
		}
		double motionX = entityLiving.motionX, motionY = entityLiving.motionY, motionZ = entityLiving.motionZ;
		motionX = motionZ = 0.0D;
		motionY = 0.0D;
		Vec3d newMotion = MathHelper.calcMoveRelative(movementIn.moveStrafe * speed, speed, movementIn.moveForward * speed, speed, 
				new Vec3d(motionX, motionY, motionZ), entityLiving);
		double adjustedY = Math.abs(newMotion.y);
		if (getBoolean("3D")) {
			final Vec3d lookVec = entityLiving.getLookVec();
			return new Vec3d(newMotion.x, movementIn.moveForward > 0.0D ? lookVec.y * speed
					: movementIn.moveForward < 0.0D ? -lookVec.y * speed : 0.0D, newMotion.z);
		} else {
			return new Vec3d(newMotion.x, movementIn.jump ? adjustedY : movementIn.sneak ? -adjustedY : 0.0D, newMotion.z);
		}
	}

	@Override
	public void init() {
		oldX = oldY = oldZ = INIT_POS;
		executing = false;
		movePlayer = true;
		resetKeys();
	}

	public boolean isExecuting() {
		return executing;
	}

	@Override
	public String name() {
		return "Flight";
	}

	@Override
	public void onExecute(final NetHandlerData data, final Phase phase) {
		if (!phase.equals(Phase.START) || !isEnabled()) {
			return;
		}
		if (data.retVal instanceof CPacketPlayer) {
			final CPacketPlayer playerPacket = (CPacketPlayer) data.retVal;
			final double posX = playerPacket.getX(0.0D), posY = playerPacket.getY(0.0D), posZ = playerPacket.getZ(0.0D),
					EXPAND_VEC = 0.0625D;
			if (RoutineUtil.viaVersionEnabled() && getBoolean("Levitation")
					&& (!((Boolean) ReflectHelper.getValue(CPacketPlayer.class, playerPacket,
							SrgMappings.CPacketPlayer_moving))
							|| (oldX != INIT_POS || oldY != INIT_POS || oldZ != INIT_POS)
									&& MathHelper.distance(oldX, oldY, oldZ, posX, posY, posZ) < 9.0D
									&& !Lanius.mc.player.capabilities.allowFlying
									&& !Lanius.mc.player.isPotionActive(MobEffects.LEVITATION)
									&& !Lanius.mc.player.isElytraFlying()
									&& !Lanius.mc.world.checkBlockCollision(Lanius.mc.player.getEntityBoundingBox()
											.grow(EXPAND_VEC, EXPAND_VEC, EXPAND_VEC).expand(0.0D, -0.55D, 0.0D)))
					&& !RoutineUtil.noclipEnabled() && !RoutineUtil.ncpEnabled()) {
				data.retVal = null;
				((KillAuraRoutine) Lanius.getInstance().getRoutineRegistry().get("Kill Aura"))
						.setIgnorePlayerPacket(false); // Eric: Hotfix for ViaVersion vanilla Flight breaking Kill Aura.
			} else {
				oldX = posX;
				oldY = posY;
				oldZ = posZ;
			}
		} else if (data.retVal instanceof SPacketPlayerPosLook || data.retVal instanceof SPacketRespawn) {
			movePlayer = true;
		}
	}

	@SubscribeEvent(priority = EventPriority.LOW)
	public void onLivingUpdateLow(final LivingEvent.LivingUpdateEvent livingUpdateEv) {
		final EntityLivingBase entityLiving = livingUpdateEv.getEntityLiving();
		if (!entityLiving.equals(Lanius.mc.player) || entityLiving instanceof EntityPlayerMP
				|| Lanius.mc.player.movementInput == null || RoutineUtil.noclipEnabled()) {
			return;
		}
		if (entityLiving.isPlayerSleeping()) {
			((NoclipRoutine) Lanius.getInstance().getRoutineRegistry().get("Noclip")).wakeNoPacket();
			executing = true;
		}
		final float speed = getFloat("Speed").floatValue();
		if (RoutineUtil.ncpEnabled()) {
			if (Lanius.mc.player.capabilities.allowFlying) {
				Lanius.mc.player.capabilities.isFlying = true;
				Lanius.mc.player.sendPlayerAbilities();
				Lanius.mc.player.capabilities.isFlying = false;
			}
			if (speed > 0.6F || !Lanius.mc.player.capabilities.allowFlying) {
				if (executing) {
					final long worldTime = entityLiving.world.getWorldTime();
					if (worldTime < 13000L || worldTime >= 24000L) {
						executing = false;
						return;
					}
				} else {
					if (!RoutineUtil.viaVersionEnabled()
							&& !(entityLiving.isInWater() || entityLiving.isInLava() || (Boolean) ReflectHelper
									.getValue(Entity.class, entityLiving, SrgMappings.Entity_isInWeb))) {
						final float incSpeed = movePlayer ? Math.min(0.0625F, speed) : 0.0F;
						Vec3d newMotion = fly(entityLiving, incSpeed);
						if (movePlayer) {
							resetKeys();
							newMotion = fly(entityLiving, (float) Math.max(incSpeed - newMotion.y, 0.0F));
							final double prevMotX = entityLiving.motionX, prevMotY = entityLiving.motionY, prevMotZ = entityLiving.motionZ;
							entityLiving.move(MoverType.SELF, newMotion.x, newMotion.y,
									newMotion.z);
							entityLiving.motionX = prevMotX;
							entityLiving.motionY = prevMotY;
							entityLiving.motionZ = prevMotZ;
							NetHandlerHook.sendPlayerPacket(new CPacketPlayer.Position(entityLiving.posX,
									entityLiving.getEntityBoundingBox().minY, entityLiving.posZ,
									entityLiving.onGround));
							NetHandlerHook.sendPlayerPacket(new CPacketPlayer.Position(entityLiving.posX,
									entityLiving.getEntityBoundingBox().minY - 999.0D, entityLiving.posZ, false));
						} else {
							KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindForward.getKeyCode(), false);
							KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindBack.getKeyCode(), false);
							KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindLeft.getKeyCode(), false);
							KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindRight.getKeyCode(), false);
							keysReset = false;
						}
						Lanius.getInstance().getGameResources().getResource("local_sprint").setNewValue(false);
						movePlayer = false;
						Lanius.getInstance().getGameResources().getResource("local_motion_x").setNewValue(newMotion.x);
						Lanius.getInstance().getGameResources().getResource("local_motion_y").setNewValue(newMotion.y);
						Lanius.getInstance().getGameResources().getResource("local_motion_z").setNewValue(newMotion.z);
					}
					return;
				}
			}
		}
		Vec3d newMotion = fly(entityLiving, speed);
		GameResource<Boolean> sprintRes = Lanius.getInstance().getGameResources().getResource("local_sprint");
		if (sprintRes.hasNewValue() && (boolean) sprintRes.getNewValue()) {
			final double SPRINT_MULT = 1.30000001192092896D;
			newMotion = new Vec3d(newMotion.x * SPRINT_MULT, newMotion.y, newMotion.z * SPRINT_MULT);
		}
		Lanius.getInstance().getGameResources().getResource("local_motion_x").setNewValue(newMotion.x);
		Lanius.getInstance().getGameResources().getResource("local_motion_y").setNewValue(newMotion.y);
		Lanius.getInstance().getGameResources().getResource("local_motion_z").setNewValue(newMotion.z);
	}

	@Override
	public void registerValues() {
		registerValue("Levitation", false, "Determines whether or not to execute the pre-1.9 flight bypass.");
		registerValue("3D", false, "Determines whether or not to move the player based on their rotations.");
		registerValue("Speed", 0.6F, 0.1F, 6.0F, "Specifies the speed the player will fly at.");
	}

	private void resetKeys() {
		if (keysReset) {
			return;
		}
		KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindForward.getKeyCode(),
				GameSettings.isKeyDown(Lanius.mc.gameSettings.keyBindForward));
		KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindBack.getKeyCode(),
				GameSettings.isKeyDown(Lanius.mc.gameSettings.keyBindBack));
		KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindLeft.getKeyCode(),
				GameSettings.isKeyDown(Lanius.mc.gameSettings.keyBindLeft));
		KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindRight.getKeyCode(),
				GameSettings.isKeyDown(Lanius.mc.gameSettings.keyBindRight));
		keysReset = true;
	}

}