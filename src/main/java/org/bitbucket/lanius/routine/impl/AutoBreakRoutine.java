package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.RoutineUtil;
import org.bitbucket.lanius.util.game.InputHelper;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.client.settings.KeyBinding;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.GameType;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public final class AutoBreakRoutine extends TabbedRoutine {

	public AutoBreakRoutine() {
		super(Keyboard.KEY_V, false, Tab.WORLD);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Automatically breaks blocks when you hover over them.";
	}

	@Override
	public void init() {
		InputHelper.syncAttackBind();
	}

	@Override
	public String name() {
		return "Auto-break";
	}

	@SubscribeEvent
	public void onClientTick(final TickEvent.ClientTickEvent clientTickEv) {
		if (!clientTickEv.phase.equals(TickEvent.Phase.START) || Lanius.mc.player == null || Lanius.mc.isGamePaused()) {
			return;
		}
		if (Lanius.mc.currentScreen == null) {
			final BlockPos mouseOverPos;
			final GameType currentGameType;
			if (Lanius.mc.objectMouseOver != null && !Lanius.mc.player.isRowingBoat()
					&& Lanius.mc.objectMouseOver.typeOfHit.equals(RayTraceResult.Type.BLOCK)
					&& !Lanius.mc.world.isAirBlock(mouseOverPos = Lanius.mc.objectMouseOver.getBlockPos())
					&& !(currentGameType = Lanius.mc.playerController.getCurrentGameType()).equals(GameType.ADVENTURE)
					&& Lanius.mc.player.isAllowEdit() && Lanius.mc.world.getWorldBorder().contains(mouseOverPos)
					&& (currentGameType.isCreative() || Lanius.mc.world.getBlockState(mouseOverPos)
							.getBlockHardness(Lanius.mc.world, mouseOverPos) != -1.0F)
					&& !RoutineUtil.enabled("Nuker")) {
				KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindAttack.getKeyCode(), true);
			} else {
				InputHelper.syncAttackBind();
			}
		} else {
			InputHelper.syncAttackBind();
		}
	}
}
