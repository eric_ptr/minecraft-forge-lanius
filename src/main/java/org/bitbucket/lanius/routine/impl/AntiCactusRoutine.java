package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.game.CollisionUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.init.Blocks;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public final class AntiCactusRoutine extends TabbedRoutine {
	public AntiCactusRoutine() {
		super(Keyboard.KEY_NONE, true, Tab.WORLD);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NOCHEATPLUS, Compatibility.VIAVERSION, Compatibility.NO_VIAVERSION);
	}

	@Override
	public String description() {
		return "Prevents the player from being damaged by cacti.";
	}

	@Override
	public String name() {
		return "Anti-cactus";
	}

	@SubscribeEvent
	public void onLivingUpdate(final LivingEvent.LivingUpdateEvent livingUpdateEv) {
		final EntityLivingBase livingEntity = livingUpdateEv.getEntityLiving();
		if (!livingEntity.equals(Lanius.mc.player) || livingEntity instanceof EntityPlayerMP) {
			return;
		}
		// Eric: 10 blocks is about the fastest the player will ever move
		for (final IBlockState collidingState : CollisionUtil.collidingStates(livingEntity,
				new Class<?>[] { Blocks.CACTUS.getClass() }, 10.0D, true)) {
			final Block collidingBlock = collidingState.getBlock();
			if (collidingBlock instanceof Lanius.CactusSub || collidingBlock != Blocks.CACTUS) {
				continue;
			}
			ReflectHelper.setValue(BlockStateContainer.StateImplementation.class,
					(BlockStateContainer.StateImplementation) collidingState, Lanius.cactus,
					SrgMappings.BlockStateContainer_StateImplementation_block);
		}
	}

}
