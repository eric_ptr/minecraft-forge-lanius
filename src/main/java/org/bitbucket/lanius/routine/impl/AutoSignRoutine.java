package org.bitbucket.lanius.routine.impl;

import java.util.HashSet;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.gui.Tab;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.TabbedRoutine;
import org.bitbucket.lanius.util.RoutineUtil;
import org.lwjgl.input.Keyboard;

import com.google.common.collect.Sets;

import net.minecraft.client.gui.inventory.GuiEditSign;
import net.minecraft.network.play.client.CPacketUpdateSign;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public final class AutoSignRoutine extends TabbedRoutine {
	public AutoSignRoutine() {
		super(Keyboard.KEY_NONE, false, Tab.WORLD);
	}

	@Override
	public HashSet<Compatibility> compatibleWith() {
		return Sets.newHashSet(Compatibility.NO_VIAVERSION, Compatibility.VIAVERSION);
	}

	@Override
	public String description() {
		return "Automatically places signs with the specified text.";
	}

	@Override
	public String name() {
		return "Auto-sign";
	}

	@SubscribeEvent
	public void onGuiOpen(final GuiOpenEvent event) {
		if (event.isCanceled()) {
			return;
		}
		if (event.getGui() instanceof GuiEditSign && !RoutineUtil.enabled("Auto-break")
				&& (!RoutineUtil.enabled("Nuker")
						|| Lanius.getInstance().getRoutineRegistry().get("Nuker").getBoolean("Reverse"))) {
			event.setCanceled(true);
			Lanius.mc.player.connection.sendPacket(new CPacketUpdateSign(
					((TileEntitySign) ReflectHelper.getValue(GuiEditSign.class, (GuiEditSign) event.getGui(),
							SrgMappings.GuiEditSign_tileSign)).getPos(),
					new ITextComponent[] { new TextComponentString(getString("Line 1")),
							new TextComponentString(getString("Line 2")), new TextComponentString(getString("Line 3")),
							new TextComponentString(getString("Line 4")) }));
		}
	}

	@Override
	public void registerValues() {
		registerValue("Line 1", "Lanius", "Specifies a sign's first line of text.");
		registerValue("Line 2", "the", "Specifies a sign's second line of text.");
		registerValue("Line 3", "Minecraft Forge", "Specifies a sign's third line of text.");
		registerValue("Line 4", "cheat", "Specifies a sign's fourth line of text.");
	}
}
