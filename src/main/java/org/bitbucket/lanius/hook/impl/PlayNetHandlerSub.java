package org.bitbucket.lanius.hook.impl;

import org.bitbucket.lanius.hook.HookManager;
import org.bitbucket.lanius.util.Phase;

import com.mojang.authlib.GameProfile;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;
import net.minecraft.network.play.server.SPacketChangeGameState;
import net.minecraft.network.play.server.SPacketChat;
import net.minecraft.network.play.server.SPacketCombatEvent;
import net.minecraft.network.play.server.SPacketCustomSound;
import net.minecraft.network.play.server.SPacketEntityStatus;
import net.minecraft.network.play.server.SPacketEntityVelocity;
import net.minecraft.network.play.server.SPacketExplosion;
import net.minecraft.network.play.server.SPacketJoinGame;
import net.minecraft.network.play.server.SPacketPlayerPosLook;
import net.minecraft.network.play.server.SPacketRespawn;
import net.minecraft.network.play.server.SPacketSoundEffect;
import net.minecraft.network.play.server.SPacketSpawnGlobalEntity;
import net.minecraft.network.play.server.SPacketSpawnPosition;
import net.minecraft.network.play.server.SPacketTabComplete;
import net.minecraftforge.fml.common.FMLCommonHandler;

public final class PlayNetHandlerSub extends NetHandlerPlayClient {

	public PlayNetHandlerSub(Minecraft mcIn, GuiScreen p_i46300_2_, NetworkManager networkManagerIn,
			GameProfile profileIn) {
		super(mcIn, p_i46300_2_, networkManagerIn, profileIn);
	}

	@Override
	public void handleChangeGameState(SPacketChangeGameState packetIn) {
		try {
			HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.START);
			super.handleChangeGameState(packetIn);
			HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.END);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleChat(SPacketChat packetIn) {
		try {
			if ((packetIn = (SPacketChat) HookManager.packetManager.execute(new NetHandlerData(this, packetIn),
					Phase.START)) == null) {
				return;
			}
			super.handleChat(packetIn);
			HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.END);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleCombatEvent(SPacketCombatEvent packetIn) {
		try {
			HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.START);
			super.handleCombatEvent(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleCustomSound(SPacketCustomSound packetIn) {
		try {
			if (HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.START) == null) {
				return;
			}
			super.handleCustomSound(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleEntityStatus(SPacketEntityStatus packetIn) {
		try {
			HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.START);
			super.handleEntityStatus(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleEntityVelocity(SPacketEntityVelocity packetIn) {
		try {
			if ((packetIn = (SPacketEntityVelocity) HookManager.packetManager
					.execute(new NetHandlerData(this, packetIn), Phase.START)) == null) {
				return;
			}
			super.handleEntityVelocity(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleExplosion(SPacketExplosion packetIn) {
		try {
			if ((packetIn = (SPacketExplosion) HookManager.packetManager.execute(new NetHandlerData(this, packetIn),
					Phase.START)) == null) {
				return;
			}
			super.handleExplosion(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleJoinGame(SPacketJoinGame packetIn) {
		try {
			packetIn = (SPacketJoinGame) HookManager.packetManager.execute(new NetHandlerData(this, packetIn),
					Phase.START);
			super.handleJoinGame(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	private void handleNetError(Throwable error) {
		// Eric: it's somehow possible to have a null error object, so we must check for
		// it:
		if (error != null) {
			error.printStackTrace();
		}
		if (error != null && error.getStackTrace().length > 0
				&& error.getStackTrace()[0].getClassName().startsWith("org.bitbucket.lanius.")) {
			FMLCommonHandler.instance().exitJava(-1, false);
		}
	}

	@Override
	public void handlePlayerPosLook(SPacketPlayerPosLook packetIn) {
		try {
			if ((packetIn = (SPacketPlayerPosLook) HookManager.packetManager.execute(new NetHandlerData(this, packetIn),
					Phase.START)) == null) {
				return;
			}
			super.handlePlayerPosLook(packetIn);
			HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.END);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleRespawn(SPacketRespawn packetIn) {
		try {
			packetIn = (SPacketRespawn) HookManager.packetManager.execute(new NetHandlerData(this, packetIn),
					Phase.START);
			super.handleRespawn(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleSoundEffect(SPacketSoundEffect packetIn) {
		try {
			if (HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.START) == null) {
				return;
			}
			super.handleSoundEffect(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleSpawnGlobalEntity(SPacketSpawnGlobalEntity packetIn) {
		try {
			if (HookManager.packetManager.execute(new NetHandlerData(this, packetIn), Phase.START) == null) {
				return;
			}
			super.handleSpawnGlobalEntity(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleSpawnPosition(SPacketSpawnPosition packetIn) {
		try {
			packetIn = (SPacketSpawnPosition) HookManager.packetManager.execute(new NetHandlerData(this, packetIn),
					Phase.START);
			super.handleSpawnPosition(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void handleTabComplete(SPacketTabComplete packetIn) {
		try {
			if ((packetIn = (SPacketTabComplete) HookManager.packetManager.execute(new NetHandlerData(this, packetIn),
					Phase.START)) == null) {
				return;
			}
			super.handleTabComplete(packetIn);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

	@Override
	public void sendPacket(Packet<?> p_147297_1_) {
		try {
			if ((p_147297_1_ = HookManager.packetManager.execute(new NetHandlerData(this, p_147297_1_),
					Phase.START)) == null) {
				return;
			}
			super.sendPacket(p_147297_1_);
			HookManager.packetManager.execute(new NetHandlerData(this, p_147297_1_), Phase.END);
		} catch (NullPointerException npe) {
			handleNetError(npe);
		}
	}

}