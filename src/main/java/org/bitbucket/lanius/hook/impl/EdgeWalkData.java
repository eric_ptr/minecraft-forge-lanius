package org.bitbucket.lanius.hook.impl;

import org.bitbucket.lanius.hook.HookData;

import net.minecraft.entity.Entity;
import net.minecraft.entity.MoverType;

public class EdgeWalkData extends HookData<Entity, Boolean> {
	public final MoverType type;
	public final double x, y, z;

	public EdgeWalkData(Entity source, MoverType type, double x, double y, double z) {
		super(source);
		this.type = type;
		this.x = x;
		this.y = y;
		this.z = z;
		retVal = false;
	}
}
