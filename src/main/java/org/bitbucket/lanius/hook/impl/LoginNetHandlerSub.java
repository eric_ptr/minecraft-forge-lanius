package org.bitbucket.lanius.hook.impl;

import java.math.BigInteger;
import java.security.PublicKey;

import javax.crypto.SecretKey;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.hook.HookManager;

import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.exceptions.AuthenticationUnavailableException;
import com.mojang.authlib.exceptions.InvalidCredentialsException;

import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.network.NetHandlerLoginClient;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.login.client.CPacketEncryptionResponse;
import net.minecraft.network.login.server.SPacketEncryptionRequest;
import net.minecraft.util.CryptManager;
import net.minecraft.util.text.TextComponentTranslation;

public final class LoginNetHandlerSub extends NetHandlerLoginClient {

	private static final Logger LOGGER = LogManager.getLogger();

	private final NetworkManager networkManager;

	public LoginNetHandlerSub(NetworkManager networkManagerIn, Minecraft mcIn, GuiScreen previousScreenIn) {
		super(networkManagerIn, mcIn, previousScreenIn);
		networkManager = networkManagerIn;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void handleEncryptionRequest(SPacketEncryptionRequest packetIn) {
		HookManager.netHook.setOfflineMode();
		final SecretKey secretkey = CryptManager.createNewSharedKey();
		String s = packetIn.getServerId();
		PublicKey publickey = packetIn.getPublicKey();
		String s1 = (new BigInteger(CryptManager.getServerIdHash(s, publickey, secretkey))).toString(16);

		if (Lanius.mc.getCurrentServerData() != null && Lanius.mc.getCurrentServerData().isOnLAN()) {
			try {
				Lanius.mc.getSessionService().joinServer(Lanius.mc.getSession().getProfile(),
						Lanius.mc.getSession().getToken(), s1);
			} catch (AuthenticationException var10) {
				LOGGER.warn("Couldn\'t connect to auth servers but will continue to join LAN");
			}
		} else {
			try {
				Lanius.mc.getSessionService().joinServer(Lanius.mc.getSession().getProfile(),
						Lanius.mc.getSession().getToken(), s1);
			} catch (AuthenticationUnavailableException var7) {
				this.networkManager.closeChannel(new TextComponentTranslation("disconnect.loginFailedInfo",
						new Object[] { new TextComponentTranslation("disconnect.loginFailedInfo.serversUnavailable",
								new Object[0]) }));
				return;
			} catch (InvalidCredentialsException var8) {
				this.networkManager.closeChannel(new TextComponentTranslation("disconnect.loginFailedInfo",
						new Object[] { new TextComponentTranslation("disconnect.loginFailedInfo.invalidSession",
								new Object[0]) }));
				return;
			} catch (AuthenticationException authenticationexception) {
				this.networkManager.closeChannel(new TextComponentTranslation("disconnect.loginFailedInfo",
						new Object[] { authenticationexception.getMessage() }));
				return;
			}
		}

		this.networkManager.sendPacket(new CPacketEncryptionResponse(secretkey, publickey, packetIn.getVerifyToken()),
				new GenericFutureListener<Future<? super Void>>() {
					@Override
					public void operationComplete(Future<? super Void> p_operationComplete_1_) throws Exception {
						LoginNetHandlerSub.this.networkManager.enableEncryption(secretkey);
					}
				}, (GenericFutureListener<? extends Future<? super Void>>[]) new GenericFutureListener<?>[0]);
	}

}
