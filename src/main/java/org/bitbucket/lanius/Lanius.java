package org.bitbucket.lanius;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;

import org.bitbucket.lanius.cfg.ConfigContainer;
import org.bitbucket.lanius.cfg.ConfigHandler;
import org.bitbucket.lanius.cfg.Configurable;
import org.bitbucket.lanius.cfg.macro.MacroHandler;
import org.bitbucket.lanius.cfg.main.ModConfig;
import org.bitbucket.lanius.cmd.CommandHandler;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.impl.AllOffCommand;
import org.bitbucket.lanius.cmd.impl.AutoRespondCommand;
import org.bitbucket.lanius.cmd.impl.BreadcrumbsCommand;
import org.bitbucket.lanius.cmd.impl.CheckCommand;
import org.bitbucket.lanius.cmd.impl.ChestStealerCommand;
import org.bitbucket.lanius.cmd.impl.ConfigCommand;
import org.bitbucket.lanius.cmd.impl.CreativeDropCommand;
import org.bitbucket.lanius.cmd.impl.DamageCommand;
import org.bitbucket.lanius.cmd.impl.DescCommand;
import org.bitbucket.lanius.cmd.impl.GiveCommand;
import org.bitbucket.lanius.cmd.impl.GodmodeMobCommand;
import org.bitbucket.lanius.cmd.impl.HelpCommand;
import org.bitbucket.lanius.cmd.impl.MacroCommand;
import org.bitbucket.lanius.cmd.impl.NameProtectCommand;
import org.bitbucket.lanius.cmd.impl.NanCommand;
import org.bitbucket.lanius.cmd.impl.PingCommand;
import org.bitbucket.lanius.cmd.impl.PotionCommand;
import org.bitbucket.lanius.cmd.impl.SayCommand;
import org.bitbucket.lanius.cmd.impl.SearchCommand;
import org.bitbucket.lanius.cmd.impl.SpamCommand;
import org.bitbucket.lanius.cmd.impl.TimersCommand;
import org.bitbucket.lanius.cmd.impl.ToggleCommand;
import org.bitbucket.lanius.cmd.impl.ToggleHideCommand;
import org.bitbucket.lanius.cmd.impl.VClipCommand;
import org.bitbucket.lanius.cmd.impl.WaypointsCommand;
import org.bitbucket.lanius.game.GameResourceManager;
import org.bitbucket.lanius.game.ResourceListener;
import org.bitbucket.lanius.gui.ClickGui;
import org.bitbucket.lanius.gui.Component;
import org.bitbucket.lanius.gui.FrameComponent;
import org.bitbucket.lanius.gui.GuiHandler;
import org.bitbucket.lanius.gui.TextFieldComponent;
import org.bitbucket.lanius.hook.HookManager;
import org.bitbucket.lanius.hook.impl.NetHandlerHook;
import org.bitbucket.lanius.hook.impl.TabOverlaySub;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.routine.Renderable;
import org.bitbucket.lanius.routine.Routine;
import org.bitbucket.lanius.routine.RoutineDataParser;
import org.bitbucket.lanius.routine.RoutineHandler;
import org.bitbucket.lanius.routine.impl.AntiAfkRoutine;
import org.bitbucket.lanius.routine.impl.AntiCactusRoutine;
import org.bitbucket.lanius.routine.impl.AutoArmorRoutine;
import org.bitbucket.lanius.routine.impl.AutoBreakRoutine;
import org.bitbucket.lanius.routine.impl.AutoRespondRoutine;
import org.bitbucket.lanius.routine.impl.AutoSignRoutine;
import org.bitbucket.lanius.routine.impl.AutoUseRoutine;
import org.bitbucket.lanius.routine.impl.BackRoutine;
import org.bitbucket.lanius.routine.impl.BlinkRoutine;
import org.bitbucket.lanius.routine.impl.BoatRoutine;
import org.bitbucket.lanius.routine.impl.BreadcrumbsRoutine;
import org.bitbucket.lanius.routine.impl.BrightRoutine;
import org.bitbucket.lanius.routine.impl.BunnyHopRoutine;
import org.bitbucket.lanius.routine.impl.ChestStealerRoutine;
import org.bitbucket.lanius.routine.impl.CreativeArmorRoutine;
import org.bitbucket.lanius.routine.impl.CreativeDropRoutine;
import org.bitbucket.lanius.routine.impl.ElytraRoutine;
import org.bitbucket.lanius.routine.impl.EntityLauncherRoutine;
import org.bitbucket.lanius.routine.impl.FlightRoutine;
import org.bitbucket.lanius.routine.impl.FreecamRoutine;
import org.bitbucket.lanius.routine.impl.HighJumpRoutine;
import org.bitbucket.lanius.routine.impl.ItemSpoofRoutine;
import org.bitbucket.lanius.routine.impl.JesusRoutine;
import org.bitbucket.lanius.routine.impl.KillAuraRoutine;
import org.bitbucket.lanius.routine.impl.LagRoutine;
import org.bitbucket.lanius.routine.impl.LongJumpRoutine;
import org.bitbucket.lanius.routine.impl.NameProtectRoutine;
import org.bitbucket.lanius.routine.impl.NcpRoutine;
import org.bitbucket.lanius.routine.impl.NoFallRoutine;
import org.bitbucket.lanius.routine.impl.NoOverlaysRoutine;
import org.bitbucket.lanius.routine.impl.NoRenderRoutine;
import org.bitbucket.lanius.routine.impl.NoSwingRoutine;
import org.bitbucket.lanius.routine.impl.NoWeatherRoutine;
import org.bitbucket.lanius.routine.impl.NoclipRoutine;
import org.bitbucket.lanius.routine.impl.NukerRoutine;
import org.bitbucket.lanius.routine.impl.RegenRoutine;
import org.bitbucket.lanius.routine.impl.RetardRoutine;
import org.bitbucket.lanius.routine.impl.ReviveRoutine;
import org.bitbucket.lanius.routine.impl.SafeWalkRoutine;
import org.bitbucket.lanius.routine.impl.SearchRoutine;
import org.bitbucket.lanius.routine.impl.SneakRoutine;
import org.bitbucket.lanius.routine.impl.SpamRoutine;
import org.bitbucket.lanius.routine.impl.SpeedRoutine;
import org.bitbucket.lanius.routine.impl.SpeedyRoutine;
import org.bitbucket.lanius.routine.impl.SpiderRoutine;
import org.bitbucket.lanius.routine.impl.StepRoutine;
import org.bitbucket.lanius.routine.impl.TeamRoutine;
import org.bitbucket.lanius.routine.impl.TracersRoutine;
import org.bitbucket.lanius.routine.impl.VelocityRoutine;
import org.bitbucket.lanius.routine.impl.ViaVersionRoutine;
import org.bitbucket.lanius.routine.impl.WallhackRoutine;
import org.bitbucket.lanius.routine.impl.WaypointsRoutine;
import org.bitbucket.lanius.util.RoutineUtil;
import org.bitbucket.lanius.util.StringUtil;
import org.bitbucket.lanius.util.concurrent.Rate;
import org.bitbucket.lanius.util.registry.Registry;

import net.minecraft.block.Block;
import net.minecraft.block.BlockCactus;
import net.minecraft.block.SoundType;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod(modid = Lanius.MODID, name = Lanius.NAME, version = Lanius.VERSION)
public final class Lanius implements Configurable {
	public static final class CactusSub extends BlockCactus {
		@Override
		public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
			return HookManager.executeCollision(this, blockState, worldIn, pos);
		}

		@Override
		public void onEntityCollidedWithBlock(World worldIn, BlockPos pos, IBlockState state, Entity entityIn) {
			// Eric: prevent damage on the server side
			if (RoutineUtil.enabled("Anti-cactus") && entityIn instanceof EntityPlayerMP) {
				return;
			}
			super.onEntityCollidedWithBlock(worldIn, pos, state, entityIn);
		}
	}

	public static final CactusSub cactus = (CactusSub) new CactusSub().setHardness(0.4F).setUnlocalizedName("cactus");
	public static final Minecraft mc = Minecraft.getMinecraft();

	public static final File dataDir = new File(mc.mcDataDir, "lanius");

	private static final File enabledFile = new File(dataDir, "enabled_routines.cfg"),
			hiddenRoutinesFile = new File(dataDir, "hidden_routines.cfg");

	private static final RoutineDataParser routineEnabledParser = new RoutineDataParser() {

		@Override
		public void loadRoutine(Routine routine, String value) {
			final boolean enabled = Boolean.parseBoolean(value);
			if (routine.isEnabled() && !enabled || !routine.isEnabled() && enabled) {
				routine.setEnabled();
			}
		}

		@Override
		public void saveRoutine(PrintWriter out, Routine routine) {
			out.println(routine.name() + ":" + routine.isEnabled());
		}

	}, routineHiddenParser = new RoutineDataParser() {

		@Override
		public void loadRoutine(Routine routine, String value) {
			final boolean hidden = Boolean.parseBoolean(value);
			if (routine.isHidden() && !hidden || !routine.isHidden() && hidden) {
				routine.setHidden();
			}
		}

		@Override
		public void saveRoutine(PrintWriter out, Routine routine) {
			out.println(routine.name() + ":" + routine.isHidden());
		}

	};

	public static final String MODID = "lanius";

	static {
		try {
			ReflectHelper.findMethod(Block.class, SrgMappings.Block_setSoundType, SoundType.class).invoke(cactus,
					SoundType.CLOTH);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	@Mod.Instance(MODID)
	private static Lanius instance;

	public static final String NAME = "Lanius", VERSION = "4.0_b";

	public static Lanius getInstance() {
		return instance;
	}

	private GameResourceManager gameResources;

	private Registry<ConfigContainer> cfgContainerRegistry;

	private CommandHandler cmdHandler;

	private Registry<ModCommand> cmdRegistry;

	private GuiHandler guiHandler;

	private ModConfig modCfg;

	private Rate<CPacketPlayer> playerPacketRate;

	private Registry<Renderable> renderRegistry;

	private RoutineHandler routineHandler;

	private Registry<Routine> routineRegistry;

	private Registry<Configurable> standaloneCfgRegistry;

	private MacroHandler macroHandler;

	public Registry<ConfigContainer> getCfgContainerRegistry() {
		return cfgContainerRegistry;
	}

	public CommandHandler getCmdHandler() {
		return cmdHandler;
	}

	public Registry<ModCommand> getCmdRegistry() {
		return cmdRegistry;
	}

	public GameResourceManager getGameResources() {
		return gameResources;
	}

	public GuiHandler getGuiHandler() {
		return guiHandler;
	}

	public MacroHandler getMacroHandler() {
		return macroHandler;
	}

	public ModConfig getModCfg() {
		return modCfg;
	}

	public Rate<CPacketPlayer> getPlayerPacketRate() {
		final boolean ncpEnabled = RoutineUtil.ncpEnabled();
		playerPacketRate.setExecMax(ncpEnabled ? 2 : RoutineUtil.viaVersionEnabled() ? Rate.UNLIMITED_EXEC : 5);
		// Eric: +50 because this seems to be required on NCP. +100 for
		// variations in ping before they are taken into account by
		// NetworkUtils#lagTime.
		playerPacketRate.setResetDelay(ncpEnabled ? 1000L + 50L + 50L * 2L : 50L);
		return playerPacketRate;
	}

	public Registry<Renderable> getRenderRegistry() {
		return renderRegistry;
	}

	public RoutineHandler getRoutineHandler() {
		return routineHandler;
	}

	public Registry<Routine> getRoutineRegistry() {
		return routineRegistry;
	}

	@EventHandler
	public void init(final FMLInitializationEvent initEv) {
		// TODO(Eric) add a preset system for configs.
		playerPacketRate = new Rate<CPacketPlayer>(5, 50L) {

			@Override
			protected void onExecute(final CPacketPlayer object) {
				NetHandlerHook.sendPlayerPacket(object);
			}

			@SubscribeEvent
			public void onLivingUpdate(final LivingEvent.LivingUpdateEvent livingUpdateEv) {
				final EntityLivingBase livingEntity = livingUpdateEv.getEntityLiving();
				if (!livingEntity.equals(Lanius.mc.player) || livingEntity instanceof EntityPlayerMP) {
					return;
				}
				start();
			}

			@SubscribeEvent
			public void onUnload(final WorldEvent.Unload unloadEv) {
				stop();
			}

		};
		MinecraftForge.EVENT_BUS.register(playerPacketRate);
		registerCommands();
		registerRoutine(new SpeedRoutine());
		registerRoutine(new TracersRoutine());
		registerRoutine(new JesusRoutine());
		registerRoutine(new NoFallRoutine());
		registerRoutine(new FlightRoutine());
		registerRoutine(new StepRoutine());
		registerRoutine(new VelocityRoutine());
		registerRoutine(new KillAuraRoutine());
		registerRoutine(new ReviveRoutine());
		registerRoutine(new RegenRoutine());
		registerRoutine(new NameProtectRoutine());
		registerRoutine(new LagRoutine());
		registerRoutine(new SneakRoutine());
		registerRoutine(new FreecamRoutine());
		registerRoutine(new SearchRoutine());
		registerRoutine(new SpeedyRoutine());
		registerRoutine(new ElytraRoutine());
		registerRoutine(new BoatRoutine());
		registerRoutine(new AntiCactusRoutine());
		registerRoutine(new BlinkRoutine());
		registerRoutine(new CreativeDropRoutine());
		registerRoutine(new AutoUseRoutine());
		registerRoutine(new BreadcrumbsRoutine());
		registerRoutine(new NoSwingRoutine());
		registerRoutine(new ItemSpoofRoutine());
		registerRoutine(new LongJumpRoutine());
		registerRoutine(new NoclipRoutine());
		registerRoutine(new NoWeatherRoutine());
		registerRoutine(new RetardRoutine());
		registerRoutine(new AutoBreakRoutine());
		registerRoutine(new SpamRoutine());
		registerRoutine(new NoOverlaysRoutine());
		registerRoutine(new NukerRoutine());
		registerRoutine(new BrightRoutine());
		registerRoutine(new NoRenderRoutine());
		registerRoutine(new SpiderRoutine());
		registerRoutine(new TeamRoutine());
		registerRoutine(new HighJumpRoutine());
		registerRoutine(new WallhackRoutine());
		registerRoutine(new BackRoutine());
		registerRoutine(new WaypointsRoutine());
		registerRoutine(new EntityLauncherRoutine());
		registerRoutine(new AutoRespondRoutine());
		registerRoutine(new CreativeArmorRoutine());
		registerRoutine(new BunnyHopRoutine());
		registerRoutine(new SafeWalkRoutine());
		registerRoutine(new AutoSignRoutine());
		registerRoutine(new ChestStealerRoutine());
		registerRoutine(new AutoArmorRoutine());
		registerRoutine(new AntiAfkRoutine());
		registerCfgRoutines(initEv);
		MinecraftForge.EVENT_BUS.register(registerCfgContainer(cmdHandler = new CommandHandler()));
		MinecraftForge.EVENT_BUS.register(routineHandler = new RoutineHandler());
		MinecraftForge.EVENT_BUS.register(registerCfgContainer(guiHandler = new GuiHandler()));
		MinecraftForge.EVENT_BUS.register(registerCfgContainer(HookManager.netHook));
		MinecraftForge.EVENT_BUS.register(new TabOverlaySub());
		MinecraftForge.EVENT_BUS.register(new ConfigHandler());
		loadCfgContainers();
		loadStandaloneCfgs();
	}

	@Override
	public void load() {
		if (!enabledFile.exists()) {
			routineRegistry.get("Anti-cactus").setEnabled();
			routineRegistry.get("Name Protect").setEnabled();
			routineRegistry.get("No Fall").setEnabled();
			routineRegistry.get("Revive").setEnabled();
			routineRegistry.get("Speedy Gonzales").setEnabled();
			routineRegistry.get("Step").setEnabled();
			routineRegistry.get("Wallhack").setEnabled();
			routineRegistry.get("Waypoints").setEnabled();
			return;
		}
		loadRoutineData(enabledFile, routineEnabledParser);
		loadRoutineData(hiddenRoutinesFile, routineHiddenParser);
	}

	public void loadAllCfgs() {
		modCfg.load();
		loadCfgContainers();
		loadStandaloneCfgs();
	}

	public void loadCfgContainers() {
		for (final ConfigContainer cfgContainer : cfgContainerRegistry.objects()) {
			cfgContainer.registerValues();
		}
		for (final FrameComponent frame : ClickGui.instance.frames) {
			for (final Component child : frame.children) {
				if (child instanceof TextFieldComponent) {
					final TextFieldComponent txtField = (TextFieldComponent) child;
					txtField.setText(txtField.cfgContainer.getString(child.text()));
				}
			}
		}
	}

	private void loadRoutineData(File dataFile, RoutineDataParser parser) {
		if (!dataFile.exists()) {
			return;
		}
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(dataFile));
			String line;
			try {
				while ((line = in.readLine()) != null) {
					final int colonIdx = line.indexOf(":");
					parser.loadRoutine(routineRegistry.get(line.substring(0, colonIdx)), line.substring(colonIdx + 1));
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void loadStandaloneCfgs() {
		for (final Configurable config : standaloneCfgRegistry.objects()) {
			config.load();
		}
	}

	@EventHandler
	public void postInit(final FMLPostInitializationEvent postInitEv) {
		for (final Routine routine : routineRegistry.objects()) {
			routine.init();
		}
		load();
		MinecraftForge.EVENT_BUS.register(new ResourceListener(gameResources));
		System.out.println("Finished startup");
	}

	@EventHandler
	public void preInit(final FMLPreInitializationEvent preInitEv) {
		System.out.println("Starting up");
		dataDir.mkdir();
		modCfg = new ModConfig();
		modCfg.load();
		standaloneCfgRegistry = new Registry<>();
		macroHandler = new MacroHandler();
		MinecraftForge.EVENT_BUS.register(macroHandler);
		standaloneCfgRegistry.register("macro_handler", macroHandler);
		cfgContainerRegistry = new Registry<>();
		gameResources = new GameResourceManager();
		gameResources.init();
		renderRegistry = new Registry<>();
		routineRegistry = new Registry<Routine>() {
			@Override
			public final Routine get(final String name) {
				return objMap.get(StringUtil.configName(name));
			}

			@Override
			public final Routine register(final String name, final Routine object) {
				macroHandler.addMacro(object.getKeyBind());
				if (object instanceof Configurable) {
					standaloneCfgRegistry.register(name, (Configurable) object);
				}
				if (object instanceof Renderable) {
					renderRegistry.register(name, (Renderable) object);
				}
				return objMap.put(StringUtil.configName(name), object);
			}
		};
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public final void run() {
				saveCfgNoContainers();
			}

		}));
	}

	private ConfigContainer registerCfgContainer(final ConfigContainer cfgContainer) {
		cfgContainerRegistry.register(cfgContainer.category(), cfgContainer);
		return cfgContainer;
	}

	private void registerCfgRoutines(FMLInitializationEvent initEv) {
		registerRoutine(new NcpRoutine());
		registerRoutine(new ViaVersionRoutine());
	}

	private void registerCmd(final ModCommand cmd) {
		for (final String name : cmd.names()) {
			cmdRegistry.register(name, cmd);
		}
	}

	public void registerCommands() {
		cmdRegistry = new Registry<>();
		registerCmd(new VClipCommand());
		registerCmd(new ToggleCommand());
		registerCmd(new ConfigCommand());
		registerCmd(new DescCommand());
		registerCmd(new CheckCommand());
		registerCmd(new NameProtectCommand());
		registerCmd(new SearchCommand());
		registerCmd(new CreativeDropCommand());
		registerCmd(new BreadcrumbsCommand());
		registerCmd(new NanCommand());
		registerCmd(new SpamCommand());
		registerCmd(new GiveCommand());
		registerCmd(new HelpCommand());
		registerCmd(new DamageCommand());
		registerCmd(new WaypointsCommand());
		registerCmd(new TimersCommand());
		registerCmd(new GodmodeMobCommand());
		registerCmd(new ToggleHideCommand());
		registerCmd(new PingCommand());
		registerCmd(new PotionCommand());
		registerCmd(new AutoRespondCommand());
		registerCmd(new AllOffCommand());
		registerCmd(new MacroCommand());
		registerCmd(new ChestStealerCommand());
		registerCmd(new SayCommand());
	}

	private Routine registerRoutine(final Routine routine) {
		registerCfgContainer(routine);
		return routineRegistry.register(routine.name(), routine);
	}

	@Override
	public void save() {
		saveRoutineData(enabledFile, routineEnabledParser);
		saveRoutineData(hiddenRoutinesFile, routineHiddenParser);
	}

	public void saveAllCfgs() {
		for (final ConfigContainer container : cfgContainerRegistry.objects()) {
			container.saveDefaultValues();
		}
		saveCfgNoContainers();
	}

	private void saveCfgNoContainers() {
		System.out.println("Saving configuration changes");
		modCfg.save();
		ClickGui.instance.save();
		for (final Configurable config : standaloneCfgRegistry.objects()) {
			config.save();
		}
		save();
	}

	private void saveRoutineData(File dataFile, RoutineDataParser parser) {
		dataFile.delete();
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter((new FileWriter(dataFile))));
			for (final Routine routine : routineRegistry.objects()) {
				parser.saveRoutine(out, routine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}
}
