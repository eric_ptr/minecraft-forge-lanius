package org.bitbucket.lanius.game;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class ResettableGameResource<T> extends BasicGameResource<T> {
	private Consumer<GameResource<T>> resetFn;
	
	public ResettableGameResource(String id, Predicate<T> executeFn, Consumer<GameResource<T>> resetFn) {
		super(id, executeFn);
		this.resetFn = resetFn;
	}
	
	@Override
	public boolean execute() {
		boolean success = super.execute();
		if (!success) {
			resetFn.accept(this);
		}
		return success;
	}
}
