package org.bitbucket.lanius.game;

import java.util.function.Predicate;

public abstract class AbstractGameResource<T> implements GameResource<T> {
	protected final Predicate<T> executeFn;
	private final String id;

	public AbstractGameResource(String id, Predicate<T> executeFn) {
		this.id = id;
		this.executeFn = executeFn;
	}

	@Override
	public String id() {
		return id;
	}
}
