package org.bitbucket.lanius.game;

import java.util.function.Predicate;

public class BasicGameResource<T> extends AbstractGameResource<T> {
	private T newValue;
	private boolean hasSetVal;

	public BasicGameResource(String id, Predicate<T> executeFn) {
		super(id, executeFn);
	}

	@Override
	public boolean execute() {
		if (!hasSetVal) {
			return false;
		}
		hasSetVal = false;
		return executeFn.test(getNewValue());
	}

	@Override
	public T getNewValue() {
		return newValue;
	}

	@Override
	public boolean setNewValue(T newValue) {
		if (hasSetVal) {
			return false;
		}
		this.newValue = newValue;
		hasSetVal = true;
		return true;
	}

	@Override
	public boolean hasNewValue() {
		return hasSetVal;
	}
}
