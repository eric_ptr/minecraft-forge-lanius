package org.bitbucket.lanius.game;

public interface GameResource<T> {
	boolean execute();

	T getNewValue();

	String id();

	boolean setNewValue(T newValue);
	boolean hasNewValue();
}
