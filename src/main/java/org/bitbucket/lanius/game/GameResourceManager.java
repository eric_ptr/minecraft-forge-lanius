package org.bitbucket.lanius.game;

import java.util.List;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.hook.HookManager;
import org.bitbucket.lanius.routine.impl.SpeedRoutine;
import org.bitbucket.lanius.util.registry.Registry;

import net.minecraft.network.play.client.CPacketPlayer;

public final class GameResourceManager {
	private final Registry<GameResource<?>> resources = new Registry<>();

	@SuppressWarnings("unchecked")
	public <T> GameResource<T> getResource(String id) {
		return (GameResource<T>) resources.get(id);
	}

	private void registerResources() {
		// Working:
		registerResource(new BasicGameResource<Boolean>("local_sprint", (newValue) -> {
			Lanius.mc.player.setSprinting(newValue && (Lanius.mc.player.getFoodStats().getFoodLevel() > 6.0F
					|| Lanius.mc.player.capabilities.allowFlying));
			if (!Lanius.mc.player.isSprinting()) {
				((SpeedRoutine) Lanius.getInstance().getRoutineRegistry().get("Speed")).setSprinting();
			}
			return true;
		}));

		registerResource(new ResettableGameResource<Float>("server_yaw", (newValue) -> {
			HookManager.netHook.setServerYaw(newValue);
			return true;
		}, (resource) -> {
			HookManager.netHook.setServerYaw(Lanius.mc.player.rotationYaw);
		}));
		registerResource(new ResettableGameResource<Float>("server_pitch", (newValue) -> {
			HookManager.netHook.setServerPitch(newValue);
			return true;
		}, (resource) -> {
			HookManager.netHook.setServerYaw(Lanius.mc.player.rotationPitch);
		}));

		registerResource(new MultiGameResource<CPacketPlayer>("extra_movement_packets", (newValue) -> {
			return Lanius.getInstance().getPlayerPacketRate().execute(newValue, 0);
		}));

		registerResource(new BasicGameResource<Double>("local_motion_x", (newValue) -> {
			Lanius.mc.player.motionX = newValue;
			return true;
		}));
		registerResource(new BasicGameResource<Double>("local_motion_y", (newValue) -> {
			Lanius.mc.player.motionY = newValue;
			return true;
		}));
		registerResource(new BasicGameResource<Double>("local_motion_z", (newValue) -> {
			Lanius.mc.player.motionZ = newValue;
			return true;
		}));
		// Working:
		registerResource(new ResettableGameResource<Float>("local_step_height", (newValue) -> {
			Lanius.mc.player.stepHeight = newValue;
			return true;
		}, (resource) -> {
			Lanius.mc.player.stepHeight = 0.5F;
		}));
		registerResource(new BasicGameResource<Float>("local_forward", (newValue) -> {
			Lanius.mc.player.moveForward = newValue;
			return true;
		}));
		registerResource(new BasicGameResource<Float>("local_strafing", (newValue) -> {
			Lanius.mc.player.moveStrafing = newValue;
			return true;
		}));
		// TODO(Eric) add more resources (see the list you created). Also implement
		// them.
	}
	public void init() {
		registerResources();
	}

	private void registerResource(GameResource<?> resource) {
		resources.register(resource.id(), resource);
	}

	public List<GameResource<?>> resources() {
		return resources.objects();
	}
}
