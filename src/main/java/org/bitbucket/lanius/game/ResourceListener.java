package org.bitbucket.lanius.game;

import org.bitbucket.lanius.Lanius;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public final class ResourceListener {
	private GameResourceManager resManager;
	
	public ResourceListener(GameResourceManager resManager) {
		this.resManager = resManager;
	}
	
	@SubscribeEvent(priority = EventPriority.LOWEST)
	public void onLivingUpdateLowest(LivingEvent.LivingUpdateEvent livingUpdateEv) {
		if (Lanius.mc.player == null || Lanius.mc.player.movementInput == null) {
			return;
		}
		EntityLivingBase entity = livingUpdateEv.getEntityLiving();
		if (!entity.equals(Lanius.mc.player) || entity instanceof EntityPlayerMP) {
			return;
		}
		// Implemented:
		resManager.getResource("local_sprint").execute();
		resManager.getResource("local_step_height").execute();
		
		resManager.getResource("local_motion_x").execute();
		resManager.getResource("local_motion_y").execute();
		resManager.getResource("local_motion_z").execute();
		
		resManager.getResource("local_forward").execute();
		resManager.getResource("local_strafing").execute();
		
		// TODO Decide whether or not to keep these:
		//resManager.getResource("server_yaw").execute();
		//resManager.getResource("server_pitch").execute();
	}
	@SubscribeEvent
	public void onClientTick(final TickEvent.ClientTickEvent clientTickEv) {
		if (!clientTickEv.phase.equals(TickEvent.Phase.END) || Lanius.mc.player == null || Lanius.mc.isGamePaused()) {
			return;
		}
		resManager.getResource("extra_movement_packets").execute();
	}
	public void init() {
		MinecraftForge.EVENT_BUS.register(this);
	}
	public void uninit() {
		MinecraftForge.EVENT_BUS.unregister(this);
	}
}
