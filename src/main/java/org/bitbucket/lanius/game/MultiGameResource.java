package org.bitbucket.lanius.game;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class MultiGameResource<T> extends AbstractGameResource<T> {
	private final List<T> newValues = new ArrayList<>();
	private int successfulExecCount;

	public MultiGameResource(String id, Predicate<T> executeFn) {
		super(id, executeFn);
	}

	public void addNewValue(T newValue) {
		newValues.add(newValue);
	}

	public void clearNewValues() {
		newValues.clear();
	}

	@Override
	public boolean execute() {
		successfulExecCount = 0;
		for (T newValue : newValues) {
			if (!executeFn.test(newValue)) {
				return false;
			}
			++successfulExecCount;
		}
		return true;
	}

	@Override
	public T getNewValue() {
		return newValues.isEmpty() ? null : newValues.get(newValues.size() - 1);
	}

	@SuppressWarnings("unchecked")
	public T[] getNewValues() {
		return (T[]) newValues.toArray();
	}

	public int getSuccessfulExecCount() {
		return successfulExecCount;
	}

	public void removeNewValue(T newValue) {
		newValues.remove(newValue);
	}

	@Override
	public boolean setNewValue(T newValue) {
		addNewValue(newValue);
		return true;
	}

	@Override
	public boolean hasNewValue() {
		return !newValues.isEmpty();
	}
}
