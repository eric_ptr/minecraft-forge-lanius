package org.bitbucket.lanius.gui;

import org.bitbucket.lanius.Lanius;

public class HubLoadButton extends BasicButton {
	public HubLoadButton(int x, int y, int width, int height) {
		super("Load", x, y, width, height);
	}

	@Override
	public void click(final int mouseX, final int mouseY, final int mouseBtn) {
		super.click(mouseX, mouseY, mouseBtn);
		if (mouseBtn == 0) {
			Lanius.getInstance().loadAllCfgs();
		}
	}

	@Override
	protected String tooltip() {
		return "Reloads the configuration of " + Lanius.NAME + " from the configuration files.";
	}
}
