package org.bitbucket.lanius.gui;

import org.bitbucket.lanius.Lanius;

import net.minecraft.client.gui.Gui;

public abstract class BasicButton extends AbstractButton {

	public BasicButton(String text, int x, int y, int width, int height) {
		super(text, x, y, width, height, null);
	}

	@Override
	public void render(final int mouseX, final int mouseY, final float partialTicks) {
		Gui.drawRect(getX(), getY(), getX() + getWidth((byte) 0), getY() + getHeight(),
				ClickGui.instance.frontmostFrame(mouseX, mouseY) == null && SliderComponent.changingSlider == null
						&& mouseX >= getX() && mouseX <= getX() + getWidth((byte) 0) && mouseY >= getY()
						&& mouseY <= getY() + getHeight() ? 0x80000000 : 0x80404040);
		Lanius.mc.fontRenderer.drawStringWithShadow(text(),
				getX() + getWidth((byte) 0) / 2 - Lanius.mc.fontRenderer.getStringWidth(text()) / 2, getY() + 2,
				16777215);
	}
}
