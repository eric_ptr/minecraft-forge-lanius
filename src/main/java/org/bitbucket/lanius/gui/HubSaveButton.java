package org.bitbucket.lanius.gui;

import org.bitbucket.lanius.Lanius;

public class HubSaveButton extends BasicButton {
	public HubSaveButton(int x, int y, int width, int height) {
		super("Save", x, y, width, height);
	}

	@Override
	public void click(final int mouseX, final int mouseY, final int mouseBtn) {
		super.click(mouseX, mouseY, mouseBtn);
		if (mouseBtn == 0) {
			Lanius.getInstance().saveAllCfgs();
		}
	}

	@Override
	protected String tooltip() {
		return "Saves the current configuration of " + Lanius.NAME + " to the configuration files.";
	}
}
