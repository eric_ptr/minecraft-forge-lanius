package org.bitbucket.lanius.gui;

public class HubResetButton extends BasicButton {

	public HubResetButton(int x, int y, int width, int height) {
		super("Reset", x, y, width, height);
	}

	@Override
	public void click(final int mouseX, final int mouseY, final int mouseBtn) {
		super.click(mouseX, mouseY, mouseBtn);
		if (mouseBtn == 0) {
			ClickGui.instance.resetFramesState();
			ClickGui.instance.resetMovedFrame();
			ClickGui.instance.initPositions();
		}
	}

	@Override
	protected String tooltip() {
		return "Resets the Click UI's frame states.";
	}
}
