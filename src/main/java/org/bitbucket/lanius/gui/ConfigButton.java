package org.bitbucket.lanius.gui;

import org.bitbucket.lanius.cfg.ConfigContainer;

public final class ConfigButton extends ButtonComponent {
	private final ConfigContainer cfgContainer;

	public ConfigButton(final String text, int x, int y, int width, int height, FrameComponent parent,
			final ConfigContainer cfgContainer) {
		super(text, x, y, width, height, parent, cfgContainer.getBoolean(text));
		this.cfgContainer = cfgContainer;
	}

	@Override
	public void click(final int mouseX, final int mouseY, final int mouseBtn) {
		super.click(mouseX, mouseY, mouseBtn);
		cfgContainer.putValue(text(), !isSelected());
	}

	@Override
	protected boolean isSelected() {
		return cfgContainer.getBoolean(text());
	}

	@Override
	protected String tooltip() {
		return cfgContainer.getComment(text());
	}
}
