package org.bitbucket.lanius.util.game;

import static org.lwjgl.opengl.GL11.GL_ALPHA_TEST;
import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glIsEnabled;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cfg.ConfigContainer;
import org.bitbucket.lanius.routine.impl.FreecamRoutine;

import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.util.math.AxisAlignedBB;

public final class RenderHelper {
	public static void drawOutline(final AxisAlignedBB outlineBox, final float partialTicks, final float red,
			final float green, final float blue, final float alpha, final ConfigContainer cfgContainer,
			boolean drawLines) {
		final AxisAlignedBB renderBox = outlineBox
				.grow(0.0020000000949949026D, 0.0020000000949949026D, 0.0020000000949949026D)
				.offset(-org.bitbucket.lanius.util.math.MathHelper.interpolate(Lanius.mc.player.posX,
						Lanius.mc.player.lastTickPosX, partialTicks),
						-org.bitbucket.lanius.util.math.MathHelper.interpolate(Lanius.mc.player.posY,
								Lanius.mc.player.lastTickPosY, partialTicks),
						-org.bitbucket.lanius.util.math.MathHelper.interpolate(Lanius.mc.player.posZ,
								Lanius.mc.player.lastTickPosZ, partialTicks));
		final Tessellator tessellator = Tessellator.getInstance();
		final BufferBuilder buffer = tessellator.getBuffer();
		final boolean culling = glIsEnabled(GL_CULL_FACE);
		if (culling) {
			glDisable(GL_CULL_FACE);
		}
		final boolean blend = glIsEnabled(GL_BLEND);
		if (!blend) {
			GlStateManager.enableBlend();
		}
		final boolean alphaTest = glIsEnabled(GL_ALPHA_TEST);
		if (alphaTest) {
			GlStateManager.disableAlpha();
		}
		buffer.begin(GL_QUADS, DefaultVertexFormats.POSITION_COLOR);
		final float quadAlpha;
		final float MAX_QUAD_ALPHA = 0.375F;
		if (cfgContainer.getBoolean("Fade") && drawLines) {
			final FreecamRoutine freecam = (FreecamRoutine) Lanius.getInstance().getRoutineRegistry().get("Freecam");
			final double BOX_VEC = 0.5D;
			float distPercent = (float) (org.bitbucket.lanius.util.math.MathHelper.distance(
					Lanius.mc.player.posX + freecam.getPosX(), Lanius.mc.player.posY + freecam.getPosY(),
					Lanius.mc.player.posZ + freecam.getPosZ(), outlineBox.minX + BOX_VEC, outlineBox.minY + BOX_VEC,
					outlineBox.minZ + BOX_VEC) / ((Lanius.mc.gameSettings.renderDistanceChunks * 2 + 1) * 16 / 2));
			if (distPercent > MAX_QUAD_ALPHA) {
				distPercent = MAX_QUAD_ALPHA;
			}
			quadAlpha = distPercent;
		} else {
			quadAlpha = MAX_QUAD_ALPHA;
		}
		buffer.pos(renderBox.minX, renderBox.minY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.maxY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.minY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.minY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.minY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.minY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.maxY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.minY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.minY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.minY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.minY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.minY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.minY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.minY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.maxY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.minX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, quadAlpha).endVertex();
		buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.minZ).color(red, green, blue, quadAlpha).endVertex();
		tessellator.draw();
		if (alphaTest) {
			GlStateManager.enableAlpha();
		}
		if (!blend) {
			GlStateManager.disableBlend();
		}
		if (culling) {
			glEnable(GL_CULL_FACE);
		}
		if (drawLines) {
			RenderGlobal.drawSelectionBoundingBox(renderBox, red, green, blue, alpha);
			buffer.begin(GL_LINES, DefaultVertexFormats.POSITION_COLOR);
			buffer.pos(renderBox.minX, renderBox.minY, renderBox.minZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.minZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.minX, renderBox.minY, renderBox.maxZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.minX, renderBox.minY, renderBox.minZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.minX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.maxX, renderBox.minY, renderBox.minZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.minX, renderBox.minY, renderBox.minZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.maxX, renderBox.minY, renderBox.maxZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.minX, renderBox.maxY, renderBox.minZ).color(red, green, blue, alpha).endVertex();
			buffer.pos(renderBox.maxX, renderBox.maxY, renderBox.maxZ).color(red, green, blue, alpha).endVertex();
			tessellator.draw();
		}
	}
}
