package org.bitbucket.lanius.util.game;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.reflect.ReflectHelper;
import org.bitbucket.lanius.reflect.SrgMappings;
import org.bitbucket.lanius.util.RoutineUtil;

import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.CPacketInput;
import net.minecraft.network.play.client.CPacketPlayer;
import net.minecraft.network.play.client.CPacketPlayerDigging;
import net.minecraft.util.math.MathHelper;

public final class NetworkUtil {
	public static boolean digBlockPacket(final Packet<?> packet) {
		if (!(packet instanceof CPacketPlayerDigging)) {
			return false;
		}
		final CPacketPlayerDigging.Action action = ((CPacketPlayerDigging) packet).getAction();
		return action.equals(CPacketPlayerDigging.Action.START_DESTROY_BLOCK)
				|| action.equals(CPacketPlayerDigging.Action.ABORT_DESTROY_BLOCK)
				|| action.equals(CPacketPlayerDigging.Action.STOP_DESTROY_BLOCK);
	}

	public static void dismountEntity() {
		Lanius.mc.player.connection.sendPacket(new CPacketInput(0.0F, 0.0F, false, true));
		Lanius.mc.player.dismountRidingEntity();
	}

	public static int lagTicks() {
		return MathHelper.ceil(lagTime() / 50.0F);
	}

	public static int lagTime() {
		return !RoutineUtil.lagEnabled() || Lanius.mc.isSingleplayer() ? 0
				: lagTime(Lanius.mc.player.connection.getPlayerInfo(Lanius.mc.player.getName()));
	}

	public static int lagTime(NetworkPlayerInfo netPlayerInfo) {
		return netPlayerInfo == null ? 0 : Math.max(netPlayerInfo.getResponseTime(), 0);
	}

	public static boolean motionPacket(final Packet<?> packet) {
		if (!(packet instanceof CPacketPlayer)) {
			return false;
		}
		final CPacketPlayer playerPacket = (CPacketPlayer) packet;
		return (Boolean) ReflectHelper.getValue(CPacketPlayer.class, playerPacket, SrgMappings.CPacketPlayer_moving)
				|| (Boolean) ReflectHelper.getValue(CPacketPlayer.class, playerPacket,
						SrgMappings.CPacketPlayer_rotating);
	}

	public static void sendChatMessageNoBlock(String message) {
		Lanius.getInstance().getCmdHandler().setAllowChat(true);
		Lanius.mc.player.sendChatMessage(message);
		Lanius.getInstance().getCmdHandler().setAllowChat(false);
	}
}
