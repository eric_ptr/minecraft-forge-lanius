package org.bitbucket.lanius.util.game;

import org.bitbucket.lanius.Lanius;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.client.ClientCommandHandler;

public final class InputHelper {
	public static final String NO_KEY_NAME = "NONE";

	public static String getKeyName(int keyCode) {
		if (keyCode != Keyboard.KEY_NONE && keyCode < Keyboard.KEYBOARD_SIZE) {
			return keyCode < Keyboard.KEY_NONE ? Mouse.getButtonName(keyCode + 100) : Keyboard.getKeyName(keyCode);
		}
		return NO_KEY_NAME;
	}

	public static boolean isKeyDown(int keyCode) {
		if (keyCode != Keyboard.KEY_NONE && keyCode < Keyboard.KEYBOARD_SIZE) {
			return keyCode < Keyboard.KEY_NONE ? Mouse.isButtonDown(keyCode + 100) : Keyboard.isKeyDown(keyCode);
		}
		return false;
	}

	public static void simulateChatInput(String message, boolean addToMessages) {
		if (addToMessages) {
			Lanius.mc.ingameGUI.getChatGUI().addToSentMessages(message);
		}
		if (ClientCommandHandler.instance.executeCommand(Lanius.mc.player, message) == 0) {
			Lanius.mc.player.sendChatMessage(message);
		}
	}

	public static void syncAttackBind() {
		if (!GameSettings.isKeyDown(Lanius.mc.gameSettings.keyBindAttack)
				&& Lanius.mc.gameSettings.keyBindAttack.isKeyDown()) {
			KeyBinding.setKeyBindState(Lanius.mc.gameSettings.keyBindAttack.getKeyCode(), false);
		}
	}
}
