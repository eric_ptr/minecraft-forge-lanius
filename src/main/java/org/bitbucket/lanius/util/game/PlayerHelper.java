package org.bitbucket.lanius.util.game;

import org.bitbucket.lanius.Lanius;

import net.minecraft.util.MovementInput;
import net.minecraft.util.math.Vec2f;

public final class PlayerHelper {
	public static Vec2f calcMoveInputs(MovementInput movementIn) {
		float moveStrafe = movementIn.moveStrafe, moveForward = movementIn.moveForward;
	    moveStrafe = 0.0F;
	    moveForward = 0.0F;
	    if (Lanius.mc.gameSettings.keyBindForward.isKeyDown()) {
	      moveForward++;
	      movementIn.forwardKeyDown = true;
	    } else {
	      movementIn.forwardKeyDown = false;
	    } 
	    if (Lanius.mc.gameSettings.keyBindBack.isKeyDown()) {
	      moveForward--;
	      movementIn.backKeyDown = true;
	    } else {
	      movementIn.backKeyDown = false;
	    } 
	    if (Lanius.mc.gameSettings.keyBindLeft.isKeyDown()) {
	      moveStrafe++;
	      movementIn.leftKeyDown = true;
	    } else {
	      movementIn.leftKeyDown = false;
	    } 
	    if (Lanius.mc.gameSettings.keyBindRight.isKeyDown()) {
	      moveStrafe--;
	      movementIn.rightKeyDown = true;
	    } else {
	      movementIn.rightKeyDown = false;
	    } 
	    movementIn.jump = Lanius.mc.gameSettings.keyBindJump.isKeyDown();
	    movementIn.sneak = Lanius.mc.gameSettings.keyBindSneak.isKeyDown();
	    if (movementIn.sneak) {
	      moveStrafe = (float)(moveStrafe * 0.3D);
	      moveForward = (float)(moveForward * 0.3D);
	    }
		return new Vec2f(moveStrafe, moveForward);
	}
}
