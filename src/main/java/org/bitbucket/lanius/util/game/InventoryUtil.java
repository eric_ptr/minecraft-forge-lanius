package org.bitbucket.lanius.util.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.util.RoutineUtil;

import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.ai.attributes.IAttributeInstance;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.network.play.client.CPacketClickWindow;
import net.minecraft.network.play.client.CPacketHeldItemChange;

public final class InventoryUtil {
	public static final int HOTBAR_BEGIN = 36, INIT_SLOT = -1, MIN_SLOT = 9, MAX_SLOT = 45;
	public static final int ARMOR_BEGIN = 5, ARMOR_END = 8, SHIELD_SLOT = 45; // ARMOR_END is the player's boots,
																				// ARMOR_BEGIN is the player's helmet
	private static final Map<ArmorMaterial, Integer> armorPriorityMap = new HashMap<>();

	static {
		armorPriorityMap.put(ArmorMaterial.DIAMOND, 0);
		armorPriorityMap.put(ArmorMaterial.IRON, 1);
		armorPriorityMap.put(ArmorMaterial.CHAIN, 2);
		armorPriorityMap.put(ArmorMaterial.GOLD, 3);
		armorPriorityMap.put(ArmorMaterial.LEATHER, 4);
	}

	public static void addAllNanAttributes(ItemStack stack) {
		if (!isStackValid(stack)) {
			return;
		}
		for (IAttributeInstance attribute : Lanius.mc.player.getAttributeMap().getAllAttributes()) {
			for (final EntityEquipmentSlot equipSlot : EntityEquipmentSlot.values()) {
				for (int operation = 0; operation <= 2; operation++) {
					addNanAttribute(stack, equipSlot, attribute, operation);
				}
			}
		}
	}

	public static void addNanAttribute(final ItemStack stack, EntityEquipmentSlot equipSlot,
			IAttributeInstance attribute, int operation) {
		if (!isStackValid(stack) || equipSlot == null || attribute == null || operation < 0 || operation > 2) {
			return;
		}
		stack.addAttributeModifier(attribute.getAttribute().getName(),
				new AttributeModifier(UUID.randomUUID(), attribute.getAttribute().getName(), Double.NaN, operation),
				equipSlot);
	}

	public static Slot calcLastSlotAgainstInventory(List<Slot> slots, IInventory againstInv, boolean excludeEmpty) {
		Slot lastSlot = null;
		for (Slot s : slots) {
			if (s.isHere(againstInv, s.getSlotIndex()) || excludeEmpty && !s.getHasStack()) {
				continue;
			}
			lastSlot = s;
		}
		return lastSlot;
	}

	public static void clickWindow(final int slot, final int mouseBtn, final ClickType type) {
		ensureInventory();
		clickWindowOpenContainer(slot, mouseBtn, type);
	}

	public static void clickWindowOpenContainer(final int slot, final int mouseBtn, final ClickType type) {
		Lanius.mc.playerController.windowClick(Lanius.mc.player.openContainer.windowId, slot, mouseBtn, type,
				Lanius.mc.player);
	}

	public static int compareMaterials(ArmorMaterial material1, ArmorMaterial material2) {
		return armorPriorityMap.get(material1) - armorPriorityMap.get(material2);
	}

	public static int convertEquipSlotToSlotNum(EntityEquipmentSlot slot) {
		switch (slot) {
		case FEET:
			return ARMOR_END;
		case LEGS:
			return 7;
		case CHEST:
			return 6;
		case HEAD:
			return ARMOR_BEGIN;
		case OFFHAND:
			return SHIELD_SLOT;
		case MAINHAND:
			return Lanius.mc.player.inventory.currentItem + HOTBAR_BEGIN;
		default:
			return -1;
		}
	}

	public static void ensureInventory() {
		if (Lanius.mc.player.openContainer != Lanius.mc.player.inventoryContainer) {
			Lanius.mc.player.closeScreen();
		}
	}

	public static List<Slot> getSortedSlots(Container container) {
		List<Slot> sortedSlots = new ArrayList<>(container.inventorySlots);
		Collections.sort(sortedSlots, (o1, o2) -> Integer.compare(o1.slotNumber, o2.slotNumber));
		return sortedSlots;
	}

	public static boolean hasArmor(final EntityPlayer player) {
		for (int armorIdx = 0; armorIdx < player.inventory.armorInventory.size(); armorIdx++) {
			if (isStackValid(player.inventory.armorInventory.get(armorIdx))) {
				return true;
			}
		}
		return false;
	}

	public static boolean hasHeldItem(final EntityPlayer player) {
		boolean holdingItem = isStackValid(player.getHeldItemMainhand());
		if (!RoutineUtil.viaVersionEnabled()) {
			holdingItem |= isStackValid(player.getHeldItemOffhand());
		}
		return holdingItem;
	}

	public static boolean isInventoryFull(boolean checkStackSz) {
		for (int slotId = MIN_SLOT; slotId < MAX_SLOT; slotId++) {
			if (!Lanius.mc.player.inventoryContainer.getSlot(slotId).getHasStack()) {
				return false;
			}
			ItemStack stack = Lanius.mc.player.inventoryContainer.getSlot(slotId).getStack();
			if (checkStackSz && stack.isStackable() && stack.getCount() < stack.getMaxStackSize()) {
				return false;
			}
		}
		return true;
	}

	public static boolean isSlotDefenseEquipment(int slotIdx) {
		return slotIdx >= ARMOR_BEGIN && slotIdx <= ARMOR_END || slotIdx == SHIELD_SLOT;
	}

	public static boolean isStackInSlotsAndNotFull(ItemStack stack, List<Slot> slots) {
		for (Slot slot : slots) {
			if (!slot.getHasStack()) {
				continue;
			}
			if (stack.isStackable() && ItemStack.areItemStackTagsEqual(stack, slot.getStack())
					&& stack.getItemDamage() == slot.getStack().getItemDamage()) {
				return true;
			}
		}
		return false;
	}

	public static boolean isStackValid(ItemStack stack) {
		return stack != null && stack != ItemStack.EMPTY && stack.getItem() != Items.AIR;
	}

	public static void putStackInHotbar(final ItemStack stack) {
		if (!Lanius.mc.player.capabilities.isCreativeMode) {
			return;
		}
		Lanius.mc.player.inventoryContainer.putStackInSlot(HOTBAR_BEGIN, stack);
		Lanius.mc.playerController.sendSlotPacket(stack, HOTBAR_BEGIN);
	}

	public static void switchItem(final int newItem) {
		if (newItem != Lanius.mc.player.inventory.currentItem) {
			Lanius.mc.player.inventory.currentItem = newItem;
			Lanius.mc.player.connection.sendPacket(new CPacketHeldItemChange(Lanius.mc.player.inventory.currentItem));
		}
	}

	/**
	 * Sends an invalid ClickWindow packet with unmatching ItemStacks to force the
	 * server to resend the player's inventory.
	 */
	public static void syncInventory() {
		ensureInventory();

		// Eric: checks if the player has something in their inventory (this is required
		// if I recall correctly):
		int slotId;
		for (slotId = MIN_SLOT; slotId < MAX_SLOT; slotId++) {
			if (Lanius.mc.player.inventoryContainer.getSlot(slotId).getHasStack()) {
				break;
			}
		}

		if (slotId < MAX_SLOT) {
			syncSlot(Lanius.mc.player.openContainer.getSlot(slotId));
		}
	}

	public static void syncSlot(Slot slot) {
		Lanius.mc.player.connection.sendPacket(new CPacketClickWindow(Lanius.mc.player.openContainer.windowId, -1, 0,
				ClickType.QUICK_MOVE, slot.getStack(),
				Lanius.mc.player.openContainer.getNextTransactionID(Lanius.mc.player.inventory)));
	}
}
