package org.bitbucket.lanius.util;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.routine.Routine;

import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public final class CommandUtil {

	public static void addEnabledMsg(final Routine routine) {
		CommandUtil.addText(RoutineUtil.stateText(routine, true) + " routine " + routine + ".");
	}

	public static void addMessage(ITextComponent message) {
		final ITextComponent prefix = new TextComponentString("[" + Lanius.NAME + "]: ");
		prefix.getStyle().setColor(TextFormatting.RED);
		prefix.appendSibling(message);
		message.getStyle().setColor(TextFormatting.RESET);
		Lanius.mc.player.sendMessage(prefix);
	}

	public static void addText(String text) {
		addMessage(new TextComponentString(text));
	}

	public static String concatArgs(String[] args) {
		String argStr = "";
		for (int i = 0; i < args.length; i++) {
			argStr += args[i];
			if (i < args.length - 1) {
				argStr += " ";
			}
		}
		return argStr;
	}

}
