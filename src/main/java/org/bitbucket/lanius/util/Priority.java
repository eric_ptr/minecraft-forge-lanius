package org.bitbucket.lanius.util;

import java.util.HashMap;
import java.util.Map;

public enum Priority {
	HIGHEST, HIGHER, HIGH, NORMAL, LOW, LOWER, LOWEST;

	public static final Map<String, Priority> priorities = new HashMap<>();

	static {
		for (Priority priority : values()) {
			priorities.put(priority.toString().toUpperCase(), priority);
		}
	}

	public static Priority getById(String id) {
		return priorities.get(id.toUpperCase());
	}
}
