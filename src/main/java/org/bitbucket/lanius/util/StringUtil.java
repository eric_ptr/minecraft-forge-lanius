package org.bitbucket.lanius.util;

public final class StringUtil {
	public static String configName(final String name) {
		return name.toLowerCase().replace(' ', '_');
	}
}
