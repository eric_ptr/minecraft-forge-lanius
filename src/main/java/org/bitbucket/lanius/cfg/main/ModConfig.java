package org.bitbucket.lanius.cfg.main;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cfg.ClampedNumber;
import org.bitbucket.lanius.cfg.Configurable;

public class ModConfig implements Configurable {
	private static enum EntryType {
		BOOLEAN, FLOAT, INT, STRING
	}

	private static final File cfgFile = new File(Lanius.dataDir, "main.cfg");
	private static final Pattern cfgEntryRegex = Pattern.compile("([^:]+):([^:]+):([^:]*):([^:]*)"),
			rangedEntryRegex = Pattern.compile("([^:]+):([^:]+):([^:]+):([^:]+):([^:]*):([^:]*)");
	private final Map<ModConfigKey, ModConfigVal<Boolean>> boolVals = new HashMap<>();
	private final Map<ModConfigKey, ModConfigVal<ClampedNumber<Float>>> floatVals = new HashMap<>();
	private final Map<ModConfigKey, ModConfigVal<ClampedNumber<Integer>>> intVals = new HashMap<>();
	private final Map<ModConfigKey, ModConfigVal<String>> strVals = new HashMap<>();

	public boolean getBoolean(String name, String category, boolean defaultValue, String comment) {
		ModConfigKey cfgKey = new ModConfigKey(name, category);
		if (!boolVals.containsKey(cfgKey)) {
			boolVals.put(cfgKey, new ModConfigVal<>(comment, defaultValue));
		}
		return boolVals.get(cfgKey).getValue();
	}

	public float getFloat(String name, String category, float defaultValue, float minValue, float maxValue,
			String comment) {
		ModConfigKey cfgKey = new ModConfigKey(name, category);
		if (!floatVals.containsKey(cfgKey)) {
			floatVals.put(cfgKey, new ModConfigVal<>(comment, new ClampedNumber<>(defaultValue, minValue, maxValue)));
		}
		return floatVals.get(cfgKey).getValue().floatValue();
	}

	public int getInt(String name, String category, int defaultValue, int minValue, int maxValue, String comment) {
		ModConfigKey cfgKey = new ModConfigKey(name, category);
		if (!intVals.containsKey(cfgKey)) {
			intVals.put(cfgKey, new ModConfigVal<>(comment, new ClampedNumber<>(defaultValue, minValue, maxValue)));
		}
		return intVals.get(cfgKey).getValue().intValue();
	}

	private Map<ModConfigKey, ?> getMapForType(EntryType type) {
		switch (type) {
		case BOOLEAN:
			return boolVals;
		case FLOAT:
			return floatVals;
		case INT:
			return intVals;
		default:
			return strVals;
		}
	}

	public String getString(String name, String category, String defaultValue, String comment) {
		ModConfigKey cfgKey = new ModConfigKey(name, category);
		if (!strVals.containsKey(cfgKey)) {
			strVals.put(cfgKey, new ModConfigVal<>(comment, defaultValue));
		}
		return strVals.get(cfgKey).getValue();
	}

	@Override
	public void load() {
		boolVals.clear();
		floatVals.clear();
		intVals.clear();
		strVals.clear();
		if (!cfgFile.exists()) {
			return;
		}
		try (BufferedReader in = new BufferedReader(new FileReader(cfgFile))) {
			String line;
			EntryType currentType = null;
			while ((line = in.readLine()) != null) {
				if (!cfgEntryRegex.matcher(line).matches() && !rangedEntryRegex.matcher(line).matches()) {
					currentType = EntryType.valueOf(line.trim());
					continue;
				}
				if (currentType.equals(EntryType.BOOLEAN) || currentType.equals(EntryType.STRING)) {
					Matcher fieldMatcher = cfgEntryRegex.matcher(line);
					fieldMatcher.matches();
					switch (currentType) {
					case BOOLEAN:
						getBoolean(fieldMatcher.group(2), fieldMatcher.group(1),
								Boolean.parseBoolean(fieldMatcher.group(3)), fieldMatcher.group(4));
						break;
					default:
						getString(fieldMatcher.group(2), fieldMatcher.group(1), fieldMatcher.group(3),
								fieldMatcher.group(4));
					}
				} else {
					Matcher fieldMatcher = rangedEntryRegex.matcher(line);
					fieldMatcher.matches();
					switch (currentType) {
					case FLOAT:
						getFloat(fieldMatcher.group(2), fieldMatcher.group(1), Float.parseFloat(fieldMatcher.group(5)),
								Float.parseFloat(fieldMatcher.group(3)), Float.parseFloat(fieldMatcher.group(4)),
								fieldMatcher.group(6));
						break;
					default:
						getInt(fieldMatcher.group(2), fieldMatcher.group(1), Integer.parseInt(fieldMatcher.group(5)),
								Integer.parseInt(fieldMatcher.group(3)), Integer.parseInt(fieldMatcher.group(4)),
								fieldMatcher.group(6));
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void save() {
		cfgFile.delete();
		try (PrintWriter out = new PrintWriter(new BufferedWriter((new FileWriter(cfgFile))))) {
			for (EntryType type : EntryType.values()) {
				Map<ModConfigKey, ?> values = getMapForType(type);
				out.println(type.toString());
				for (Map.Entry<ModConfigKey, ?> entry : values.entrySet()) {
					ModConfigVal<?> value = (ModConfigVal<?>) entry.getValue();
					out.print(entry.getKey() + ":");
					if (value.getValue() instanceof ClampedNumber<?>) {
						ClampedNumber<?> entryNum = (ClampedNumber<?>) value.getValue();
						out.print(entryNum.getMin() + ":" + entryNum.getMax() + ":");
					}
					out.println(value);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setBoolean(String name, String category, boolean defaultValue, String comment) {
		getBoolean(name, category, defaultValue, comment);
		boolVals.put(new ModConfigKey(name, category), new ModConfigVal<>(comment, defaultValue));
	}

	public void setFloat(String name, String category, float defaultValue, float minValue, float maxValue,
			String comment) {
		getFloat(name, category, defaultValue, minValue, maxValue, comment);
		floatVals.put(new ModConfigKey(name, category),
				new ModConfigVal<>(comment, new ClampedNumber<>(defaultValue, minValue, maxValue)));
	}

	public void setInt(String name, String category, int defaultValue, int minValue, int maxValue, String comment) {
		getInt(name, category, defaultValue, minValue, maxValue, comment);
		intVals.put(new ModConfigKey(name, category),
				new ModConfigVal<>(comment, new ClampedNumber<>(defaultValue, minValue, maxValue)));
	}

	public void setString(String name, String category, String defaultValue, String comment) {
		getString(name, category, defaultValue, comment);
		strVals.put(new ModConfigKey(name, category), new ModConfigVal<>(comment, defaultValue));
	}
}
