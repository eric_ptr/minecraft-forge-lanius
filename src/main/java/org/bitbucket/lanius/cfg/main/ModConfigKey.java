package org.bitbucket.lanius.cfg.main;

import java.util.Objects;

public class ModConfigKey {
	private final String entryName, categoryName;

	public ModConfigKey(String entryName, String categoryName) {
		this.entryName = entryName;
		this.categoryName = categoryName;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ModConfigKey)) {
			return false;
		}
		ModConfigKey other = (ModConfigKey) obj;
		return other.categoryName.equals(categoryName) && other.entryName.equals(entryName);
	}

	public String getCategoryName() {
		return categoryName;
	}

	public String getEntryName() {
		return entryName;
	}

	@Override
	public int hashCode() {
		return Objects.hash(categoryName, entryName);
	}

	@Override
	public String toString() {
		return categoryName + ":" + entryName;
	}
}
