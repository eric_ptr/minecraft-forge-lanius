/**
 * Provides a system of configurations similar to that of Forge's since we are aiming to break away from Forge APIs. This package will likely be removed during further refactoring.
 */
/**
 * @author Eric
 *
 */
package org.bitbucket.lanius.cfg.main;