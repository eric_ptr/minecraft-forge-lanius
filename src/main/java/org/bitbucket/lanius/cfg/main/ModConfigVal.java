package org.bitbucket.lanius.cfg.main;

public class ModConfigVal<T> {
	private final String desc;
	private T value;

	public ModConfigVal(String desc, T value) {
		this.desc = desc;
		this.value = value;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ModConfigVal<?>)) {
			return false;
		}
		ModConfigVal<?> other = (ModConfigVal<?>) obj;
		return other.value.equals(value);
	}

	public String getDesc() {
		return desc;
	}

	public T getValue() {
		return value;
	}

	@Override
	public int hashCode() {
		return value.hashCode();
	}

	@Override
	public String toString() {
		return value.toString() + ":" + desc;
	}
}
