/**
 * Provides the classes of the macro key bind system.
 */
/**
 * @author Eric
 *
 */
package org.bitbucket.lanius.cfg.macro;