package org.bitbucket.lanius.cfg.macro;

import java.util.HashMap;
import java.util.Map;

public enum InputType {
	ON_DOWN("down",
			(id, cmd, keyCode, inType, engineType, execPriority, removable) -> new Macro(id, cmd, keyCode, inType,
					engineType, execPriority, removable)),
	ON_PRESS("press", (id, cmd, keyCode, inType, engineType, execPriority, removable) -> new PressableMacro(id, cmd,
			keyCode, engineType, execPriority, removable));

	private static final Map<String, InputType> types = new HashMap<>();

	static {
		for (InputType type : values()) {
			types.put(type.id.toUpperCase(), type);
		}
	}

	public static InputType getById(String id) {
		return types.get(id.toUpperCase());
	}

	private final String id;

	private final MacroFactory factory;

	private InputType(String id, MacroFactory factory) {
		this.id = id;
		this.factory = factory;
	}

	public MacroFactory getFactory() {
		return factory;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return id;
	}
}