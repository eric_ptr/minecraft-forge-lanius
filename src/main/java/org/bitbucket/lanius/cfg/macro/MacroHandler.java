package org.bitbucket.lanius.cfg.macro;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cfg.Configurable;
import org.bitbucket.lanius.cmd.CommandSource;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.util.Priority;
import org.bitbucket.lanius.util.game.InputHelper;
import org.lwjgl.input.Keyboard;

import net.minecraftforge.fml.common.eventhandler.EventPriority;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public final class MacroHandler implements Configurable {
	private static final File macrosFile = new File(Lanius.dataDir, "macros.cfg");
	private static final Pattern macroCfgRegex = Pattern.compile("'(.+)':'(.+)':(.+):(.+):([0-9]+):(.+):(.+)");
	private final List<Macro> activeMacros = new ArrayList<>();

	public boolean addMacro(Macro macro) {
		if (activeMacros.contains(macro)) {
			return false;
		}
		activeMacros.add(macro);
		sortMacros();
		return true;
	}

	public void clearMacros() {
		Iterator<Macro> macroIt = activeMacros.iterator();
		while (macroIt.hasNext()) {
			final Macro macro = macroIt.next();
			if (macro.isRemovable()) {
				macroIt.remove();
			}
		}
	}

	private void execMacro(Macro macro) {
		String prefix;
		switch (macro.getEngineType()) {
		case VANILLA:
			prefix = "/";
			break;
		case LANIUS:
			prefix = ModCommand.prefix();
			break;
		case CHAT:
		default:
			prefix = "";
			break;
		}
		CommandSource prevExecSrc = ModCommand.getExecSrc();
		ModCommand.setExecSrc(CommandSource.MACRO);
		InputHelper.simulateChatInput(prefix + macro.getCmd(), false);
		ModCommand.setExecSrc(prevExecSrc);
	}

	public Macro getMacro(String id) {
		for (Macro macro : activeMacros) {
			if (macro.getId().equals(id)) {
				return macro;
			}
		}
		return null;
	}

	public Macro[] getMacros() {
		return activeMacros.toArray(new Macro[0]);
	}

	@Override
	public void load() {
		clearMacros();
		if (!macrosFile.exists()) {
			return;
		}
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(macrosFile));
			String line;
			while ((line = in.readLine()) != null) {
				Matcher fieldMatcher = macroCfgRegex.matcher(line);
				if (!fieldMatcher.matches()) {
					System.err.println("Skipping bad entry in file " + macrosFile + ": \"" + line + "\"");
					continue;
				}
				String id = fieldMatcher.group(1);
				String cmd = fieldMatcher.group(2);
				String rawKeyName = fieldMatcher.group(3);
				int keyCode = rawKeyName.equalsIgnoreCase(InputHelper.NO_KEY_NAME) ? Keyboard.KEY_NONE
						: Keyboard.getKeyIndex(rawKeyName); // TODO(Eric) support mouse buttons.
				InputType inType = InputType.getById(fieldMatcher.group(4));
				EngineType engineType = EngineType.values()[Integer.parseInt(fieldMatcher.group(5))];
				Priority execPriority = Priority.getById(fieldMatcher.group(6));
				boolean removable = Boolean.parseBoolean(fieldMatcher.group(7));
				if (!removable) {
					// TODO(Eric) for now, non-removable macros cannot change input type.
					Macro nonRemovableMacro = getMacro(id);
					nonRemovableMacro.setCmd(cmd);
					nonRemovableMacro.setEngineType(engineType);
					nonRemovableMacro.setExecPriority(execPriority);
					nonRemovableMacro.setKeyCode(keyCode);
				} else {
					addMacro(inType.getFactory().createMacro(id, cmd, keyCode, inType, engineType, execPriority,
							removable));
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@SubscribeEvent(priority = EventPriority.HIGHEST)
	public void onClientTickHighest(final TickEvent.ClientTickEvent event) {
		if (!event.phase.equals(TickEvent.Phase.END) || Lanius.mc.player == null || Lanius.mc.currentScreen != null) {
			return;
		}
		for (Macro macro : activeMacros) {
			if (macro instanceof PressableMacro) {
				PressableMacro pressableMacro = (PressableMacro) macro;
				if (pressableMacro.wasPressed()) {
					execMacro(pressableMacro);
				}
				pressableMacro.setPrevKeyDown(InputHelper.isKeyDown(pressableMacro.getKeyCode()));
			} else if (macro.getInType().equals(InputType.ON_DOWN) && InputHelper.isKeyDown(macro.getKeyCode())) {
				execMacro(macro);
			}
		}
	}

	public boolean removeMacro(String id) {
		Macro foundMacro = getMacro(id);
		if (foundMacro == null || !foundMacro.isRemovable()) {
			return false;
		}
		activeMacros.remove(foundMacro);
		return true;
	}

	@Override
	public void save() {
		macrosFile.delete();
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter((new FileWriter(macrosFile))));
			for (Macro macro : activeMacros) {
				out.println(
						"'" + macro.getId() + "':'" + macro.getCmd() + "':" + InputHelper.getKeyName(macro.getKeyCode())
								+ ":" + macro.getInType().getId() + ":" + macro.getEngineType().ordinal() + ":"
								+ macro.getExecPriority().toString() + ":" + macro.isRemovable());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
			}
		}
	}

	public void sortMacros() {
		Collections.sort(activeMacros, (o1, o2) -> o1.getId().compareTo(o2.getId()));
		Collections.sort(activeMacros);
	}
}
