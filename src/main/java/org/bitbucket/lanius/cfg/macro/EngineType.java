package org.bitbucket.lanius.cfg.macro;

import org.bitbucket.lanius.cmd.ModCommand;

public enum EngineType {
	VANILLA, LANIUS, CHAT;

	public static EngineType getForMessage(String message) {
		if (message.startsWith("/")) {
			return VANILLA;
		} else if (message.startsWith(ModCommand.prefix())) {
			return LANIUS;
		} else {
			return CHAT;
		}
	}
}
