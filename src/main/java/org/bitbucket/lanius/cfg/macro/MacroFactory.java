package org.bitbucket.lanius.cfg.macro;

import org.bitbucket.lanius.util.Priority;

public interface MacroFactory {
	Macro createMacro(String id, String cmd, int keyCode, InputType inType, EngineType engineType,
			Priority execPriority, boolean removable);
}
