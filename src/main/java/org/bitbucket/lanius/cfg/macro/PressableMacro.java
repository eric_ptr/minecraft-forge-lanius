package org.bitbucket.lanius.cfg.macro;

import java.io.Serializable;

import org.bitbucket.lanius.util.Priority;
import org.bitbucket.lanius.util.game.InputHelper;

public class PressableMacro extends Macro implements Serializable {
	private static final long serialVersionUID = 1L;
	private boolean prevKeyDown;

	public PressableMacro(String id, String cmd, int keyCode, EngineType engineType, Priority execPriority,
			boolean removable) {
		super(id, cmd, keyCode, InputType.ON_PRESS, engineType, execPriority, removable);
	}

	public void setPrevKeyDown(boolean prevKeyDown) {
		this.prevKeyDown = prevKeyDown;
	}

	public boolean wasPressed() {
		return prevKeyDown && !InputHelper.isKeyDown(getKeyCode());
	}
}
