package org.bitbucket.lanius.cfg.macro;

import java.io.Serializable;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.util.Priority;

public class Macro implements Serializable, Comparable<Macro> {
	private static final long serialVersionUID = 1L;
	private final String id;
	private String cmd;
	private int keyCode;
	private InputType inType;
	private EngineType engineType;
	private Priority execPriority;
	private boolean removable;

	public Macro(String id, String cmd, int keyCode, InputType inType, EngineType engineType, Priority execPriority,
			boolean removable) {
		this.id = id;
		this.cmd = cmd;
		this.keyCode = keyCode;
		this.inType = inType;
		this.engineType = engineType;
		this.execPriority = execPriority;
		this.removable = removable;
	}

	@Override
	public int compareTo(Macro o) {
		return execPriority.compareTo(o.execPriority);
	}

	public Macro createWithInType(InputType inType) {
		return inType.getFactory().createMacro(id, cmd, keyCode, inType, engineType, execPriority, removable);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Macro)) {
			return false;
		}
		Macro other = (Macro) obj;
		return other.id.toUpperCase().equals(id.toUpperCase());
	}

	public String getCmd() {
		return cmd;
	}

	public EngineType getEngineType() {
		return engineType;
	}

	public Priority getExecPriority() {
		return execPriority;
	}

	public String getId() {
		return id;
	}

	public InputType getInType() {
		return inType;
	}

	public int getKeyCode() {
		return this.keyCode;
	}

	@Override
	public int hashCode() {
		return id.toUpperCase().hashCode();
	}

	public boolean isRemovable() {
		return removable;
	}

	public void setCmd(String cmd) {
		this.cmd = cmd;
	}

	public void setEngineType(EngineType engineType) {
		this.engineType = engineType;
	}

	public void setExecPriority(Priority execPriority) {
		this.execPriority = execPriority;
		Lanius.getInstance().getMacroHandler().sortMacros();
	}

	public void setKeyCode(int keyCode) {
		this.keyCode = keyCode;
	}

	@Override
	public String toString() {
		return id;
	}
}
