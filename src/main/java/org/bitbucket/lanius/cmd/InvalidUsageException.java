package org.bitbucket.lanius.cmd;

public final class InvalidUsageException extends ModCommandException {
	/**
	 * The default serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	public InvalidUsageException(String usageStr) {
		super("Usage: " + usageStr);
	}
}
