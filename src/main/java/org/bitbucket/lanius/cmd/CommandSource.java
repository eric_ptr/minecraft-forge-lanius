package org.bitbucket.lanius.cmd;

public enum CommandSource {
	CHAT_INPUT, MACRO
}
