package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.CommandSource;
import org.bitbucket.lanius.cmd.RoutineCommand;
import org.bitbucket.lanius.routine.Routine;
import org.bitbucket.lanius.util.CommandUtil;

public final class ToggleCommand extends RoutineCommand {

	public ToggleCommand() {
		super("toggle", "t");
	}

	@Override
	protected void handleRoutine(final Routine foundRoutine, String[] args) {
		foundRoutine.setEnabled();
		if (getExecSrc().equals(CommandSource.CHAT_INPUT)
				&& !Lanius.getInstance().getModCfg().getBoolean("Toggle Messages", Routine.CFG_CATEGORY, false,
						"Determines whether or not to display a toggle message when a routine is toggled.")) {
			CommandUtil.addEnabledMsg(foundRoutine);
		}
	}

}
