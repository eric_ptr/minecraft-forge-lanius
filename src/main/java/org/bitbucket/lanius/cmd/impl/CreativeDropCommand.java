package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.InvalidObjectException;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.impl.CreativeDropRoutine;
import org.bitbucket.lanius.util.CommandUtil;

import net.minecraft.item.Item;

public final class CreativeDropCommand extends ModCommand {

	public CreativeDropCommand() {
		super("creativedrop", "drop");
	}

	@Override
	protected String getParamUsage() {
		return "<add|remove> <item> [item2]";
	}

	@Override
	public void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length <= 0);
		final boolean addParam = args[0].equalsIgnoreCase("add");
		assertUsage(!addParam && !args[0].equalsIgnoreCase("remove") || args.length <= 1);
		final CreativeDropRoutine dropRoutine = (CreativeDropRoutine) Lanius.getInstance().getRoutineRegistry()
				.get("Creative Drop");
		for (int argIdx = 1; argIdx < args.length; argIdx++) {
			args[argIdx] = args[argIdx].toLowerCase();
			if (Item.getByNameOrId(args[argIdx]) == null) {
				throw new InvalidObjectException(args[argIdx]);
			}
			if (addParam) {
				dropRoutine.putExempt(args[argIdx]);
			} else {
				dropRoutine.removeExempt(args[argIdx]);
			}
			CommandUtil.addText((addParam ? "Added" : "Removed") + " " + args[argIdx] + " " + (addParam ? "to" : "from")
					+ " the list of exempt items.");
		}
	}

}
