package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.impl.BreadcrumbsRoutine;
import org.bitbucket.lanius.util.CommandUtil;

public final class BreadcrumbsCommand extends ModCommand {

	public BreadcrumbsCommand() {
		super("breadcrumbs", "crumbs", "bc");
	}

	@Override
	protected String getParamUsage() {
		return "<clear|positions>";
	}

	@Override
	public void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length <= 0);
		final BreadcrumbsRoutine breadcrumbsRoutine = (BreadcrumbsRoutine) Lanius.getInstance().getRoutineRegistry()
				.get("Breadcrumbs");
		if (args[0].equalsIgnoreCase("clear")) {
			breadcrumbsRoutine.clearPositions();
			CommandUtil.addText("Removed all positions from the trail.");
		} else {
			final int posRemoved = breadcrumbsRoutine.removePositions(Integer.parseInt(args[0]));
			CommandUtil.addText(
					"Removed " + posRemoved + " " + (posRemoved == 1 ? "position" : "positions") + " from the trail.");
		}
	}

}
