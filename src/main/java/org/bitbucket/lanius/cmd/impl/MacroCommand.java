package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cfg.macro.EngineType;
import org.bitbucket.lanius.cfg.macro.InputType;
import org.bitbucket.lanius.cfg.macro.Macro;
import org.bitbucket.lanius.cmd.InvalidObjectException;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.util.CommandUtil;
import org.bitbucket.lanius.util.Priority;
import org.bitbucket.lanius.util.game.InputHelper;
import org.lwjgl.input.Keyboard;

public final class MacroCommand extends ModCommand {
	public MacroCommand() {
		super("macro", "m", "key", "bind", "keybind", "kb");
	}

	private String getCmdParam(String argStr, int cmdIdx, String[] args) {
		return argStr.substring(argStr.lastIndexOf(args[cmdIdx - 1]) + args[cmdIdx - 1].length() + 1);
	}

	@Override
	protected String getParamUsage() {
		return "<add|remove|clear|set> [id] [key] [inputType] [priority] [cmd]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length <= 0);
		assertUsage(!args[0].equalsIgnoreCase("add") && !args[0].equalsIgnoreCase("remove")
				&& !args[0].equalsIgnoreCase("clear") && !args[0].equalsIgnoreCase("set"));
		if (args[0].equalsIgnoreCase("add")) {
			assertUsage(args.length <= 5);
			String cmd = getCmdParam(CommandUtil.concatArgs(args), 5, args);
			String keyName = args[2].toUpperCase();
			InputType inType = InputType.getById(args[3]);
			if (inType == null) {
				throw new InvalidObjectException(args[3]);
			}
			Priority priority = Priority.getById(args[4]);
			if (priority == null) {
				throw new InvalidObjectException(args[4]);
			}
			if (!Lanius.getInstance().getMacroHandler().addMacro(new Macro(args[1], cmd.substring(1),
					Keyboard.getKeyIndex(keyName), inType, EngineType.getForMessage(cmd), priority, true))) {
				CommandUtil.addText("Failed to add macro \"" + args[1] + "\" as it already exists.");
			} else {
				CommandUtil.addText(
						"Added macro \"" + args[1] + "\" with command \"" + cmd + "\" to be executed using the "
								+ keyName + " key on " + inType + " with priority " + priority + ".");
			}
		} else if (args[0].equalsIgnoreCase("remove")) {
			assertUsage(args.length <= 1);
			if (!Lanius.getInstance().getMacroHandler().removeMacro(args[1])) {
				CommandUtil.addText(
						"Failed to remove macro \"" + args[1] + "\" as it is non-removable or does not exist.");
			} else {
				CommandUtil.addText("Removed macro " + args[1] + ".");
			}
		} else if (args[0].equalsIgnoreCase("set")) {
			assertUsage(args.length <= 2);
			Macro foundMacro = Lanius.getInstance().getMacroHandler().getMacro(args[1]);
			if (foundMacro == null) {
				throw new InvalidObjectException(args[1]);
			}
			for (int i = 2; i < Math.min(6, args.length); i++) {
				if (i == 2) {
					foundMacro.setKeyCode(Keyboard.getKeyIndex(args[i].toUpperCase()));
					CommandUtil.addText("Set macro " + args[1] + "'s key to "
							+ InputHelper.getKeyName(foundMacro.getKeyCode()) + ".");
				} else if (i == 3) {
					InputType inType = InputType.getById(args[i]);
					if (inType == null) {
						throw new InvalidObjectException(args[i]);
					}
					if (foundMacro.isRemovable()) {
						foundMacro = foundMacro.createWithInType(inType);
						Lanius.getInstance().getMacroHandler().removeMacro(args[1]);
						Lanius.getInstance().getMacroHandler().addMacro(foundMacro);
						CommandUtil.addText("Set macro " + args[1] + "'s input type to " + inType + ".");
					} else {
						CommandUtil.addText("Failed to change macro " + args[1] + "'s input type to " + inType
								+ " as the macro is non-removable.");
					}
				} else if (i == 4) {
					Priority priority = Priority.getById(args[i]);
					if (priority == null) {
						throw new InvalidObjectException(args[i]);
					}
					foundMacro.setExecPriority(priority);
					CommandUtil.addText("Set macro " + args[1] + "'s priority to " + priority + ".");
				} else if (i == 5) {
					String cmd = getCmdParam(CommandUtil.concatArgs(args), 5, args);
					foundMacro.setCmd(cmd.substring(1));
					foundMacro.setEngineType(EngineType.getForMessage(cmd));
					CommandUtil.addText("Set macro " + args[1] + "'s command to \"" + cmd + ".\"");
				}
			}
		} else {
			Lanius.getInstance().getMacroHandler().clearMacros();
			CommandUtil.addText("Removed all removable macros.");
		}
	}
}
