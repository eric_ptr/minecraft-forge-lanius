package org.bitbucket.lanius.cmd.impl;

import java.util.Map.Entry;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.impl.BlinkRoutine;
import org.bitbucket.lanius.routine.impl.FreecamRoutine;
import org.bitbucket.lanius.routine.impl.NameProtectRoutine;
import org.bitbucket.lanius.util.CommandUtil;
import org.bitbucket.lanius.util.game.NetworkUtil;

import net.minecraft.client.entity.EntityOtherPlayerMP;
import net.minecraft.entity.player.EntityPlayer;

public final class PingCommand extends ModCommand {

	public PingCommand() {
		super("ping");
	}

	@Override
	protected String getParamUsage() {
		return "[player]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		if (args.length == 0) {
			CommandUtil.addText("Your ping is "
					+ NetworkUtil.lagTime(Lanius.mc.player.connection.getPlayerInfo(Lanius.mc.player.getName()))
					+ " ms.");
		} else {
			String playerName = CommandUtil.concatArgs(args);
			NameProtectRoutine nameProt = (NameProtectRoutine) Lanius.getInstance().getRoutineRegistry()
					.get("Name Protect");
			if (nameProt.isEnabled()) {
				for (Entry<String, String> protName : nameProt.nameEntries()) {
					playerName.replace("-" + protName.getValue(), protName.getKey());
				}
			}
			int ping = -1;
			for (EntityPlayer player : Lanius.mc.world.playerEntities) {
				if ((player instanceof EntityOtherPlayerMP || Lanius.mc.player.equals(player))
						&& !player.equals(((FreecamRoutine) Lanius.getInstance().getRoutineRegistry().get("Freecam"))
								.getRenderEntity())
						&& !player.equals(
								((BlinkRoutine) Lanius.getInstance().getRoutineRegistry().get("Blink")).getPosEntity())
						&& player.getName().equalsIgnoreCase(playerName)) {
					ping = NetworkUtil.lagTime(Lanius.mc.player.connection.getPlayerInfo(player.getName()));
					playerName = player.getName();
					break;
				}
			}
			if (ping == -1) {
				throw new ModCommandException("Player: " + playerName + " is invalid");
			}
			CommandUtil.addText(playerName + "'s ping is " + ping + " ms.");
		}
	}
}
