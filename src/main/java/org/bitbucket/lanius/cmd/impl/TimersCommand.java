package org.bitbucket.lanius.cmd.impl;

import java.util.HashSet;
import java.util.Set;

import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.util.CommandUtil;
import org.bitbucket.lanius.util.concurrent.Timer;

import net.minecraft.util.math.MathHelper;

public final class TimersCommand extends ModCommand {

	public final Set<Timer> timers = new HashSet<>();

	public TimersCommand() {
		super("timers", "timer", "time");
	}

	@Override
	protected String getParamUsage() {
		return "<add|remove|clear> [duration|up|name] [name]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length <= 0);
		if (args[0].equalsIgnoreCase("clear")) {
			for (final Timer timer : timers) {
				timer.setTiming(false);
			}
			timers.clear();
			CommandUtil.addText("Removed all timers.");
		} else if (args[0].equalsIgnoreCase("remove")) {
			assertUsage(args.length <= 1);
			final Timer timer = new Timer(argumentStr(args, 1));
			timer.setTiming(false);
			timers.remove(timer);
			CommandUtil.addText("Removed timer \"" + timer.name + ".\"");
		} else if (args[0].equalsIgnoreCase("add")) {
			assertUsage(args.length <= 2);
			if (args[1].equalsIgnoreCase("up")) {
				final Timer timer = new Timer(argumentStr(args, 2));
				timers.add(timer);
				timer.start();
				CommandUtil.addText("Added an upwards-counting timer with name \"" + timer.name + ".\"");
			} else {
				final String[] duration = args[1].split(":");
				int hours = duration.length == 3 ? Integer.parseInt(duration[0]) : 0,
						minutes = duration.length > 1 ? Integer.parseInt(duration[1]) : 0,
						seconds = Integer.parseInt(duration[0]);
				if (seconds > 59) {
					final float addMinutes = seconds / 60.0F;
					minutes += addMinutes;
					seconds = Math.round((addMinutes - MathHelper.floor(addMinutes)) * 60.0F);
				}
				if (minutes > 59) {
					final float addHours = minutes / 60.0F;
					hours += addHours;
					minutes = Math.round((addHours - MathHelper.floor(addHours)) * 60.0F);
				}
				final Timer timer = new Timer(argumentStr(args, 2), hours, minutes, seconds);
				timers.add(timer);
				timer.start();
				CommandUtil.addText("Added a timer starting from " + (hours < 10 ? "0" + hours : hours) + ":"
						+ (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds)
						+ " with name \"" + timer.name + ".\"");
			}
		}
	}

}
