package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.InvalidObjectException;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.impl.SearchRoutine;
import org.bitbucket.lanius.util.CommandUtil;

import net.minecraft.block.Block;

public final class SearchCommand extends ModCommand {

	public SearchCommand() {
		super("search");
	}

	@Override
	protected String getParamUsage() {
		return "<add|remove|clear> [block] [block2]";
	}

	@Override
	public void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length <= 0);
		SearchRoutine searchRoutine = (SearchRoutine) Lanius.getInstance().getRoutineRegistry().get("Search");
		if (args[0].equalsIgnoreCase("clear")) {
			CommandUtil.addText("Removed all blocks from the search list.");
			searchRoutine.clearBlocks();
			return;
		}
		final boolean addParam = args[0].equalsIgnoreCase("add");
		assertUsage(!addParam && !args[0].equalsIgnoreCase("remove") || args.length <= 1);
		for (int argIdx = 1; argIdx < args.length; argIdx++) {
			args[argIdx] = args[argIdx].toLowerCase();
			if (Block.getBlockFromName(args[argIdx]) == null) {
				throw new InvalidObjectException(args[argIdx]);
			}
			if (addParam) {
				searchRoutine.putBlock(args[argIdx]);
			} else {
				searchRoutine.removeBlock(args[argIdx]);
			}
			CommandUtil.addText((addParam ? "Added" : "Removed") + " " + args[argIdx] + " " + (addParam ? "to" : "from")
					+ " the list of selected blocks.");
		}
	}

}
