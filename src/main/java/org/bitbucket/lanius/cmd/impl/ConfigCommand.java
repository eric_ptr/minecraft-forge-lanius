package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cfg.ConfigContainer;
import org.bitbucket.lanius.cmd.InvalidObjectException;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.util.CommandUtil;

public final class ConfigCommand extends ModCommand {

	public ConfigCommand() {
		super("configuration", "config", "cfg");
	}

	@Override
	protected String getParamUsage() {
		return "<set|reload|save> ['category'] ['name'] [value]";
	}

	@Override
	public void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(
				args.length <= 0
						|| !args[0].equalsIgnoreCase("set") && !args[0].equalsIgnoreCase("reload")
								&& !args[0].equalsIgnoreCase("save")
						|| args[0].equalsIgnoreCase("set") && args.length < 4);
		final Lanius instance = Lanius.getInstance();
		if (args[0].equalsIgnoreCase("reload")) {
			instance.loadAllCfgs();
			CommandUtil.addText("Reloaded the configuration.");
		} else if (args[0].equalsIgnoreCase("save")) {
			instance.saveAllCfgs();
			CommandUtil.addText("Saved the current configuration.");
		} else {
			final String arguments = argumentStr(args, 1);
			final String CATEGORY_BOUND = "'";
			int boundCount = 0;
			for (int charIdx = 0; charIdx < arguments.length(); charIdx++) {
				if (Character.toString(arguments.charAt(charIdx)).equals(CATEGORY_BOUND)) {
					++boundCount;
				}
			}
			assertUsage(boundCount < 4);
			final String parsedCategory = arguments.substring(arguments.indexOf(CATEGORY_BOUND) + 1,
					arguments.substring(arguments.indexOf(CATEGORY_BOUND) + 1).indexOf(CATEGORY_BOUND) + 1);
			final ConfigContainer parsedCfg = instance.getCfgContainerRegistry().get(parsedCategory);
			final String parsedName = arguments.substring(
					arguments.indexOf(parsedCategory) + parsedCategory.length() + CATEGORY_BOUND.length() + " ".length()
							+ CATEGORY_BOUND.length(),
					arguments
							.substring(arguments.indexOf(parsedCategory) + parsedCategory.length()
									+ CATEGORY_BOUND.length() + " ".length() + CATEGORY_BOUND.length())
							.indexOf(CATEGORY_BOUND) + arguments.indexOf(parsedCategory) + parsedCategory.length()
							+ CATEGORY_BOUND.length() + " ".length() + CATEGORY_BOUND.length()),
					parsedVal = arguments.substring(arguments.indexOf(parsedName) + parsedName.length()
							+ CATEGORY_BOUND.length() + " ".length());
			if (parsedCfg == null) {
				throw new InvalidObjectException(parsedCategory);
			} else if (parsedCfg.getBoolean(parsedName) != null) {
				parsedCfg.putValue(parsedName, Boolean.parseBoolean(parsedVal));
				CommandUtil.addText("Set the value of \"" + parsedName + "\" in category \"" + parsedCategory + "\" to "
						+ parsedCfg.getBoolean(parsedName) + ".");
			} else if (parsedCfg.getFloat(parsedName) != null) {
				parsedCfg.getFloat(parsedName).setValue(Float.parseFloat(parsedVal));
				CommandUtil.addText("Set the value of \"" + parsedName + "\" in category \"" + parsedCategory + "\" to "
						+ parsedCfg.getFloat(parsedName) + ".");
			} else if (parsedCfg.getInt(parsedName) != null) {
				parsedCfg.getInt(parsedName).setValue(Integer.parseInt(parsedVal));
				CommandUtil.addText("Set the value of \"" + parsedName + "\" in category \"" + parsedCategory + "\" to "
						+ parsedCfg.getInt(parsedName) + ".");
			} else if (parsedCfg.getString(parsedName) != null) {
				parsedCfg.putValue(parsedName, parsedVal);
				CommandUtil.addText("Set the value of \"" + parsedName + "\" in category \"" + parsedCategory
						+ "\" to \"" + parsedCfg.getString(parsedName) + "\".");
			} else {
				throw new InvalidObjectException(parsedName);
			}
		}
	}

}
