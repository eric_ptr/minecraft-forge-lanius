package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.InvalidObjectException;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.cmd.WrongGameModeException;
import org.bitbucket.lanius.util.CommandUtil;
import org.bitbucket.lanius.util.game.InventoryUtil;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.PlayerNotFoundException;
import net.minecraft.inventory.ClickType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.JsonToNBT;
import net.minecraft.nbt.NBTException;

public final class GiveCommand extends ModCommand {

	public GiveCommand() {
		super("give");
	}

	@Override
	protected String getParamUsage() {
		return "<item> [amount] [data] [dataTag]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		if (!Lanius.mc.player.capabilities.isCreativeMode) {
			throw new WrongGameModeException("Creative");
		}
		assertUsage(args.length <= 0);
		args[0] = args[0].toLowerCase();
		final Item item = Item.getByNameOrId(args[0]);
		if (item == null) {
			throw new InvalidObjectException(args[0]);
		}
		final ItemStack prevStack = Lanius.mc.player.inventoryContainer.getSlot(InventoryUtil.HOTBAR_BEGIN).getStack();
		final int amount = args.length >= 2 ? Integer.parseInt(args[1]) : 1,
				data = args.length >= 3 ? Integer.parseInt(args[2]) : 0;
		final ItemStack newStack = new ItemStack(item, amount, data);
		String message = "Given the player a stack of " + amount + " " + args[0] + " with metadata " + data;
		if (args.length >= 4) {
			String tagJson = null;
			try {
				tagJson = CommandBase.getChatComponentFromNthArg(Lanius.mc.player, args, 3).getUnformattedText();
			} catch (PlayerNotFoundException e) {
				throw new ModCommandException(e.getMessage());
			} catch (CommandException e) {
				throw new ModCommandException(e.getMessage());
			}
			try {
				newStack.setTagCompound(JsonToNBT.getTagFromJson(tagJson));
			} catch (final NBTException nbtEx) {
				throw new ModCommandException(nbtEx.getMessage()); // TODO(Eric) tweak this error message.
			}
			message += " and data tag " + tagJson;
		}
		InventoryUtil.putStackInHotbar(newStack);
		InventoryUtil.clickWindow(InventoryUtil.HOTBAR_BEGIN, 1, ClickType.THROW);
		InventoryUtil.putStackInHotbar(prevStack);
		CommandUtil.addText(message + ".");
	}

}
