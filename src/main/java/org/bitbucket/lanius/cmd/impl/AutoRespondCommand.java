package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.InvalidObjectException;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.impl.AutoRespondRoutine;
import org.bitbucket.lanius.util.CommandUtil;

public final class AutoRespondCommand extends ModCommand {

	public AutoRespondCommand() {
		super("autorespond", "respond");
	}

	@Override
	protected String getParamUsage() {
		return "<add|remove|clear> ['key'|index] ['response'] [matchType] [ignoreCase]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length == 0 || !args[0].equalsIgnoreCase("add") && !args[0].equalsIgnoreCase("remove")
				&& !args[0].equalsIgnoreCase("clear"));
		AutoRespondRoutine respondRoutine = (AutoRespondRoutine) Lanius.getInstance().getRoutineRegistry()
				.get("Auto-respond");
		if (args[0].equalsIgnoreCase("add")) {
			assertUsage(args.length < 5);
			String[] argFields = argumentStr(args, 1).split("'");
			assertUsage(argFields.length < 4);
			AutoRespondRoutine.ResponseEntry.MatchType matchType = AutoRespondRoutine.ResponseEntry.MatchType
					.getById(args[args.length - 2]);
			if (matchType == null) {
				throw new InvalidObjectException(args[args.length - 2]);
			}
			respondRoutine.getResponses().add(new AutoRespondRoutine.ResponseEntry(argFields[1], argFields[3],
					matchType, Boolean.parseBoolean(args[args.length - 1])));
			CommandUtil.addText("Added a response entry with key \"" + argFields[1] + "\" and match type "
					+ matchType.getId() + ".");
		} else if (args[0].equalsIgnoreCase("remove")) {
			assertUsage(args.length < 2);
			int index = Integer.parseInt(args[1]);
			try {
				respondRoutine.getResponses().remove(index);
			} catch (IndexOutOfBoundsException ex) {
				// Empty stub implementation
			}
			CommandUtil.addText("Removed the response entry at index " + index + ".");
		} else {
			respondRoutine.getResponses().clear();
			CommandUtil.addText("Removed all response entries.");
		}
	}

}
