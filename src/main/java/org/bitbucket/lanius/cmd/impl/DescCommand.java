package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.cmd.RoutineCommand;
import org.bitbucket.lanius.routine.Routine;
import org.bitbucket.lanius.util.CommandUtil;

public final class DescCommand extends RoutineCommand {

	public DescCommand() {
		super("description", "desc", "d");
	}

	@Override
	protected void handleRoutine(final Routine foundRoutine, String[] args) {
		CommandUtil.addText("Description of " + foundRoutine + ": \"" + foundRoutine.description() + "\"");
	}

}
