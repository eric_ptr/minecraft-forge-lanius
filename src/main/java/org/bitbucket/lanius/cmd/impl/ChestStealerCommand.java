package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.InvalidObjectException;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.impl.ChestStealerRoutine;
import org.bitbucket.lanius.util.CommandUtil;
import org.bitbucket.lanius.util.Priority;

import net.minecraft.item.Item;

public final class ChestStealerCommand extends ModCommand {
	public ChestStealerCommand() {
		super("cheststealer", "cs");
	}

	@Override
	protected String getParamUsage() {
		return "<add|remove|clear|set> [item] [priority]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length <= 0);
		assertUsage(!args[0].equalsIgnoreCase("add") && !args[0].equalsIgnoreCase("remove")
				&& !args[0].equalsIgnoreCase("clear") && !args[0].equalsIgnoreCase("set"));
		final ChestStealerRoutine chestStealer = (ChestStealerRoutine) Lanius.getInstance().getRoutineRegistry()
				.get("Chest Stealer");
		if (args[0].equalsIgnoreCase("add")) {
			assertUsage(args.length < 3);
			args[1] = args[1].toLowerCase();
			Item item = Item.getByNameOrId(args[1]);
			if (item == null) {
				throw new InvalidObjectException(args[1]);
			}
			Priority priority = Priority.getById(args[2]);
			if (priority == null) {
				throw new InvalidObjectException(args[2]);
			}
			chestStealer.addItemPriority(item, priority);
			CommandUtil.addText("Added a " + chestStealer.name() + " priority of " + priority + " for item "
					+ item.getRegistryName() + ".");
		} else if (args[0].equalsIgnoreCase("remove")) {
			assertUsage(args.length < 2);
			args[1] = args[1].toLowerCase();
			Item item = Item.getByNameOrId(args[1]);
			if (item == null) {
				throw new InvalidObjectException(args[1]);
			}
			chestStealer.removeItemPriority(item);
			CommandUtil
					.addText("Removed a " + chestStealer.name() + " priority for item " + item.getRegistryName() + ".");
		} else if (args[0].equalsIgnoreCase("set")) {
			assertUsage(args.length < 3);
			args[1] = args[1].toLowerCase();
			Item item = Item.getByNameOrId(args[1]);
			if (item == null) {
				throw new InvalidObjectException(args[1]);
			}
			Priority priority = Priority.getById(args[2]);
			if (priority == null) {
				throw new InvalidObjectException(args[2]);
			}
			chestStealer.removeItemPriority(item);
			chestStealer.addItemPriority(item, priority);
			CommandUtil.addText("A " + chestStealer.name() + " priority of " + priority + " for item "
					+ item.getRegistryName() + " has been set.");
		} else {
			chestStealer.clearItemPriorities();
			CommandUtil.addText("Removed all item priorities from " + chestStealer.name() + ".");
		}
	}
}
