package org.bitbucket.lanius.cmd.impl;

import java.util.List;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.InvalidObjectException;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.Routine;
import org.bitbucket.lanius.util.CommandUtil;
import org.bitbucket.lanius.util.StringUtil;
import org.bitbucket.lanius.util.registry.Registry;

public final class HelpCommand extends ModCommand {

	public HelpCommand() {
		super("help", "?");
	}

	private String cmdList(final Registry<ModCommand> cmdRegistry) {
		final List<ModCommand> cmds = cmdRegistry.objects();
		String message = "Commands " + "(" + cmds.size() + "): ";
		for (int cmdIdx = 0; cmdIdx < cmds.size(); cmdIdx++) {
			message += cmds.get(cmdIdx).names()[0];
			if (cmdIdx < cmds.size() - 1) {
				message += ", ";
			}
		}
		return message;
	}

	@Override
	protected String getParamUsage() {
		return "[feature|list] [name|commands|routines]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length > 0 && (!args[0].equalsIgnoreCase("feature") || args.length <= 1)
				&& (!args[0].equalsIgnoreCase("list") || args.length <= 1
						|| !args[1].equalsIgnoreCase("commands") && !args[1].equalsIgnoreCase("routines")));
		final Lanius instance = Lanius.getInstance();
		final Registry<ModCommand> cmdRegistry = instance.getCmdRegistry();
		final Registry<Routine> routineRegistry = instance.getRoutineRegistry();
		if (args.length <= 0) {
			CommandUtil.addText(cmdList(cmdRegistry));
			return;
		}
		if (args[0].equalsIgnoreCase("feature")) {
			args[1] = args[1].toLowerCase();
			final ModCommand foundCmd = cmdRegistry.get(args[1]);
			if (foundCmd != null) {
				CommandUtil.addText(foundCmd.getUsage());
			}
			final Routine foundRoutine = routineRegistry.get(args[1]);
			if (foundRoutine != null) {
				CommandUtil.addText(foundRoutine.name() + ": " + foundRoutine.description());
			}
			if (foundCmd == null && foundRoutine == null) {
				throw new InvalidObjectException(args[1]);
			}
		} else if (args[0].equalsIgnoreCase("list")) {
			String message;
			if (args[1].equalsIgnoreCase("commands")) {
				message = cmdList(cmdRegistry);
			} else {
				final List<Routine> routines = routineRegistry.objects();
				message = "Routines " + "(" + routines.size() + "): ";
				for (int routineIdx = 0; routineIdx < routines.size(); routineIdx++) {
					message += "\247" + (routines.get(routineIdx).isEnabled() ? "a" : "c")
							+ StringUtil.configName(routines.get(routineIdx).name()) + "\247r";
					if (routineIdx < routines.size() - 1) {
						message += ", ";
					}
				}
			}
			CommandUtil.addText(message);
		}
	}

}
