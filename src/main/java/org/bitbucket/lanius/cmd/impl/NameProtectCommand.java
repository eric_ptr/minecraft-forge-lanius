package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.impl.NameProtectRoutine;
import org.bitbucket.lanius.util.CommandUtil;

public final class NameProtectCommand extends ModCommand {

	public NameProtectCommand() {
		super("nameprotect", "protect");
	}

	@Override
	protected String getParamUsage() {
		return "<add|remove|clear> <name> [alias]";
	}

	@Override
	public void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length <= 0);
		final boolean addParam = args[0].equalsIgnoreCase("add");
		assertUsage(!addParam && !args[0].equalsIgnoreCase("remove") && !args[0].equalsIgnoreCase("clear"));
		final NameProtectRoutine tagsRoutine = (NameProtectRoutine) Lanius.getInstance().getRoutineRegistry()
				.get("Name Protect");
		if (addParam) {
			assertUsage(args.length <= 2);
			tagsRoutine.putTag(args[1], args[2]);
			CommandUtil.addText("Added \"" + args[1] + "\" with alias " + "\"" + args[2] + ".\"");
		} else if (args[0].equalsIgnoreCase("remove")) {
			assertUsage(args.length <= 1);
			tagsRoutine.removeAlias(args[1]);
			CommandUtil.addText("Removed \"" + args[1] + "'s\" alias.");
		} else {
			tagsRoutine.clearTags();
			CommandUtil.addText("Removed all name tags.");
		}
	}

}
