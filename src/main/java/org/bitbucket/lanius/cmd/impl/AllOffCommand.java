package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.gui.GuiHandler;
import org.bitbucket.lanius.routine.Routine;
import org.bitbucket.lanius.util.CommandUtil;

public final class AllOffCommand extends ModCommand {

	public AllOffCommand() {
		super("togglealloff", "alloff", "off");
	}

	@Override
	protected String getParamUsage() {
		return "[hud]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		for (Routine routine : Lanius.getInstance().getRoutineRegistry().objects()) {
			if (routine.isEnabled()) {
				routine.setEnabled();
			}
		}
		if (args.length > 0 && Boolean.parseBoolean(args[0])) {
			GuiHandler hud = Lanius.getInstance().getGuiHandler();
			for (String boolName : hud.boolNames()) {
				hud.putValue(boolName, false);
			}
			CommandUtil.addText(!Lanius.getInstance().getModCfg().getBoolean("Toggle Messages", Routine.CFG_CATEGORY,
					false, "Determines whether or not to display a toggle message when a routine is toggled.")
							? "Disabled all routines and HUD options."
							: "Disabled all HUD options.");
		} else if (!Lanius.getInstance().getModCfg().getBoolean("Toggle Messages", Routine.CFG_CATEGORY, false,
				"Determines whether or not to display a toggle message when a routine is toggled.")) {
			CommandUtil.addText("Disabled all routines.");
		}
	}

}
