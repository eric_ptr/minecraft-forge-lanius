package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.cmd.RoutineCommand;
import org.bitbucket.lanius.routine.Routine;
import org.bitbucket.lanius.util.CommandUtil;

public final class ToggleHideCommand extends RoutineCommand {

	public ToggleHideCommand() {
		super("togglehideroutine", "togglehide", "hide", "h");
	}

	@Override
	protected void handleRoutine(Routine foundRoutine, String[] args) {
		foundRoutine.setHidden();
		if (foundRoutine.isHidden()) {
			CommandUtil.addText("Started hiding " + foundRoutine + " from the enabled list.");
		} else {
			CommandUtil.addText("Started showing " + foundRoutine + " in the enabled list.");
		}
	}

}
