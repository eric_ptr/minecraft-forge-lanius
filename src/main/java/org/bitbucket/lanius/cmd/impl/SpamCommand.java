package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.routine.impl.SpamRoutine;
import org.bitbucket.lanius.util.CommandUtil;

public final class SpamCommand extends ModCommand {

	public SpamCommand() {
		super("spam");
	}

	@Override
	protected String getParamUsage() {
		return "<add|remove|clear> [message|index]";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length <= 0 || !args[0].equalsIgnoreCase("add") && !args[0].equalsIgnoreCase("remove")
				&& !args[0].equalsIgnoreCase("clear"));
		final SpamRoutine spamRoutine = (SpamRoutine) Lanius.getInstance().getRoutineRegistry().get("Spam");
		if (args[0].equalsIgnoreCase("add")) {
			final String message = argumentStr(args, 1);
			spamRoutine.addLine(message);
			CommandUtil.addText("Added '" + message + "' to the list of spam.");
		} else if (args[0].equalsIgnoreCase("remove")) {
			CommandUtil.addText(
					"Removed '" + spamRoutine.removeLine(Integer.parseInt(args[1])) + "' from the list of spam.");
		} else {
			spamRoutine.clearLines();
			CommandUtil.addText("Removed all spam from the list.");
		}
	}

}
