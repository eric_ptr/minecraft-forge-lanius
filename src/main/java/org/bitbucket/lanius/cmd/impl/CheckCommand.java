package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.cmd.RoutineCommand;
import org.bitbucket.lanius.routine.Routine;
import org.bitbucket.lanius.util.CommandUtil;
import org.bitbucket.lanius.util.RoutineUtil;

public final class CheckCommand extends RoutineCommand {

	public CheckCommand() {
		super("check", "c");
	}

	@Override
	protected void handleRoutine(final Routine foundRoutine, String[] args) {
		CommandUtil.addText(foundRoutine + " is " + RoutineUtil.stateText(foundRoutine, false) + ".");
	}

}
