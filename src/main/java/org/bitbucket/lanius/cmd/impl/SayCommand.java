package org.bitbucket.lanius.cmd.impl;

import org.bitbucket.lanius.cmd.ModCommand;
import org.bitbucket.lanius.cmd.ModCommandException;
import org.bitbucket.lanius.util.game.NetworkUtil;

public class SayCommand extends ModCommand {

	public SayCommand() {
		super("say", "talk", "chat", "message", "msg");
	}

	@Override
	protected String getParamUsage() {
		return "<message>";
	}

	@Override
	protected void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length < 1);
		NetworkUtil.sendChatMessageNoBlock(argStr);
	}

}
