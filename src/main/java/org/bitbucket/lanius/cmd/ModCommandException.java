package org.bitbucket.lanius.cmd;

public class ModCommandException extends RuntimeException {
	/**
	 * The default serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	public ModCommandException(String message) {
		super(message);
	}
}
