package org.bitbucket.lanius.cmd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.routine.impl.NameProtectRoutine;

import net.minecraft.util.StringUtils;

public abstract class ModCommand {
	private static CommandSource execSrc = CommandSource.CHAT_INPUT;

	public static CommandSource getExecSrc() {
		return execSrc;
	}

	public static String prefix() {
		return Lanius.getInstance().getModCfg().getString("Prefix", "Command", ".",
				"Indicates that the chat message is a client command.");
	}

	public static void setExecSrc(CommandSource execSrc) {
		ModCommand.execSrc = execSrc;
	}

	private final String[] aliases;

	private final String name;

	public ModCommand(final String name, final String... aliases) {
		this.name = name;
		this.aliases = aliases;
	}

	protected final String argumentStr(final String[] args, final int startIdx) {
		String arguments = "";
		for (int argIdx = startIdx; argIdx < args.length; argIdx++) {
			arguments += args[argIdx];
			if (argIdx < args.length - 1) {
				arguments += " ";
			}
		}
		return arguments;
	}

	protected final void assertUsage(final boolean invalidCond) throws InvalidUsageException {
		if (invalidCond) {
			throw new InvalidUsageException(getUsage());
		}
	}

	public final void execute(String[] args, String argStr) throws ModCommandException {
		final NameProtectRoutine protectRoutine = (NameProtectRoutine) Lanius.getInstance().getRoutineRegistry()
				.get("Name Protect");
		if (protectRoutine.isEnabled()) {
			for (int argIdx = 0; argIdx < args.length; argIdx++) {
				for (final Entry<String, String> nameEntry : protectRoutine.nameEntries()) {
					args[argIdx] = args[argIdx].replace("-" + nameEntry.getValue(), nameEntry.getKey());
				}
			}
		}
		onExecute(args, argStr);
	}

	public final List<String> getAliases() {
		String[] prefixAliases = new String[aliases.length];
		final String cmdPrefix = prefix();
		for (int aliasIdx = 0; aliasIdx < prefixAliases.length; aliasIdx++) {
			prefixAliases[aliasIdx] = cmdPrefix + aliases[aliasIdx];
		}
		return Arrays.asList(prefixAliases);
	}

	public final String getName() {
		return prefix() + name;
	}

	protected abstract String getParamUsage();

	public final String getUsage() {
		String usage = getName();
		if (!getAliases().isEmpty()) {
			for (String alias : getAliases()) {
				usage += "|" + alias;
			}
		}
		final String paramUsage = getParamUsage();
		if (!StringUtils.isNullOrEmpty(paramUsage)) {
			usage += " " + paramUsage;
		}
		return usage;
	}

	public final String[] names() {
		final List<String> names = new ArrayList<>();
		names.add(name);
		for (final String alias : aliases) {
			names.add(alias);
		}
		return names.toArray(new String[0]);
	}

	protected abstract void onExecute(String[] args, String argStr) throws ModCommandException;

	@Override
	public final String toString() {
		return getName();
	}
}
