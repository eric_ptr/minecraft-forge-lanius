package org.bitbucket.lanius.cmd;

public final class WrongGameModeException extends ModCommandException {

	/**
	 * The default serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	public WrongGameModeException(String validMode) {
		super("The player is not in game mode: " + validMode);
	}

}
