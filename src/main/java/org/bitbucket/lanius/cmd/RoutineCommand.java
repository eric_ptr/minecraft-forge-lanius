package org.bitbucket.lanius.cmd;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.routine.Routine;

public abstract class RoutineCommand extends ModCommand {

	public RoutineCommand(final String name, final String... aliases) {
		super(name, aliases);
	}

	@Override
	protected final String getParamUsage() {
		return "<routine> [routine2]";
	}

	protected abstract void handleRoutine(final Routine foundRoutine, String[] args);

	@Override
	public final void onExecute(String[] args, String argStr) throws ModCommandException {
		assertUsage(args.length < 1);
		for (int argIdx = 0; argIdx < args.length; argIdx++) {
			final Routine foundRoutine = Lanius.getInstance().getRoutineRegistry().get(args[argIdx]);
			if (foundRoutine == null) {
				throw new InvalidObjectException(args[argIdx]);
			}
			handleRoutine(foundRoutine, args);
		}
	}

}
