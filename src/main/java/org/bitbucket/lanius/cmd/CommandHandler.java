package org.bitbucket.lanius.cmd;

import org.bitbucket.lanius.Lanius;
import org.bitbucket.lanius.cfg.ConfigContainer;
import org.bitbucket.lanius.hook.Hook;
import org.bitbucket.lanius.hook.impl.NetHandlerData;
import org.bitbucket.lanius.util.CommandUtil;
import org.bitbucket.lanius.util.Phase;

import net.minecraft.network.play.client.CPacketChatMessage;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public final class CommandHandler extends ConfigContainer implements Hook<NetHandlerData> {
	private boolean allowChat;

	@Override
	public String category() {
		return "Command";
	}

	private TextComponentString format(TextFormatting color, String str) {
		TextComponentString ret = new TextComponentString(str);
		ret.getStyle().setColor(color);
		return ret;
	}

	@Override
	public void onExecute(final NetHandlerData data, final Phase phase) {
		if (phase.equals(Phase.START) && data.retVal instanceof CPacketChatMessage && !allowChat) {
			final String message = ((CPacketChatMessage) data.retVal).getMessage();
			if (!message.startsWith(ModCommand.prefix())) {
				return;
			}
			final String[] splitMsg = message.split(" ");
			ModCommand foundCmd = Lanius.getInstance().getCmdRegistry()
					.get(splitMsg[0].substring(ModCommand.prefix().length()));
			data.retVal = null;
			if (foundCmd != null) {
				String[] params = new String[Math.max(0, splitMsg.length - 1)];
				if (params.length > 0) {
					System.arraycopy(splitMsg, 1, params, 0, params.length);
				}
				try {
					foundCmd.execute(params,
							params.length > 0 ? message.substring(splitMsg[0].length() + ModCommand.prefix().length())
									: "");
				} catch (InvalidUsageException iue) {
					CommandUtil.addMessage(format(TextFormatting.RED, iue.getMessage()));
				} catch (ModCommandException mce) {
					CommandUtil.addMessage(format(TextFormatting.RED, mce.getMessage()));
				} catch (Throwable e) {
					CommandUtil.addMessage(
							format(TextFormatting.RED, "An error has occurred while executing " + foundCmd + ": " + e));
				}
			} else if (!getBoolean("Send as Chat")) {
				CommandUtil.addText("Command: " + splitMsg[0] + " is invalid.");
			}
		}
	}

	@Override
	public void registerValues() {
		registerValue("Send as Chat", false, "Determines whether or not to send unknown commands as chat messages.");
	}

	public void setAllowChat(boolean allowChat) {
		this.allowChat = allowChat;
	}
}
