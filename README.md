# README #

### What is this repository for? ###

* This repository is for hosting Lanius's SRC
* Version: 4.0_b

### How do I get set up? ###

* This repository is for the developers of Lanius
* Configuration: The configuration files can be found in <your_minecraft_directory>/lanius
* Dependencies: forge-1.12.2-14.23.5.2854-mdk
* How to run tests: Compile and run the code in Eclipse/IntelliJ. Also, add "-Dfml.coreMods.load=org.bitbucket.lanius.core.LaniusLoadingPlugin -Dlanius.debugEnv" to your VM launch parameters.
* Deployment instructions: run "gradlew build" from the MDK directory. The compiled JAR can be found in build/libs. Put it in <your_minecraft_directory>/mods.

### Contribution guidelines ###

* Writing tests: Please test your code thoroughly
* Code review: Please provide explanations for why you think <idea> and please keep it constructive
* Other guidelines: Please give variables meaningful names. Do not use temporary variable names (example: int i).

### Who do I talk to? ###

* Eric